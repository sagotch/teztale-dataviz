import * as UI from './ui/UI'
import { PropsWithChildren, useState } from 'react'
import { StringMap, useDebounce, useHashParamArithmetic, useHashParamRange } from './lib/Utils'
import {
  About,
  Available,
  DelayToConsensus,
  DelegateStats,
  CommitteeSizePerDelay,
  CommitteeSizePerLevel,
  Level,
  LevelDiff,
  NotFound404,
  Report,
} from './Views'
import * as Raviger from 'raviger'
import TeztaleLogo from './assets/teztale.svg';
import {
  useEffect,
  useContext,
} from 'react';
import * as TeztaleServerAPI from './lib/TeztaleServerAPI';
import * as DelegateAliases from './lib/DelegateAliases';
import * as ServerAliases from './lib/ServerAliases';
import { I18nContext, I18nContextProvider } from './lib/I18nContext';
import { ErrorBoundary } from 'react-error-boundary';
import * as Routes from './lib/Routes';
import * as Protocols from './lib/Protocols';

const routes = {
  [Routes.path_About]: () =>
    <About />,
  [Routes.path_Available]: () =>
    <Available />,
  [Routes.path_DelegateStats]: () =>
    <DelegateStats />,
  [Routes.path_CommitteeSizePerDelay]: () =>
    <CommitteeSizePerDelay />,
  [Routes.path_CommitteeSizePerLevel]: () =>
    <CommitteeSizePerLevel />,
  [Routes.path_DelayToConsensus]: () =>
    <DelayToConsensus />,
  [Routes.path_LevelDiff]: () =>
    <LevelDiff />,
  [Routes.path_Level]: () =>
    <Level />,
  [Routes.path_Report]: () =>
    <Report />,
  ['/']: () =>
    <Level />,
}

const Link = ({ href, params, children }: PropsWithChildren<{ href: string, params?: string }>) => {
  const navigate = Raviger.useNavigate()
  const current = Raviger.usePath(Routes.BASE_URL)
  const active = current == href || current == '/' && href === Routes.path_Level // Should we redirect in `routes` instead of having this hack?
  return <UI.Button onClick={active ? () => { } : () => navigate(href + params)} variant={active ? 'active' : undefined}>{children}</UI.Button>
}

const styles = {
  MenuBar: {
    alignItems: 'center',
    paddingBottom: 12,
    paddingTop: 12,
  },
}

const LanguageButton = () => {
  const i18n = useContext(I18nContext)
  return (
    <UI.IconButton
      onClick={() => i18n.setLanguage(i18n.Language === 'debug' ? 'en' : 'debug')}
      icon={UI.Translate}
    />
  )
}

/**
 * If #block or #rangeLowerBound/rangeUpperBound param is found in the url, try to propagate it to other pages.
 */
const Navigation = ({ server, head }: { server: string, head: number | undefined }) => {
  const { I18n } = useContext(I18nContext);
  const { ParamInput: Block0 } = useHashParamArithmetic('block', undefined)
  const { ParamInput: RangeStart0 } = useHashParamArithmetic('rangeLowerBound', undefined)
  const { ParamInput: RangeEnd0 } = useHashParamArithmetic('rangeUpperBound', undefined)
  const Block = Block0?.input || RangeEnd0?.input || 'HEAD-1'
  const [RangeStart, RangeEnd] =
    RangeStart0 !== undefined && RangeEnd0 !== undefined
      ? [RangeStart0.input, RangeEnd0.input]
      : ['-20', Block]
  const block = '#server=' + server + '&block=' + Block
  const blocks = '#server=' + server + '&rangeLowerBound=' + RangeStart + '&rangeUpperBound=' + RangeEnd
  return (
    <UI.Stack style={styles.MenuBar} direction='row' spacing={1}>
      <Link href={Routes.path_Level} params={block}>{I18n.navigation_Level}</Link>
      <Link href={Routes.path_DelayToConsensus} params={blocks}>{I18n.navigation_DelayToConsensus}</Link>
      <Link href={Routes.path_CommitteeSizePerLevel} params={blocks}>{I18n.navigation_CommitteeSizePerLevel}</Link>
      <Link href={Routes.path_CommitteeSizePerDelay} params={blocks}>{I18n.navigation_CommitteeSizePerDelay}</Link>
      <Link href={Routes.path_DelegateStats} params={blocks}>{I18n.navigation_DelegateStats}</Link>
      <Link href={Routes.path_Report} params={blocks}>{I18n.navigation_Report}</Link>
      <Link href={Routes.path_Available} params={blocks}>{I18n.navigation_Availability}</Link>
    </UI.Stack>
  )
}


const FallbackComponent = ({ error }: { error: any }) => {
  return (
    <div role="alert">
      <p>Something went wrong:</p>
      <pre style={{ color: "red" }}>{error.message}</pre>
    </div>
  );
}

const App0 = () => {
  const route = Raviger.useRoutes(routes, { basePath: Routes.BASE_URL });

  const { ServerURL, setServerURL, HEAD, setHEAD, TzKTURL, setTzKTURL } = useContext(TeztaleServerAPI.ServerContext);
  const [TzKTDelegatesAliases, setTzKTDelegatesAliases] = useState({} as StringMap<string>)
  const [ServerDelegatesAliases, setServerDelegatesAliases] = useState({} as StringMap<string>)
  const { setAliases } = useContext(DelegateAliases.AliasesContext);
  const { setProtocols } = useContext(Protocols.ProtocolsContext);
  const { TeztaleServers, setServers, TzKTServers } = useContext(ServerAliases.ServersContext);
  const Servers_keys = TeztaleServers === undefined ? [] : Object.keys(TeztaleServers);

  const [ServerURLInput, setServerURLInput] = useState(ServerURL);
  const dServerURLInput = useDebounce(ServerURLInput);
  useEffect(() => setServerURL(dServerURLInput), [dServerURLInput]);

  const updateHEAD = () => TeztaleServerAPI.getHead().then(setHEAD);
  useEffect(() => {
    setHEAD(undefined);
    DelegateAliases.getAliases(ServerURL).then(setServerDelegatesAliases)
  }, [ServerURL]);
  useEffect(() => { ServerAliases.getServers().then(setServers) }, []);
  useEffect(() => {
    if (ServerURL === undefined || TzKTServers === undefined || TzKTServers[ServerURL] === undefined) {
      setTzKTURL(undefined);
    } else {
      setTzKTURL(TzKTServers[ServerURL]);
    }
  }, [TzKTServers, ServerURL]);
  useEffect(() => {
    DelegateAliases.getTzKTAliases(TzKTURL).then(setTzKTDelegatesAliases)
    Protocols.getProtocols(TzKTURL).then(setProtocols);
  }, [TzKTURL]);
  useEffect(() => {
    updateHEAD();
    const interval = setInterval(updateHEAD, 8000);
    return () => clearInterval(interval);
  }, []);
  useEffect(() => {
    setAliases({ ...TzKTDelegatesAliases, ...ServerDelegatesAliases })
  }, [ServerDelegatesAliases, TzKTDelegatesAliases]);

  return (
    <UI.Stack direction='column' spacing={0} style={{ minHeight: '100vh' }}>
      <UI.FullWidthContainer>
        <UI.Stack style={styles.MenuBar} direction='row' spacing={2}>
          <Raviger.Link href={Routes.path_About} basePath={Routes.BASE_URL}>
            <img src={TeztaleLogo} className="logo" height={20} alt="Teztale logo" />
          </Raviger.Link>
          <UI.Typography.P style={{ marginLeft: 'auto' }}>HEAD: </UI.Typography.P>
          <UI.ButtonCopyOnClick value={HEAD?.toString() || '-'}>
            <UI.BlockLevel level={HEAD ? `${HEAD}` : '-'} />
          </UI.ButtonCopyOnClick>
          {
            Servers_keys
              ? <UI.Input
                value={ServerURLInput}
                onChange={setServerURLInput}
                // Make the search works with both address and alias
                renderItem={Servers_keys.length !== 0 ? (option: string) => <>{(TeztaleServers as StringMap<string>)[option]}</> || option : undefined}
                filterFormatter={Servers_keys.length !== 0 ? (option: string) => TeztaleServers ? (TeztaleServers[option] + '---' + option) || option : option : undefined}
                datalist={Servers_keys.length !== 0 ? Servers_keys : undefined}
              />
              : <UI.Input value={ServerURLInput} onChange={setServerURLInput} />
          }
          <UI.ButtonModeToggle />
          <LanguageButton />
        </UI.Stack>
      </UI.FullWidthContainer>
      <UI.FullWidthContainer>
        <Navigation server={ServerURL} head={HEAD} />
      </UI.FullWidthContainer>
      <ErrorBoundary FallbackComponent={FallbackComponent}>
        {route || <NotFound404 />}
      </ErrorBoundary>
      <UI.Toaster.Toaster />
    </UI.Stack>
  )
}

export const App = () => {
  return (
    <ServerAliases.ServersContextProvider>
      <DelegateAliases.AliasesContextProvider>
        <Protocols.ProtocolsContextProvider>
          <UI.ThemeProvider>
            <I18nContextProvider>
              <TeztaleServerAPI.ServerContextProvider>
                <App0 />
              </TeztaleServerAPI.ServerContextProvider>
            </I18nContextProvider>
          </UI.ThemeProvider >
        </Protocols.ProtocolsContextProvider>
      </DelegateAliases.AliasesContextProvider>
    </ServerAliases.ServersContextProvider>
  )
}
