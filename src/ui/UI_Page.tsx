import React, {
  useContext,
  useEffect,
  useState,
} from 'react';
import * as TeztaleServerData from '../lib/TeztaleServerData';
import {
  type DataByLevel,
  type delaysDistributionWDelegate,
  type ByLevel,
  type endorsingPower,
  type Partitioned,
  delaysDistributionOfOperationsWDelegate,
  extractSources,
  filterSource,
  getEndorsingPower,
  populate,
  populateMany,
} from '../lib/PopulateSortData';
import {
  Arithmetic,
  NumberMap_map,
  range,
  unique,
  useHashParamStringNoAutoURLUpdate,
  useHashParamStringOption,
  useThrottle,
} from '../lib/Utils';
import {
  Option,
  Select,
  Spinner,
} from './UI_Basics';
import {
  BlockRangeInputs,
  Labelled,
  PageParameters,
  PageWithStickySection,
  Toaster,
} from './UI_Advanced';

export type TeztaleDataBase = {
  data: DataByLevel,
  distribution: Partitioned<ByLevel<delaysDistributionWDelegate>>,
  endorsingPower: ByLevel<endorsingPower>,
}
import { ServerContext } from '../lib/TeztaleServerAPI';
import { I18nContext } from '../lib/I18nContext';

export const BlocksRangePage0 = <T,>(props: {
  children: (data: T) => React.ReactNode,
  data: T | undefined,
  loading: boolean,
  onSubmit: (fst: number | undefined, lst: number | undefined) => void,
  shortcuts?: boolean,
  sticky?: React.ReactNode,
  waitForCompletion?: boolean,
}) => {
  const { HEAD } = useContext(ServerContext);

  const {
    updateURL: updateLowerBoundURL,
    ParamURL: LowerBoundURL,
    ParamInput: LowerBoundInput,
    setParamInput: setLowerBoundInput,
  } = useHashParamStringNoAutoURLUpdate('rangeLowerBound', '' as string)
  const {
    updateURL: updateUpperBoundURL,
    ParamURL: UpperBoundURL,
    ParamInput: UpperBoundInput,
    setParamInput: setUpperBoundInput,
  } = useHashParamStringNoAutoURLUpdate('rangeUpperBound', '' as string)

  const onSubmit = (fstBlock?: string, sndBlock?: string) => {
    const ia = fstBlock === undefined ? LowerBoundInput : fstBlock;
    const ib = sndBlock === undefined ? UpperBoundInput : sndBlock;
    try {
      const a0 = ia === undefined ? undefined : Arithmetic.parse(ia)
      const b0 = ib === undefined ? undefined : Arithmetic.parse(ib)
      if (fstBlock === undefined || sndBlock === undefined) {
        updateLowerBoundURL()
        updateUpperBoundURL()
      }
      const a = a0 !== undefined && HEAD !== undefined ? Arithmetic.updateWithHEAD(a0, HEAD) : a0
      const b = b0 !== undefined && HEAD !== undefined ? Arithmetic.updateWithHEAD(b0, HEAD) : b0
      const [fst, lst] = Arithmetic.computeRange([a, b]);
      props.onSubmit(fst, lst);
    } catch (e) {
      console.error(e);
    }
  }

  React.useEffect(() => onSubmit(), [])
  React.useEffect(() => {
    const containsHEAD = (s: string | undefined) => s?.includes('HEAD')
    if (containsHEAD(LowerBoundURL) || containsHEAD(UpperBoundURL)) onSubmit()
  }, [HEAD])

  const sticky =
    <PageParameters>
      <BlockRangeInputs
        required={true}
        size={10}
        lowerBound={[LowerBoundInput, setLowerBoundInput]}
        upperBound={[UpperBoundInput, setUpperBoundInput]}
        onKeyUp_Enter={onSubmit}
      />
      {props.sticky}
    </PageParameters >

  return (
    <PageWithStickySection sticky={sticky} loading={props.loading} data={props.data}>
      {props.children}
    </PageWithStickySection >
  )
}

export const BlocksRangePage = <T,>(props: {
  children: (data: T) => React.ReactNode,
  dataMsec?: boolean,
  formatData: (data: TeztaleDataBase | undefined) => T | undefined,
  formatDataDeps: React.DependencyList,
  sticky?: React.ReactNode,
  waitForCompletion?: boolean,
}) => {

  const { I18n } = useContext(I18nContext);
  const [Fetching, setFetching] = useState(false as boolean | Array<number>)
  const [FetchedData, setFetchedData] = useState(undefined as undefined | DataByLevel)
  const [Loading, setLoading] = useState(false as boolean)

  const onSubmit = (fst: number | undefined, lst: number | undefined) => {
    if (fst != undefined && lst != undefined) {
      setFetchedData({})
      const blocks = range(fst, lst)
      setFetching(blocks)
      const progress = (k: number, v: TeztaleServerData.T | undefined) => {
        if (v !== undefined) setFetchedData((data) => data === undefined ? { [k]: v } : { ...data, [k]: v })
        else Toaster.toast(I18n.toast_ErrorFetchingData(k));
      }
      populate(blocks, progress).then(() => {
        setFetching(false);
      });
    } else {
      setFetching(false);
    }
  }

  const ThrottledFetchedData = useThrottle(FetchedData, 2000)
  const progress = FetchedData === undefined || typeof Fetching === 'boolean' || Fetching.length === 0 ? undefined : Object.keys(FetchedData).length * 100 / Fetching.length

  const [Data0, setData0] = useState(undefined as TeztaleDataBase | undefined)
  const [Data, setData] = useState(undefined as T | undefined)

  useEffect(() => setData(props.formatData(Data0)), [Data0, ...props.formatDataDeps])
  useEffect(() => {
    if ((!props.waitForCompletion || progress === undefined) && ThrottledFetchedData !== undefined) {
      const data = ThrottledFetchedData
      try {
        setLoading(true);
        const endorsingPower = getEndorsingPower(data);
        const distribution = delaysDistributionOfOperationsWDelegate(data, props.dataMsec);
        setData0({ data, distribution, endorsingPower });
        setLoading(false);
      } catch {
        setData0(undefined);
      }
      setLoading(false);
    }
  }, [ThrottledFetchedData])

  const page_LOADING = Loading
  const sticky =
    <>
      {props.sticky}
      {progress !== undefined ? <Spinner style={{ marginBottom: 8 }} progress={progress} /> : <></>}
    </>

  const props00 =
  {
    children: props.children,
    data: Data,
    formatData: props.formatData,
    formatDataDeps: props.formatDataDeps,
    loading: page_LOADING,
    onSubmit: onSubmit,
    sticky: sticky,
  }

  return <BlocksRangePage0 {...props00} />

}

export const BlocksRangePageChunks = <T,>(props: {
  children: (data: T, fst: number | undefined, lst: number | undefined, source: string | undefined) => React.ReactNode,
  dataMsec?: boolean,
  formatData: (acc: T | undefined, data: TeztaleDataBase | undefined) => T | undefined,
  formatDataDeps: React.DependencyList,
  sticky?: React.ReactNode,
  waitForCompletion?: boolean,
}) => {

  const { I18n } = useContext(I18nContext);
  const [Fetching, setFetching] = useState(0 as number | boolean)
  const [FetchedData, setFetchedData] = useState(0)
  const [Data, setData] = useState(undefined as T | undefined);

  const {
    ParamURL: Source0,
    setParamURL: setSource,
  } = useHashParamStringOption('source', '' as string)
  const [SourceList, setSourceList] = useState([] as Array<string>)
  const Source: string | undefined = (Source0 === undefined || SourceList.indexOf(Source0) === -1) ? undefined : Source0;

  const [Fst, setFst] = useState(undefined as number | undefined);
  const [Lst, setLst] = useState(undefined as number | undefined);

  /* FIXME: Use a parameter coming from the server's settings instead of hardcoded values */
  const batchSize = 200;
  useEffect(() => {
    if (Fst != undefined && Lst != undefined) {
      setFetchedData(0);
      const blocks = [];
      for (let i = Fst; i < Lst; i += batchSize) {
        blocks.push(i);
      };
      setFetching(Lst - Fst);
      let result: T | undefined = undefined
      let fetched = 0
      const progress = (begin: number, end: number, r0: { [level: number]: TeztaleServerData.T }) => {
        if (Object.keys(r0).length === 0) Toaster.toast(I18n.toast_ErrorFetchingData(begin, end));
        fetched = fetched + end - begin;
        setFetchedData(fetched);
        setSourceList(sources => unique([...Object.entries(r0).flatMap(([_, data]) => extractSources(data)), ...sources]));
        const filterData = Source === undefined ? undefined : (x: DataByLevel) => NumberMap_map(x, x => filterSource(x, Source));
        const r = filterData ? filterData(r0) : r0;
        Object.entries(r).forEach(([k, v]) => {
          const level = parseInt(k);
          const data = { [level]: v };
          const endorsingPower = getEndorsingPower(data);
          const distribution = delaysDistributionOfOperationsWDelegate(data, props.dataMsec);
          const vv = props.formatData(result, { data, endorsingPower, distribution });
          result = vv;
        })
      }
      blocks
        .reduce((p, x) => p.then(() => populateMany(x, Math.min(Lst, x + batchSize), progress)), Promise.resolve())
        .then(() => setFetching(false))
        .then(() => setFetchedData(0))
        .then(() => setData(result));
    } else {
      setFetching(false);
    }
  }, [Fst, Lst, Source])

  const onSubmit = (fst: number | undefined, lst: number | undefined) => {
    setFst(fst);
    setLst(lst);
  }

  const progress = FetchedData === undefined || typeof Fetching === 'boolean' || Fetching === 0 ? undefined : FetchedData * 100 / Fetching
  const sticky =
    <>
      <Labelled label={I18n.label_Source} tooltip={I18n.tooltip_Source}>
        <Select value={Source || ''} onChange={setSource}>
          <Option key='option_source_default' value={''}>-</Option>
          {SourceList.map((s, i) => <Option key={'option_source_' + i} value={s}>{s}</Option>)}
        </Select>
      </Labelled>
      {props.sticky}
      {progress !== undefined ? <Spinner style={{ marginBottom: 8 }} progress={progress} /> : <></>}
    </>

  const page_LOADING = Fetching !== false

  const props00 =
  {
    children: (data: T) => props.children(data, Fst, Lst, Source),
    data: Data,
    loading: page_LOADING,
    onSubmit: onSubmit,
    shortcuts: true,
    sticky: sticky,
  }

  return <BlocksRangePage0 {...props00} />

}