
import MUICssBaseline from '@mui/joy/CssBaseline';
import {
  CssVarsProvider as MUICssVarsProvider,
  extendTheme as MUIextendTheme,
  useColorScheme as MUIuseColorScheme,
  useTheme as MUIuseTheme,
  type Theme as MUIThemeType,
} from '@mui/joy/styles';

import '@fontsource/jetbrains-mono/500.css';
import '@fontsource/jetbrains-mono/800.css';
import '@fontsource/source-serif-pro/400.css';
import '@fontsource/source-serif-pro/700.css';

let globalBorderRadius = '4px'

declare module '@mui/joy/Button' {
  interface ButtonPropsVariantOverrides {
    active: true,
    neutral: true,
    plain: true,
  }
}

export type Theme = MUIThemeType

const mkColors = (theme: Theme, [dfg, dbg]: [string, string], [lfg, lbg]: [string, string]) => {
  return {
    color: lfg,
    background: lbg,
    [theme.getColorSchemeSelector('dark')]: {
      color: dfg,
      background: dbg,
    },
  }
}

const fontFamily_Serif = 'Source Serif Pro, serif'
const fontFamily_Monospace = 'JetBrains Mono, monospace'

const theme = MUIextendTheme({
  colorSchemes: {
    light: {
      palette: {
        background: {
          body: "rgba(255,255,255)",
          surface: "#f1f1f1",
          popup: "rgba(255,255,255)",
        },
        divider: '#d8d8d8',
        neutral: {
          solidActiveBg: "rgba(40,40,40)",
          solidActiveColor: "#ffffff",
          solidHoverBg: "rgba(40,40,40,0.25)",
          solidHoverColor: "#181818",
          solidBg: "rgba(40,40,40,0.12)",
          solidColor: "#rgba(40,40,40,0.6)",
          plainBg: 'transparent',
          plainColor: 'rgba(40,40,40,0.6)',
          plainHoverBg: 'rgba(40,40,40,0.1)',
          plainActiveBg: 'rgba(40,40,40,0.3)',
          plainHoverColor: 'rgba(40,40,40,0.6)',
          plainDisabledColor: 'rgba(40,40,40,0.38)',
          outlinedBorder: 'rgba(40,40,40,0.38)',
          outlinedColor: 'rgba(40,40,40)',
          outlinedHoverBorder: 'rgba(40,40,40,0.6)',
          outlinedHoverBg: 'rgba(40,40,40,0.12)',
          outlinedHoverColor: 'rgba(40,40,40)',
        },
        primary: {
          plainHoverBg: "rgba(40,40,40,0.12)",
          plainColor: "rgba(40,40,40)",
          softColor: "rgba(40,40,40)",
          softBg: "rgba(40,40,40,0.12)",
        },
        text: {
          primary: "rgba(40,40,40)",
          secondary: "rgba(40,40,40,0.6)",
          tertiary: "rgba(40,40,40,0.38)",
        },
      },
    },
    dark: {
      palette: {
        background: {
          body: "rgba(33,33,33)",
          surface: "rgba(40, 40, 40, 0.6)",
          popup: "rgba(33,33,33)",
        },
        divider: '#333333',
        neutral: {
          solidActiveBg: "rgba(255,255,255)",
          solidActiveColor: "#282828",
          solidHoverBg: "rgba(255,255,255,0.25)",
          solidHoverColor: "rgba(255,255,255)",
          solidBg: "rgba(255, 255, 255, 0.12)",
          solidColor: "rgba(255,255,255)",
          plainBg: 'transparent',
          plainColor: 'rgba(255, 255, 255, 0.6)',
          plainHoverBg: 'rgba(255,255,255,0.1)',
          plainActiveBg: 'rgba(255,255,255,0.3)',
          plainHoverColor: 'rgba(255, 255, 255, 0.6)',
          plainDisabledColor: 'rgba(255,255,255,0.38)',
          outlinedBorder: 'rgba(255,255,255,0.38)',
          outlinedColor: 'rgba(255,255,255)',
          outlinedHoverBorder: 'rgba(255,255,255,0.6)',
          outlinedHoverBg: 'rgba(255,255,255,0.12)',
          outlinedHoverColor: 'rgba(255,255,255)',
        },
        primary: {
          plainHoverBg: "rgba(255,255,255,0.12)",
          plainColor: "rgba(255,255,255)",
          softColor: "rgba(255,255,255)",
          softBg: "rgba(255,255,255,0.12)",
        },
        text: {
          primary: "rgba(255,255,255)",
          secondary: "rgba(255,255,255,0.60)",
          tertiary: "rgba(255,255,255,0.38)",
        },
      },
    },
  },
  radius: {
    xs: globalBorderRadius,
    sm: globalBorderRadius,
    md: globalBorderRadius,
    lg: globalBorderRadius,
    xl: globalBorderRadius,
  },
  components: {
    JoyButton: {
      styleOverrides: {
        root: ({ ownerState, theme }) => ({
          fontWeight: '500',
          '--variant-borderWidth': '2px',
          //          ...(ownerState.variant === 'neutral' && mkColors(theme, ['#ffffff', 'rgba(255, 255, 255, 0.12)'], ['#282828', 'rgba(0, 0, 0, 0.12)'])),
          ...(ownerState.variant === 'active' && mkColors(theme, ['#282828', '#ffffff'], ['#ffffff', '#181818'])),
          //         ...(ownerState.variant === 'plain' && mkColors(theme, ['rgba(255, 255, 255, 0.74)', 'transparent'], ['rgba(0, 0, 0, 0.6)', 'transparent'])),
        }),
      },
    },
    JoyModalClose: {
      defaultProps: {
        sx: {
          zIndex: '1',
        },
      },
    },
    JoyModalDialog: {
      styleOverrides: {
        root: ({ theme }) => ({
          ...{
            background: theme.colorSchemes.light.palette.background.body,
            [theme.getColorSchemeSelector('dark')]: {
              background: theme.colorSchemes.dark.palette.background.body,
            },
          },
        }),
      },
    },
    JoyTooltip:{
      styleOverrides:{
                root: ({ theme }) => ({
          ...{
            background: theme.colorSchemes.light.palette.background.body,
            [theme.getColorSchemeSelector('dark')]: {
              background: theme.colorSchemes.dark.palette.background.body,
            },
          },
        }),
      }
    },
    JoyCard: {
      defaultProps: {
        sx: {
          boxShadow: 'none',
          marginTop: '16px',
        },
      },
    },
    JoyAutocomplete: {
      styleOverrides: {
        input: {
          minWidth: '1ex',
        },
      },
    },
    JoyInput: {
      defaultProps: {
        sx: {
          minWidth: '1ex',
        },
      },
    },
    JoyTable: {
      styleOverrides: {
        root: ({ theme }) => ({
          ...({
            '&': {
              '--Table-headerUnderlineThickness': '1px',
              '--TableCell-paddingX': '32px',
              '--TableCell-paddingY': '8px',
              'background': theme.palette.background.surface,
            },
            '& th, & td': {
              'color': theme.palette.text.primary,
              'vertical-align': 'top',
            },
          }),
        }),
      },
    },
  },
  typography: {
    h1: { fontFamily: fontFamily_Serif },
  },
  fontFamily: {
    body: fontFamily_Monospace,
    display: fontFamily_Monospace,
    code: fontFamily_Monospace,
    fallback: 'monospace',
  },
})

export type colorScheme = 'light' | 'dark'
export const useColorScheme = () => {
  const mui = MUIuseColorScheme();
  const mode0 = mui.mode === 'light' || mui.mode === 'dark' ? mui.mode : mui.colorScheme
  const mode: colorScheme = mode0 === 'dark' ? 'dark' : 'light'
  const setMode = (m: colorScheme): void => mui.setMode(m)
  return [mode, setMode] as [colorScheme, (_: colorScheme) => void]
}

export const useTheme = MUIuseTheme

export const ThemeProvider = (props: React.PropsWithChildren<{}>) => {
  // `useColorScheme` requires MUICssVarsProvider
  return (
    <MUICssVarsProvider defaultMode='system' theme={theme}>
      <MUICssBaseline />
      {props.children}
    </MUICssVarsProvider>
  )
}
