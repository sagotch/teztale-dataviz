import {
  Area as RechartsArea,
  Bar as RechartsBar,
  BarChart as RechartsBarChart,
  CartesianGrid as RechartsCartesianGrid,
  Cell as RechartsCell,
  ComposedChart as RechartsComposedChart,
  Label,
  Legend as RechartsLegend,
  Line,
  RadialBar,
  RadialBarChart as RechartsRadialBarChart,
  ReferenceArea as RechartsReferenceArea,
  ReferenceLine,
  ResponsiveContainer,
  Scatter,
  Pie as RechartsPie,
  PieChart as RechartsPieChart,
  Tooltip,
  XAxis,
  YAxis,
  LegendType,
} from 'recharts';
import { Margin } from 'recharts/types/util/types';
import * as Theme from './UI_Theme';

import MUIBox from '@mui/joy/Box';
import React, { PropsWithChildren, useRef, useState } from 'react';
import { CategoricalChartState } from 'recharts/types/chart/types';
import { parseIntUndefined } from '../lib/Utils';

export type AxisRound = 'endo' | 'preendo' | number
export const AxisRoundEndorsment: AxisRound = 'endo'
export const AxisRoundPreEndorsment: AxisRound = 'preendo'
export type Axis = { key: string, label: string, color?: string, round: AxisRound }
export type Reference = {
  color?: string,
  dash?: boolean,
  label: string,
  width?: number,
} & ({ x: number, y?: never, } | { x?: never, y: number, })

export type ChartParam<T, R> = {
  data: Array<T>,
  height?: number,
  margin?: Margin,
  onClick?: (_: { tick: string }) => void,
  onSelection?: (start: number, stop: number) => void,
  placeholder?: string,
  references?: Array<R>,
  tooltip?: (tick: string) => JSX.Element,
  tooltipLabelFormatter?: (value: string) => JSX.Element,
  width?: number,
  xAxis: {
    key: string,
    label: string,
    ticks: Array<number>,
  },
  yAxis: Array<Axis>,
  yAxisLabel?: string,
}

export type chartTheme0 = {
  background: string,
  grid: string,
  ref: Array<string>,
  endorsement: string,
  preendorsement: string,
  quorum: string,
  round: Array<string>,
}
export type chartTheme = {
  dark: chartTheme0,
  light: chartTheme0,
}

export const chartTheme: chartTheme = {
  dark: {
    background: '#282828',
    grid: '#ffffff61',
    ref: ['#E85252'],
    preendorsement: '#6A72E4',
    endorsement: '#27CE8B',
    quorum: 'rgb(246, 179, 70)',
    round: [
      '#43c9ee',
      '#ffa600',
      '#f8578b',
      '#de4c2a',
      '#b00082',
      '#128daf',
      '#d4a292',
      '#823800',
      '#0063a6',
      '#ff771d',
      '#bc7000',
      '#914dac',
      '#5E4FA2',
      '#3288BD',
      '#66C2A5',
      '#ABDDA4',
      '#E6F598',
      '#FFFFBF',
      '#FEE08B',
      '#FDAE61',
      '#F46D43',
      '#D53E4F',
      '#9E0142',
    ]
  },
  light: {
    background: '#f1f1f1',
    grid: '#28282861',
    ref: ['#E85252'],
    preendorsement: '#7975C7',
    endorsement: '#39A57A',
    quorum: 'rgb(246, 179, 70)',
    round: [
      '#43c9ee',
      '#ffa600',
      '#f8578b',
      '#de4c2a',
      '#b00082',
      '#128daf',
      '#d4a292',
      '#823800',
      '#0063a6',
      '#ff771d',
      '#bc7000',
      '#914dac',
      '#5E4FA2',
      '#3288BD',
      '#66C2A5',
      '#ABDDA4',
      '#E6F598',
      '#FFFFBF',
      '#FEE08B',
      '#FDAE61',
      '#F46D43',
      '#D53E4F',
      '#9E0142',
    ]
  },
}

// Only NumberBarChart can have references on the X axis because the use 'number' instead of 'category'
export type AreaChartParam<T> = Omit<ChartParam<T, Exclude<Reference, { x: number, y?: never, }>>, 'yAxis'> & { yAxis: Array<{ area: Axis, line?: Omit<Axis, 'color' | 'round'>, }> }
export type NumberBarChartParam<T> = ChartParam<T, Reference>
export type RadialBarChartParam<T> = Omit<ChartParam<T, Reference>, 'onClick' | 'xAxis' | 'yAxis' | 'yAxisLabel'>
export type LineChartParam<T> = ChartParam<T, Exclude<Reference, { x: number, y?: never, }>>
export type ScatterChartParam<T> = ChartParam<T, Exclude<Reference, { x: number, y?: never, }>>

const CartesianGrid = (props: { colors: chartTheme0 }) => {
  return <RechartsCartesianGrid stroke={props.colors.grid} strokeDasharray="1" />
}

const getYColor = (colors: chartTheme0, data: Omit<Axis, 'key'>) => {
  return data.color || (data.round === 'endo' ? colors.endorsement : data.round === 'preendo' ? colors.preendorsement : colors.round[data.round])
}

const Legend = (yAxis0: Array<Omit<Axis, 'key'>> | undefined, references0: Array<Reference> | undefined) => {
  const [scheme, _] = Theme.useColorScheme()
  const colors = chartTheme[scheme]
  const theme = Theme.useTheme()
  const yAxis = (yAxis0 || []).map((y, i) => {
    return { value: y.label, type: 'circle' as LegendType, color: getYColor(colors, y), }
  })
  const references00 = (references0 || []).map((r, i) => {
    return r.label ? { value: r.label, type: 'plainline' as LegendType, color: r.color || colors.ref[i], payload: { strokeDasharray: r.dash ? 5 : 0 } } : undefined
  }).filter(Boolean)
  const references = references00 as Array<Exclude<typeof references00[0], undefined>> // We used filter(Boolean) on the array
  const payload = [...yAxis, ...references]
  const formatter = (element: string) => {
    const style = {
      color: theme.palette.text.primary,
      fontSize: 15,
      marginLeft: 8,
      marginRight: 10,
      verticalAlign: 'middle',
    }
    return <span style={style}>{element}</span>
  }
  return (
    <RechartsLegend wrapperStyle={{ position: 'relative' }} formatter={formatter} payload={payload} />
  )
}

const References = (references: Array<Reference> | undefined) => {
  const [scheme, _] = Theme.useColorScheme()
  const colors = chartTheme[scheme]
  return (
    (references || []).map((r, i) => {
      const stroke = r.color || colors.ref[i]
      const strokeDasharray = r.dash ? 5 : undefined
      const strokeWidth = r.width || 2
      return (
        <ReferenceLine
          key={'reference_' + i}
          x={r.x ? r.x : undefined}
          y={r.y}
          stroke={stroke}
          strokeDasharray={strokeDasharray}
          strokeWidth={strokeWidth}
          ifOverflow="extendDomain"
        />
      )
    })
  )
}

const Container = <T, R extends Reference>({
  children,
  onClick: onClick0,
  onSelection,
  XAxisType,
  tooltip,
  tooltipCursor,
  ...props
}: PropsWithChildren<ChartParam<T, R> & {
  XAxisType?: 'number' | 'category',
  yAxis: Array<Omit<Axis, 'key'>> | undefined,
  tooltip?: (tick: string) => JSX.Element,
  tooltipCursor?: boolean,
}>
) => {
  const theme = Theme.useTheme()
  const [scheme, _] = Theme.useColorScheme()
  const colors = chartTheme[scheme]
  const wrapOnFunction = (fn: ((_: { tick: string }, __: any) => void) | undefined) =>
    fn === undefined ? undefined : (state: CategoricalChartState, event: any) => { if (state.activeLabel) { fn({ tick: state.activeLabel }, event) } }
  const [SelectStart, setSelectStart] = useState(undefined as number | undefined)
  const [SelectStop, setSelectStop] = useState(undefined as number | undefined)
  const interactive = onClick0 !== undefined || onSelection !== undefined
  const onMouseDown = interactive ? ({ tick }: { tick: string }) => setSelectStart(parseIntUndefined(tick)) : undefined
  const onMouseMove = interactive ? ({ tick }: { tick: string }) => setSelectStop(parseIntUndefined(tick)) : undefined
  const onMouseUp =
    interactive
      ? (arg: { tick: string }) => {
        if (SelectStart === SelectStop || SelectStart === undefined || SelectStop === undefined) {
          setSelectStart(undefined);
          setSelectStop(undefined);
          if (onClick0 !== undefined) onClick0(arg)
        } else if (onSelection !== undefined) {
          const [Start, Stop] = SelectStart > SelectStop ? [SelectStop, SelectStart] : [SelectStart, SelectStop];
          setSelectStart(undefined);
          setSelectStop(undefined);
          onSelection(Start, Stop);
        }
      }
      : undefined
  return (
    <ResponsiveContainer width="100%" height={props.height || 500}>
      <RechartsComposedChart
        data={props.data}
        margin={props.margin || { top: 40, right: 60, left: 40, bottom: 60 }}
        onMouseDown={wrapOnFunction(onMouseDown)}
        onMouseMove={wrapOnFunction(onMouseMove)}
        onMouseUp={wrapOnFunction(onMouseUp)}
      >
        <CartesianGrid colors={colors} />
        <XAxis dataKey={props.xAxis.key} domain={['dataMin', 'dataMax']} color={theme.palette.text.primary} ticks={props.xAxis.ticks} type={XAxisType || 'category'} {...(XAxisType === 'number' ? { scale: 'linear' } : {})}>
          <Label value={props.xAxis.label} color={theme.palette.text.primary} position={'bottom'} offset={0} />
        </XAxis>
        {props.yAxisLabel
          ? <YAxis color={theme.palette.text.primary} type='number' width={60}>
            <Label color={theme.palette.text.primary} value={props.yAxisLabel} angle={-90} position={'left'} />
          </YAxis>
          : <></>}
        {children}
        {References(props.references)}
        {Legend(props.yAxis, props.references)}
        {props.placeholder
          ? <text x="50%" y="50%" dominant-baseline="middle" text-anchor="middle" fill={theme.palette.text.primary}>{props.placeholder}</text>
          : <></>}
        {
          tooltip !== undefined
            ? <Tooltip
              content={({ active, label }) => active ? tooltip(label) : null}
              cursor={tooltipCursor}
            />
            : <Tooltip
              contentStyle={{ backgroundColor: theme.palette.background.popup, border: 'none' }}
              cursor={tooltipCursor}
              labelFormatter={props.tooltipLabelFormatter}
              separator=': '
            />
        }
        {SelectStart === undefined || SelectStop === undefined ? <></> :
          <RechartsReferenceArea x1={'' + SelectStart} x2={'' + SelectStop} strokeOpacity={0.3} />
        }
      </RechartsComposedChart>
    </ResponsiveContainer>
  )
}

const SuperimposedBarsChart0 = <T,>({ ...props }: NumberBarChartParam<T>) => {
  const [scheme, _] = Theme.useColorScheme()
  const colors = chartTheme[scheme]
  const theme = Theme.useTheme()
  return (
    <Container XAxisType='number' {...props}>
      {props.yAxis.map((y, i) => {
        return (
          <RechartsArea
            activeDot={<></>} key={'area_' + i}
            dataKey={y.key}
            fill={getYColor(colors, y)}
            fillOpacity={0.5}
            name={y.label}
            stroke='none'
            type='stepAfter'
          />
        )
      })}
    </Container>
  )
}

export type RealBarChartProps = Omit<NumberBarChartParam<{
  color: string;
  name: string;
  value: number;
}>, 'yAxis'>

const BarChart0 = ({ ...props }: RealBarChartProps) => {
  const [scheme, _] = Theme.useColorScheme()
  const colors = chartTheme[scheme]
  return (
    <Container yAxis={[]} {...props} >
      <RechartsBar dataKey='value'>
        {props.data.map((x, index) => {
          return (
            <RechartsCell key={`cell-${index}`} fill={x.color || colors.round[index % colors.round.length]} />
          )
        })}
      </RechartsBar>
    </Container>
  )
}

const LineChart0 = <T,>(props: LineChartParam<T>) => {
  const [scheme, _] = Theme.useColorScheme()
  const colors = chartTheme[scheme]
  return (
    <Container {...props} tooltipCursor={false}>
      {props.yAxis.map((y, i) => {
        return (
          <Line
            dataKey={y.key}
            fill={getYColor(colors, y)}
            isAnimationActive={false}
            key={'line_' + i}
            name={y.label}
            stroke={getYColor(colors, y)}
          />
        )
      })}
    </Container>
  )
}

const AreaChart0 = <T,>(props: AreaChartParam<T>) => {
  const [scheme, _] = Theme.useColorScheme()
  const colors = chartTheme[scheme]
  const yAxis = props.yAxis.map(y => { return { label: y.area.label, color: y.area.color, round: y.area.round, key: y.area.key } })
  const newProps = { ...props, yAxis }
  return (
    <Container XAxisType='category' {...newProps}>
      {props.yAxis.map((y, i) => {
        const color = getYColor(colors, y.area)
        if (y.line !== undefined) {
          return (
            <React.Fragment key={'area_' + i}>
              <RechartsArea
                dataKey={y.area.key}
                fill={color}
                isAnimationActive={false}
                legendType='none'
                name={y.area.label}
                opacity={0.5}
                stroke='none'
              />
              <Line
                dataKey={y.line.key}
                isAnimationActive={false}
                name={y.line.label}
                stroke={color}
              />
            </React.Fragment>
          )
        } else {
          return (
            <RechartsArea
              dataKey={y.area.key}
              fill={color}
              isAnimationActive={false}
              key={'area_' + i}
              name={y.area.label}
              stroke={color}
            />
          )
        }
      })}
    </Container>
  )
}

const ScatterChart0 = <T,>(props: ScatterChartParam<T>) => {
  const [scheme, _] = Theme.useColorScheme()
  const colors = chartTheme[scheme]
  return (
    <Container {...props} tooltipCursor={false}>
      {props.yAxis.map((y, i) =>
        <Scatter
          dataKey={y.key}
          fill={getYColor(colors, y)}
          isAnimationActive={false}
          key={'scatter_' + i}
          name={y.label}
        />)}
    </Container>
  )
}

const Wrapper = (props: { children?: React.ReactNode }) => {
  const [scheme, _] = Theme.useColorScheme()
  return (
    <MUIBox sx={{ bgcolor: chartTheme[scheme].background, }} {...props} />
  )
}

export const AreaChart = <T,>(props: AreaChartParam<T>) => {
  return (
    <Wrapper>
      <AreaChart0 {...props} />
    </Wrapper>
  )
}

export const SuperimposedBarsChart = <T,>(props: NumberBarChartParam<T>) => {
  return (
    <Wrapper>
      <SuperimposedBarsChart0 {...props} />
    </Wrapper>
  )
}

export const BarChart = (props: RealBarChartProps) => {
  return (
    <Wrapper>
      <BarChart0 {...props} />
    </Wrapper>
  )
}

export const LineChart = <T,>(props: LineChartParam<T>) => {
  return (
    <Wrapper>
      <LineChart0 {...props} />
    </Wrapper>
  )
}

export const ScatterChart = <T,>(props: ScatterChartParam<T>) => {
  return (
    <Wrapper>
      <ScatterChart0 {...props} />
    </Wrapper>
  )
}