
export type breakpoint = ('xs' | 'sm' | 'md' | 'lg' | 'xl')

export type Breakpoint<T> = T | { xs: T, sm?: T, md?: T, lg?: T, xl?: T }

export type BaseProps<T> = React.PropsWithChildren<{
  style?: React.CSSProperties,
  title?:string,
 } & T>

export type alignItems = { alignItems?: Breakpoint<('flex-start' | 'center' | 'flex-end')> }

export type direction = { direction?: Breakpoint<('row' | 'column')> }

export type margin = {
  marginX?: number,
  marginY?: number,
}

export type padding = {
  paddingX?: number,
  paddingY?: number,
}

export type spacing = { spacing?: number }

export type textAlign = {
  textAlign?: ('left' | 'center' | 'right')
}
