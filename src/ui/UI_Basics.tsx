// This module define all components need in the Teztale Dataviz application
// It's an abstraction over another UI framework in order to:
// 1. Customize component globally
// 2. Easy the switch to another framework if needed

import React, { HTMLAttributes, ReactNode } from 'react';
import MUIAutocomplete, { createFilterOptions } from '@mui/joy/Autocomplete';
import MUIAutocompleteOption from '@mui/joy/AutocompleteOption';
import MUIButton from '@mui/joy/Button';
import MUIButtonGroup from '@mui/joy/ButtonGroup';
import MUICard from '@mui/joy/Card';
import MUICircularProgress from '@mui/joy/CircularProgress';
import MUICheckbox from '@mui/joy/Checkbox';
import MUIChip from '@mui/joy/Chip';
import MUIContainer from '@mui/joy/Container';
import MUIDivider from '@mui/joy/Divider';
import MUIInput from '@mui/joy/Input';
import MUILinearProgress from '@mui/joy/LinearProgress';
import MUIModal from '@mui/joy/Modal';
import MUIModalClose from '@mui/joy/ModalClose';
import MUIModalDialog from '@mui/joy/ModalDialog';
import MUIOption from '@mui/joy/Option';
import MUISelect from '@mui/joy/Select';
import MUIStack from '@mui/joy/Stack';
import MUITable from '@mui/joy/Table';
import MUITooltip from '@mui/joy/Tooltip';
import MUITypography, { TypographyProps as MUITypographyProps } from '@mui/joy/Typography';
import {
  type alignItems,
  type BaseProps,
  type breakpoint,
  type direction,
  type margin,
  type padding,
  type spacing,
  type textAlign,
} from './UI_Props';
import * as Icons from './UI_Icons';
import * as Theme from './UI_Theme';
import { UseState } from '../lib/Utils';

export type ButtonProps = BaseProps<
  {
    disabled?: boolean | undefined,
    onClick: React.MouseEventHandler<HTMLElement>,
    variant?: ('active' | 'solid' | 'plain' | 'outlined'),
  }
  & margin
>
export const Button = ({ title, ...props }: ButtonProps) => {
  return title == undefined
    ? <MUIButton color='neutral' size='sm' {...props} />
    : <MUITooltip title={title} variant='solid'><MUIButton color='neutral' size='sm' {...props} /></MUITooltip>
}

export const ButtonsGroup = ({ children }: { children: React.ReactNode }) => {
  return (
    <MUIButtonGroup aria-label="outlined primary button group">
      {children}
    </MUIButtonGroup>
  )
}

export type CardProps = BaseProps<padding>
export const Card = (props: CardProps) => {
  return <MUICard variant='outlined' {...props} />
}

export const Chip = (props: BaseProps<{}>) => {
  return (
    <MUIChip {...props} />
  )
}

export type CheckboxProps = BaseProps<{
  checked?: boolean,
  onChange: () => void,
}>
export const Checkbox = ({ onChange: onChange0, ...props }: CheckboxProps) => {
  const onChange = (event: React.ChangeEvent) => {
    onChange0();
  };
  return <MUICheckbox onChange={onChange} {...props} />
}

export type ContainerProps = BaseProps<{ maxWidth?: breakpoint }>
export const Container = (props: ContainerProps) => {
  return <MUIContainer {...props} />
}

export type DividerProps = BaseProps<{ orientation: 'horizontal' | 'vertical' }>
export const Divider = (props: DividerProps) => {
  return <MUIDivider {...props} />
}

const Input_onKeyUpWrapper = (handlerEnter: (() => void) | undefined, handler: ((_: string) => void) | undefined): React.KeyboardEventHandler<HTMLInputElement> | undefined => {
  if (handlerEnter !== undefined) {
    return (e) => e.key == 'Enter' ? handlerEnter() : handler !== undefined ? handler(e.key) : {}
  } else if (handler !== undefined) {
    return (e) => handler(e.key)
  } else {
    return undefined
  }
}

export type InputProps = BaseProps<{
  datalist?: Array<string>,
  disabled?: boolean,
  endDecorator?: JSX.Element,
  filterFormatter?: (_: string) => string,
  onKeyUp?: (_: string) => void,
  onKeyUp_Enter?: () => void,
  onChange: (_: string) => void,
  max?: number, // FIXME: only for numbers
  min?: number, // FIXME: only for numbers
  placeholder?: string,
  readOnly?: boolean,
  renderItem?: (item: string) => JSX.Element,
  required?: boolean,
  size?: number,
  startDecorator?: JSX.Element,
  styleInput?: React.CSSProperties,
  type?: 'text' | 'number',
  value?: string,
}>
export const Input = ({
  datalist,
  endDecorator,
  filterFormatter,
  required,
  style,
  styleInput,
  onChange,
  onKeyUp,
  onKeyUp_Enter,
  renderItem,
  startDecorator,
  type,
  value,
  ...rest
}: InputProps) => {
  const slotProps = {
    input: {
      ...rest,
      style: styleInput,
      onKeyUp: Input_onKeyUpWrapper(onKeyUp_Enter, onKeyUp),
    },
    clearIndicator: {
      style: { display: 'none' },
    },
  }
  const props = {
    endDecorator,
    error: required && (value || undefined) == undefined,
    slotProps,
    startDecorator,
    style,
    type: type || 'text',
    value,
  }
  if (datalist !== undefined) {
    const filterOptions = filterFormatter ? createFilterOptions({ stringify: filterFormatter }) : undefined
    const renderOption = renderItem === undefined ? undefined : (props: Omit<HTMLAttributes<HTMLLIElement>, "color">, option: string) =>
      <MUIAutocompleteOption {...props}>{renderItem(option)}</MUIAutocompleteOption>
    return <MUIAutocomplete onInputChange={(e, value) => onChange(value)} {...props} options={datalist} renderOption={renderOption} filterOptions={filterOptions} />
  } else {
    return <MUIInput autoComplete='false' onChange={e => onChange(e.target.value)} {...props} />
  }
}

export const LinearProgress = () => {
  return <MUILinearProgress />
}

export const Modal = ({ open, children }: { open: UseState<boolean>, children: JSX.Element }) => {
  const [open0, setOpen] = open;
  return (
    <MUIModal
      open={open[0]}
      onClose={() => open[1](false)}
      sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
    >
      <MUIModalDialog>
        <MUIModalClose />
        <div style={{
          maxHeight: '100%',
          width: 'calc(100vw - 4rem)',
          maxWidth: 'calc(1200px - 4rem)',
          overflow: 'auto',
        }}>
          {children}
        </div>
      </MUIModalDialog>
    </MUIModal>
  )
}

export const Option = (props: BaseProps<{ value: string }>) => {
  return <MUIOption {...props} />
}

export const Select = <T extends string>({ onChange, ...props }: BaseProps<{ value: T | undefined, disabled?: boolean, onChange: (value: T) => void }>) => {
  const onChange0 = (e: any, value: T | null) => { if (value !== null) { onChange(value) } } // FIXME: what is the right type?
  return <MUISelect onChange={onChange0} {...props} />
}

export const SpinnerWithLabel = ({ progress, ...props }: BaseProps<{ progress: number }>) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
      <Spinner {...props} />
      <div style={{ marginLeft: 10 }}>
        {`${Math.round(progress)}%`}
      </div>
    </div>
  )
}

export const Spinner = ({ progress, ...props }: BaseProps<{ progress?: number }>) => {
  return (
    progress === undefined
      ? <MUICircularProgress variant='plain' size='sm' {...props} />
      : <SpinnerWithLabel progress={progress} {...props} />
  )
}

export type StackProps = BaseProps<
  alignItems
  & direction
  & spacing
  & { divider?: ReactNode }
>
export const Stack = (props: StackProps) => {
  return <MUIStack {...props} />
}

export const Table = ({ stripe, hover, ...props0 }: BaseProps<{ stripe?: boolean, hover?: boolean }>) => {
  const propStripe = stripe === true ? { stripe: 'even' } : {};
  const propHover = hover === true ? { hoverRow: true } : {};
  const props = { ...propStripe, ...propHover, ...props0 };
  return (
    <MUITable color='neutral' borderAxis='bothBetween' {...props} />
  )
}

export const TableBody = (props: { children: React.ReactNode }) => {
  return <tbody {...props} />
}

export const TableHead = (props: { children: React.ReactNode }) => {
  return <thead {...props} />
}

export const TD = (props: BaseProps<{ colspan?: string }>) => {
  return <td {...props} />
}

const styleTH: React.CSSProperties = {
  fontWeight: 'bold',
  backgroundColor: 'none',
}
export const TH = (props: BaseProps<{}>) => {
  return (
    <th style={styleTH} {...props} />
  )
}

export const TR = (props: BaseProps<{}>) => {
  return (
    <tr {...props} />
  )
}

export const Tooltip = ({ Icon: Icon0, size, style, title }: { Icon?: typeof Icons.Info, size?: number, style?: React.CSSProperties, title: string }) => {
  const theme = Theme.useTheme()
  const Icon = Icon0 || Icons.Info;
  return (
    <Icon color={theme.palette.text.secondary} size={size || 18} title={title} style={{ marginLeft: '8px', ...style }} />
  )
}

export const TooltipOption = (text: string | undefined) => {
  return text ? <Tooltip title={text} /> : <></>
}

export namespace Typography {

  export type TypographyProps<T> = BaseProps<{ color?: string } & margin & T>

  const MUI = (muipros: MUITypographyProps, props: TypographyProps<any>) => {
    let MUIprops: MUITypographyProps = { ...muipros, ...{ ...props, color: undefined, textColor: props.color } }
    return <MUITypography {...MUIprops} />
  }

  export type CODEProps = TypographyProps<{}>
  export const CODE = (props: CODEProps) => MUI({ component: 'code', variant: 'soft' }, props)

  export type H1Props = TypographyProps<textAlign>
  export const H1 = (props: H1Props) => MUI({
    fontSize: 24,
    level: 'h1',
    marginY: 2,
  }, props)

  export type H1SubtitleProps = TypographyProps<textAlign>
  export const H1Subtitle = (props: H1SubtitleProps) => MUI({
    fontSize: 15,
    level: 'h2',
    marginY: 2,
  }, props)

  export type PProps = TypographyProps<textAlign>
  export const P = (props: PProps) => MUI({ level: 'body-md', marginY: 2 }, props)

}
