import MUIBox from '@mui/joy/Box';
import MUIFormControl from '@mui/joy/FormControl';
import MUIFormLabel from '@mui/joy/FormLabel';
import * as Basics from './UI_Basics';
import * as Icons from './UI_Icons';
import {
  type BaseProps,
} from './UI_Props';
import * as Theme from './UI_Theme';
import React, {
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import {
  Arithmetic,
  StringMap,
  UseState,
  download,
  parseIntUndefined,
  shortenHash,
} from '../lib/Utils';
import { I18n } from '../lib/I18n_interface';
import { I18nContext } from '../lib/I18nContext';
import * as DelegateAliases from '../lib/DelegateAliases';
import * as Routes from '../lib/Routes';

const Block_WithIcon = ({ text }: { text: string }) => {
  return (
    <>
      <Icons.Block style={{ marginRight: '4px' }} />
      {text}
    </>
  )
}

export const BlockHash = ({ hash }: { hash: string }) => <Block_WithIcon text={shortenHash(hash)} />

export const BlockLevel = ({ level }: { level: string }) => <Block_WithIcon text={level} />

export const ButtonCopyOnClick = ({ value, ...props }: Omit<Basics.ButtonProps, 'onClick'> & { value: string }) => {
  const { I18n } = useContext(I18nContext);
  return (
    <Basics.Button title={I18n.tooltip_CopyToClipboard} onClick={() => copyToClipboard(value, I18n.toast_CopiedToClipboard)} {...props} />
  )
}

export type ButtonListProps = {
  data: Array<string>,
  ext?: (_: string) => Array<ContextMenuPropsItems>,
  id: string,
  noControls?: boolean,
  print?: (_: string) => (JSX.Element | string),
  shorten?: number,
  style?: (_: string) => React.CSSProperties,
  title?: (_: string) => string,
}

export const WithMenu = <T,>({
  Element,
  menuItems,
  ...props }
  : {
    data: Array<T>,
    dataKey: (_: T) => string,
    id?: string,
    Element: (props: { data: T, onClick: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void, }) => JSX.Element,
    menuItems: (_: T) => Array<ContextMenuPropsItems>,
  }) => {
  const { I18n } = useContext(I18nContext);
  const [Menu, setMenu] = useState(undefined as undefined | {
    anchor: HTMLElement,
    items: Array<ContextMenuPropsItems>
  })
  // When clicking another chip,
  // ClickAwayListener is triggered after the chip has already been set to the new value
  // preventClosing ref is a hack in order to ensure that clicking another chip will not
  // close the newly opened element.
  const preventClosing = useRef(undefined as undefined | string)
  const menu =
    Menu === undefined ? <></>
      : <ContextMenu
        anchor={Menu.anchor}
        items={Menu.items}
        close={() => {
          preventClosing.current === props.id ? preventClosing.current = undefined : setMenu(undefined);
        }
        }
      />
  return (<>
    {menu}
    {
      props.data.map(x =>
        <Element key={props.dataKey(x)} data={x} onClick={(e: React.MouseEvent<HTMLElement, MouseEvent>) => {
          if (Menu !== undefined) preventClosing.current = props.id;
          const items = [{
            icon: Icons.Copy,
            label: I18n.tooltip_CopyToClipboard,
            onClick: () => copyToClipboard(props.dataKey(x), I18n.toast_CopiedToClipboard)
          }, ...menuItems(x)]
          setMenu({ items, anchor: e.currentTarget });
        }}
        />
      )
    }
  </>);
}

/**
 * Display an truncated and expandable list of elements with few automatic interaction.
 * A click on an item copy its associated data.
 * A button triggers the export of the whole list as csv file.
 * Another button will copy the whole list as csv format in clipboard
 * Typically used for blocks or delegates list display,
 */
export const ButtonList = (props: ButtonListProps) => {
  const { I18n } = useContext(I18nContext);
  const [Expanded, setExpanded] = useState(false)
  const [Menu, setMenu] = useState(undefined as undefined | {
    anchor: HTMLElement,
    items: Array<ContextMenuPropsItems>
  })
  // When clicking another chip,
  // ClickAwayListener is triggered after the chip has already been set to the new value
  // preventClosing ref is a hack in order to ensure that clicking another chip will not
  // close the newly opened element.
  const preventClosing = useRef(undefined as undefined | string)
  const menu =
    Menu === undefined ? <></>
      : <ContextMenu
        anchor={Menu.anchor}
        items={Menu.items}
        close={() => {
          preventClosing.current === props.id ? preventClosing.current = undefined : setMenu(undefined);
        }
        }
      />
  const ext = props.ext || (_ => [])
  const print = props?.print || ((pkh) => <DelegatePkh pkh={pkh} />)
  const btnStyle = props?.style || ((_) => ({ height: '36px' }))
  const shorten = props?.shorten !== undefined ? props.shorten : 4
  const elements = props.data.map(x => {
    return (
      <Basics.Button
        key={x}
        style={btnStyle(x)}
        title={props.title ? props.title(x) : undefined}
        onClick={(e) => {
          if (Menu !== undefined) preventClosing.current = props.id;
          const items = [{
            icon: Icons.Copy,
            label: I18n.tooltip_CopyToClipboard,
            onClick: () => copyToClipboard(x, I18n.toast_CopiedToClipboard)
          }, ...(ext(x))]
          setMenu({ items, anchor: e.currentTarget });
        }}
      >
        {print(x)}
      </Basics.Button>
    )
  }
  )
  const hd = elements.slice(0, shorten)
  const csv = props.data.join(',')
  const csvName = 'teztale.csv' // FIXME: you can do better...
  return (
    <MUIBox sx={{ display: 'flex' }}>
      <MUIBox sx={{ flex: '1 1 0', display: 'flex', flexWrap: 'wrap', gap: 1 }}>
        {menu}
        {Expanded ? elements : hd}
        {
          props.data.length > hd.length && !Expanded && hd.length > 0
            ? <Icons.Ellispsis
              onClick={(_) => setExpanded(true)}
              size={24}
              style={{ alignSelf: 'end', cursor: 'pointer' }}
            />
            : <></>
        }
      </MUIBox >
      {props.noControls ? <></> :
        <MUIBox sx={{ flex: '0 0 auto', marginLeft: 'auto' }}>
          <IconButton icon={Icons.Download} onClick={() => download(csvName, csv)} />
          <IconButton icon={Icons.Copy} onClick={() => copyToClipboard(csv, I18n.toast_CopiedToClipboard)} />
          {
            props.data.length > hd.length
              ? Expanded
                ? <IconButton icon={Icons.MagnifierMinus} onClick={() => setExpanded(false)} />
                : <IconButton icon={Icons.MagnifierPlus} onClick={() => setExpanded(true)} />
              : <></>
          }
        </MUIBox>
      }
    </MUIBox >
  )
}

export const ButtonListDelegateExt = ({
  level,
  I18n,
  navigate,
  server,
}: {
  level?: number,
  I18n: I18n,
  navigate: (url: string) => void,
  server: string,
}) => {
  return (x: string) => [{
    icon: Icons.Delegate,
    label: I18n.label_GoToDelegatePage,
    onClick: () => navigate(Routes.path_DelegateStats
      + '#server=' + server
      + '&delegate=' + x + (level !== undefined ? '&rangeLowerBound=-20&rangeUpperBound=' + level : '')),
  }]
}

export const ButtonListLevelExt = ({
  I18n,
  navigate,
  server,
  ext,
}: {
  ext?: string,
  I18n: I18n,
  navigate: (url: string) => void,
  server: string,
}) => {
  return (x: string) => [{
    icon: Icons.Block,
    label: I18n.label_GoToLevelPage,
    onClick: () => navigate(
      Routes.path_Level
      + '#server=' + server
      + '&block=' + x
      + (ext || '')
    ),
  }]
}

export const ButtonModeToggle = (props: Omit<Basics.ButtonProps, 'onClick' | 'children'>) => {
  let [mode, setMode] = Theme.useColorScheme();
  return (
    <IconButton
      onClick={() => setMode(mode === 'dark' ? 'light' : 'dark')}
      icon={mode === 'dark' ? Icons.LightMode : Icons.DarkMode}
      {...props} />
  );
}

export const Centered = (props: BaseProps<{}>) => {
  return (
    <MUIBox display='flex' alignItems='center' justifyContent='center' {...props} />
  )
}

export const Chart = ({ aside, asideStyle, chart, title }: { aside?: JSX.Element, asideStyle?: React.CSSProperties, chart?: JSX.Element, title?: string, }) => {
  return (
    <Basics.Stack style={{ flex: '1 1 0' }} direction={'row'} spacing={4}>
      <SectionWithTitle title={title || ''} style={{ flex: '1 1 1090px' }}>
        {chart}
      </SectionWithTitle>
      {
        aside !== undefined
          ?
          <Basics.Stack direction='column' spacing={3} style={{ marginTop: 76, marginLeft: 21, maxWidth: 376, flex: '1 1 376px', fontSize: '15px', ...asideStyle }}>
            {aside}
          </Basics.Stack>
          : <></>
      }
    </Basics.Stack>
  )
}

import MUIMenu from '@mui/joy/Menu';
import { ClickAwayListener as MUIClickAwayListener } from '@mui/base/ClickAwayListener';
type ContextMenuPropsItems = {
  icon: (_: { size?: string | number | undefined }) => JSX.Element,
  label: string,
  onClick: () => void,
} | string
type ContextMenuProps = {
  anchor: HTMLElement,
  close: () => void,
  items: Array<ContextMenuPropsItems>,
}
const ContextMenu = ({ anchor, close, items }: ContextMenuProps) => {
  const content =
    items.map((item, i) => {
      if (typeof item === "string") {
        return <span style={{ verticalAlign: "bottom", ...IconButtonStyle }}>{item}</span>
      } else {
        const { icon, label, onClick } = item;
        return <IconButton key={i} style={{ borderRadius: '0' }
        } title={label} onClick={() => { close(); onClick() }} icon={icon} />
      }
    })
  return (
    <MUIMenu open={items?.length ? true : false} anchorEl={anchor} placement='top-end' style={{ padding: '0', flexDirection: 'row', zIndex: 'calc(var(--joy-zIndex-modal) + 1)' }} >
      <MUIClickAwayListener onClickAway={close}>
        <div>{content}</div>
      </MUIClickAwayListener >
    </MUIMenu>
  )
}

export const copyToClipboard = (s: string, msg: string) => {
  navigator.clipboard.writeText(s).then(() => Toaster.toast(msg))
}

export const DelegatePkh = ({
  displayPkh,
  pkh,
  short,
  endorsingPower,
}: {
  displayPkh?: (o: string, p: string) => string | JSX.Element,
  pkh: string,
  short?: boolean,
  endorsingPower?: number,
}) => {
  const endorsingSx = {
    minWidth: '33px',
    fontSize: '11.7px',
    margin: '0 0 0 8px',
    padding: '6.2px 2px 5.8px 2px',
    textAlign: 'center',
    opacity: '0.88',
    borderRadius: '3.1px',
    border: 'solid 1.6px',
  }
  const print = displayPkh || ((_, pkh) => pkh)
  return (
    <>
      <Icons.Delegate style={{ marginRight: '4px' }} />
      {print(pkh, short === false ? pkh : shortenHash(pkh))}
      {endorsingPower ? <MUIBox sx={endorsingSx}>{endorsingPower}</MUIBox> : <></>}
    </>
  )
}

/**
 * Will sort (in place) items of the `datalist` passed as arg:
 * - using the function passed as `sort` param.
 * - if not enough, using the alias sort (lexicographic order aliases).
 *   Note the if an item does not have an alias, it will appear after one with alias.
 * - if not enough, using lexicographic order.
 */
export const DelegateSelector = ({ sort, ...props }: Basics.InputProps & {
  datalist: Array<string>,
  onChange: (_: string) => void,
  sort?: (a: string, b: string) => number,
  style?: React.CSSProperties,
}) => {
  const { I18n } = useContext(I18nContext);
  const { Aliases } = useContext(DelegateAliases.AliasesContext);
  // See the function for details about the sorting function
  props.datalist.sort((a, b) => (sort ? sort(a, b) : 0) || (Aliases !== undefined ? Aliases[a] ? Aliases[b] ? Aliases[a] < Aliases[b] ? -1 : 1 : -1 : Aliases[b] ? 1 : a < b ? -1 : 1 : 0));
  const renderItem = (option: string) => {
    return (
      Aliases !== undefined && (Aliases as StringMap<string>)[option] //FIXME: make typescript happy without the cast
        ? (
          <div style={{ display: 'flex', width: '345px' }}>{/* FIXME? 345px */}
            <span>{(Aliases as StringMap<string>)[option]}</span>
            <span style={{ marginLeft: 'auto' }}><i>{shortenHash(option)}</i></span>
          </div>
        )
        : <div style={{ textAlign: 'right', width: '345px' }}>{option}</div>/* FIXME? 345px */
    )
  }

  return (
    <Basics.Input
      placeholder={I18n.placeholder_DelegateAddress}
      size={34}
      renderItem={renderItem}
      // Make the search works with both address and alias
      filterFormatter={(option: string) => Aliases ? (Aliases[option] + '/' + option) || option : option}
      {...props}
    />
  )
}

export const IconButtonStyle: React.CSSProperties = { borderRadius: '18px', padding: '0 7px', height: '36px', lineHeight: '36px', }

export const IconButton = ({ icon, style, ...props }: Omit<Basics.ButtonProps, 'children'> & { icon: (props: { size?: string | number | undefined }) => JSX.Element }) => {
  return (
    <Basics.Button
      variant='plain'
      style={{ ...IconButtonStyle, ...style }}
      {...props}>
      {icon({ size: '22px' })}
    </Basics.Button>
  );
}

export const RangeInputs = ({ lowerBound, upperBound, labels, tooltip, ...props }: {
  lowerBound: [string, React.Dispatch<React.SetStateAction<string>>],
  upperBound: [string, React.Dispatch<React.SetStateAction<string>>],
  labels: [string, string],
  tooltip?: string,
} & Omit<Basics.InputProps, 'onChange'>) => {
  const [A, setA] = lowerBound
  const [B, setB] = upperBound
  return (
    <>
      <LabelledInput tooltip={tooltip} label={labels[0]} onChange={(x: string) => setA(x)} value={A} {...props} />
      <LabelledInput tooltip={tooltip} label={labels[1]} onChange={(x: string) => setB(x)} value={B} {...props} />
    </>
  )
}

export const BlockRangeInputs = ({ ...props }: {
  lowerBound: [string, React.Dispatch<React.SetStateAction<string>>],
  upperBound: [string, React.Dispatch<React.SetStateAction<string>>],
} & Omit<Basics.InputProps, 'onChange'>) => {
  const { I18n } = useContext(I18nContext);
  const labels: [string, string] = [I18n.label_FirstBlock, I18n.label_LastBlock]
  return (
    <RangeInputs labels={labels} {...props} />
  )
}

export const Labelled = ({ label, tooltip, children, ...props }: BaseProps<{}> & { label: string, tooltip?: string }) => {
  return (
    <MUIFormControl {...props}>
      <MUIFormLabel>{label}{Basics.TooltipOption(tooltip)}</MUIFormLabel>
      {children}
    </MUIFormControl>
  )
}

type LabelledInputCtrl = Omit<Basics.InputProps, 'onChange'> & {
  cantDecrease: boolean,
  cantIncrease: boolean,
  label: string,
  beforeButtons?: JSX.Element,
  setState: React.Dispatch<React.SetStateAction<string>>,
  setStateByStep: (step: number) => void,
  tooltip?: string,
}

export const LabelledInputCtrl = ({ label, cantDecrease, cantIncrease, beforeButtons, setState, setStateByStep, tooltip, style, ...props }: LabelledInputCtrl) => {
  return (
    <Labelled label={label} tooltip={tooltip}>
      <Basics.Stack direction='row' style={{ alignItems: 'center' }}>
        <Basics.Input onChange={setState} style={{ ...style, marginRight: '8px' }} {...props} />
        {beforeButtons || <></>}
        {
          undefined !== props.min && props.datalist === undefined
            ? <IconButton disabled={cantDecrease} onClick={() => setState('' + props.min)} icon={Icons.FirstPage} />
            : <></>
        }
        <IconButton disabled={cantDecrease} onClick={() => setStateByStep(-1)} icon={Icons.ChevronLeft} />
        <IconButton disabled={cantIncrease} onClick={() => setStateByStep(+1)} icon={Icons.ChevronRight} />
        {
          undefined != props.max && props.datalist === undefined
            ? <IconButton disabled={cantIncrease} onClick={() => setState('' + props.max)} icon={Icons.LastPage} />
            : <></>
        }
      </Basics.Stack>
    </Labelled>
  )
}

export const LabelledInputCtrlNumber = ({ ...props }: Omit<LabelledInputCtrl, 'setStateByStep' | 'cantDecrease' | 'cantIncrease'>) => {
  const valueNumber = props.value === undefined ? undefined : parseIntUndefined(props.value)
  const cantDecrease = valueNumber === undefined || (undefined !== props.min && valueNumber <= props.min)
  const cantIncrease = valueNumber === undefined || (undefined !== props.max && valueNumber >= props.max)
  const setStateByStep =
    props.datalist !== undefined
      ? (op: number) => {
        const datalist: Array<string> = props.datalist as Array<string> // We just checked it 2 lines above
        props.setState(s => datalist[datalist.indexOf(s) + op])
      }
      : (op: number) => {
        props.setState(s => { const n = parseIntUndefined(s); return n === undefined ? s : (n + op).toString(); })
      }
  return <LabelledInputCtrl cantDecrease={cantDecrease} cantIncrease={cantIncrease} setStateByStep={setStateByStep} {...props} />
}

export const LabelledInputCtrlArithmetic = ({ HEAD, ...props }: Omit<LabelledInputCtrl, 'setStateByStep' | 'cantDecrease' | 'cantIncrease' | 'max' | 'min'> & { HEAD?: number }) => {
  const cantDecrease = false
  const cantIncrease = false
  const setStateByStep = (op: number) => {
    props.setState(s => Arithmetic.addNumber(s, op))
  }
  const onPlayPause =
    HEAD !== undefined && props.value !== undefined ?
      props.value.includes('HEAD')
        ? () => {
          const a = Arithmetic.parse(props.value as string)
          const b = a === undefined ? undefined : Arithmetic.updateWithHEAD(a, HEAD)
          if (b !== undefined) props.setState('' + Arithmetic.toNumber(b))
        } : () => props.setState('HEAD-1')
      : undefined
  const beforeButtons =
    <IconButton
      onClick={() => onPlayPause ? onPlayPause() : {}}
      icon={props.value?.includes('HEAD') ? Icons.Pause : Icons.Play}
      disabled={HEAD === undefined}
    />
  return <LabelledInputCtrl
    cantDecrease={cantDecrease}
    cantIncrease={cantIncrease}
    beforeButtons={beforeButtons}
    setStateByStep={setStateByStep}
    {...props}
  />
}

export const LabelledInput = ({ label, style, tooltip, containerStyle, ...props }: Basics.InputProps & { containerStyle?: React.CSSProperties, label: string, tooltip?: string }) => {
  return (
    <Labelled label={label} tooltip={tooltip} style={containerStyle}>
      <Basics.Input style={{ ...style, alignSelf: 'start', }} {...props} />
    </Labelled>
  )
}

export const FullWidthContainer = (props: BaseProps<{}>) => {
  const theme = Theme.useTheme()
  return (
    <div style={{ borderBottom: 'solid 1px ' + theme.palette.divider, ...props.style }}>
      <Basics.Container maxWidth="xl">
        {props.children}
      </Basics.Container>
    </div>
  )
}

const PageStyle: React.CSSProperties = {
  display: 'flex',
  flex: '1 1 auto', // expand if parent container has a minimum size
  margin: '0 auto',
  paddingBottom: '32px',
  paddingTop: '16px',
}
export type PageProps<T> = Omit<BaseProps<{
  error?: string,
  data?: T,
  loading?: boolean,
}>, 'children'> & { children: (data: T) => React.ReactNode }

/**
 * Page component is meant to be direct child of the main App Stack container (flex column)
 * Page does:
 * - expand if needed to fill the remaining space on the screen.
 * - apply a max width with auto left/right margins
 * - use `display: flex` in order to allow children component to properly align/justify itself
 */
export const Page = <T,>({ loading, error, data, children, style, ...props }: PageProps<T>) => {
  return (
    <Basics.Container maxWidth="xl" style={{ ...PageStyle, ...style }} {...props}>
      {
        error
          ? <SectionError error={error} />
          : loading
            ? <SectionLoading />
            : data === undefined
              ? <SectionMissingData />
              : children(data)
      }
    </Basics.Container>
  )
}

export const PageParameters = (props: BaseProps<{}>) => {
  return (
    <Basics.Stack direction={{ xs: 'column', md: 'row' }} spacing={8} alignItems={{ xs: 'center', md: 'flex-end' }} {...props} />
  )
}

export const PageWithStickySection = <T,>({ sticky, style, ...props }: PageProps<T> & { sticky: React.ReactNode }) => {
  const theme = Theme.useTheme();
  let stickyStyle: React.CSSProperties = {
    backgroundColor: theme.palette.background.body,
    paddingBottom: '16px',
    paddingTop: '16px',
    position: 'sticky',
    top: 0,
    zIndex: 1,
    ...style
  }
  return (
    <>
      <FullWidthContainer style={stickyStyle}>
        {sticky}
      </FullWidthContainer>
      <Page {...props} />
    </>
  )
}

export const Pagination = <T,>({ children, data, size }: { children: (data: Array<T>) => React.ReactNode, data: Array<T>, size: number }) => {
  const [CurrentPage, setCurrentPage] = useState(0);
  const chunks: Array<[number, Array<T>]> = [];
  for (let i = 0; i < data.length / size; i++) {
    chunks.push([i, data.slice(i * size, (i + 1) * size)]);
  };
  const btns = (arr: Array<any>) => arr.map(([i, _]) => {
    return <Basics.Button disabled={CurrentPage === i} onClick={() => CurrentPage !== i ? setCurrentPage(i) : {}}>{i}</Basics.Button>
  }
  );
  const currentData = chunks[CurrentPage] === undefined ? [] : chunks[CurrentPage][1];
  const mid0 =
    CurrentPage > 2
      ? CurrentPage < chunks.length - 4
        ? (CurrentPage === 3 ? 4 : CurrentPage === chunks.length - 5 ? chunks.length - 6 : CurrentPage)
        : chunks.length / 2
      : chunks.length / 2;
  const mid = Math.floor(mid0);
  const controllers =
    chunks.length > 10
      ? [
        ...btns(chunks.slice(0, 3)),
        <Basics.Button disabled={true} onClick={_ => { }}>...</Basics.Button>,
        ...btns([chunks[mid - 1], chunks[mid], chunks[mid + 1]]),
        <Basics.Button disabled={true} onClick={_ => { }}>...</Basics.Button>,
        ...btns(chunks.slice(-3)),
      ]
      : btns(chunks);
  return (
    <Basics.Stack spacing={2} style={{ alignItems: "center" }}>
      {children(currentData)}
      <Basics.ButtonsGroup>
        <Basics.Button onClick={() => setCurrentPage(i => Math.max(0, i - 1))}><Icons.ChevronLeft /></Basics.Button>
        {controllers}
        <Basics.Button onClick={() => setCurrentPage(i => Math.min(chunks.length - 1, i + 1))}><Icons.ChevronRight /></Basics.Button>
      </Basics.ButtonsGroup>
    </Basics.Stack>
  );
}

export const Popup = ({ children, open }: { children: React.ReactNode, open: UseState<boolean> }) => {
  return (
    <Basics.Modal open={open}>
      <Basics.Container>
        <Basics.Stack direction='column'>
          {children}
        </Basics.Stack>
      </Basics.Container>
    </Basics.Modal>
  )
}

export const PopupSection = ({
  buttonListProps,
  data,
  print,
  subtitle,
  title,
}: {
  buttonListProps?: Omit<ButtonListProps, 'id' | 'data' | 'print'>,
  data: Array<string>,
  print: (_: string) => JSX.Element,
  subtitle?: string,
  title?: string,
}) => {
  return (
    data.length > 0
      ? (
        <SectionWithTitle title={title} subtitle={subtitle} style={{ marginTop: '16px' }}>
          <ButtonList
            id={'PopupSection ' + title}
            data={data}
            print={print}
            shorten={Number.MAX_VALUE}
            {...buttonListProps}
          />
        </SectionWithTitle>
      )
      : <></>
  )
}

const SectionAux = ({ children, text }: BaseProps<{ text: string }>) => {
  return (
    <Basics.Stack style={{ alignSelf: 'center', flexGrow: '1' }} direction={'column'} spacing={2}>
      <Basics.Typography.P textAlign='center'>
        {text}
      </Basics.Typography.P>
      {children}
    </Basics.Stack>
  )
}

export const SectionError = ({ error }: { error: string }) => {
  const { I18n } = useContext(I18nContext);
  return (
    <SectionAux text={I18n.title_Error}>
      <Basics.Typography.P textAlign='center' color='red'>
        {error}
      </Basics.Typography.P>
    </SectionAux>
  )
}

export const SectionLoading = ({ children }: BaseProps<{}>) => {
  const { I18n } = useContext(I18nContext);
  return (
    <SectionAux text={I18n.title_Loading}>
      <Basics.LinearProgress />
      {children}
    </SectionAux>
  )
}

export const SectionMissingData = ({ children }: BaseProps<{}>) => {
  const { I18n } = useContext(I18nContext);
  return (
    <SectionAux text={I18n.title_NoData}>
      {children}
    </SectionAux>
  )
}

export type SectionWithTitleProps = BaseProps<{ title?: string, titleTooltip?: string, subtitle?: string }>
export const SectionWithTitle = ({ children, title, style, subtitle, titleTooltip }: SectionWithTitleProps) => {
  return (
    <Basics.Stack direction='column' spacing={2} style={{ ...style, flexGrow: 1 }}>
      <Basics.Stack direction='row' style={{ alignItems: 'baseline' }}>
        {
          title
            ? <>
              <Basics.Typography.H1>{title}</Basics.Typography.H1>
              {titleTooltip ? <Basics.Tooltip title={titleTooltip} /> : <></>}
            </>
            : <></>
        }
        {
          subtitle
            ? <Basics.Typography.H1Subtitle style={title ? { marginLeft: '16px', lineHeight: '32px' } : {}}>
              {subtitle}
            </Basics.Typography.H1Subtitle>
            : <></>
        }
      </Basics.Stack>
      {children}
    </Basics.Stack>
  )
}

const SummaryStyles = {
  headers: {
    fontWeight: 'bold',
  },
  type: {
    width: 232,
  },
  count: {
    width: 271,
    textAlign: 'right' as 'center',
  },

}

/**
 * Display a table row with the following info: [ title, count, details ].
 * `data` is sorted by weight and localeCompare
 * `count` is diplayed as a couple of numbers: `data length / total weight`
 * `details` is a ButtonList @see {@link ButtonList}
 */
export const TableSummaryRow = ({
  data,
  hideDetails,
  title,
  dataTitle,
  tooltip,
  totalData,
  totalWeight,
  weight,
  ...rest
}: {
  hideDetails?: boolean,
  title: string,
  dataTitle?: (_: string) => string,
  tooltip?: string,
  totalData?: number,
  totalWeight?: number,
  weight?: (element: string) => number,
} & Omit<ButtonListProps, 'id' | 'title'>) => {
  const pow = weight ? data.reduce((acc, value) => acc + weight(value), 0) : undefined
  const buttonListProps = { title: dataTitle, ...rest };
  const printCount = (count: number, total: number | undefined) => {
    if (total !== undefined) {
      const p = count / total
      return (
        count
        + (p < 0.1 ? "\u00A0\u00A0\u00A0" : p < 1 ? "\u00A0\u00A0" : "\u00A0")
        + '('
        + p.toLocaleString(undefined, { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 })
        + ')'
      )
    } else {
      return count
    }
  }
  const count =
    data.length === 0
      ? '—'
      : pow
        ? <>{printCount(data.length, totalData)}&nbsp;/<br />{printCount(pow, totalWeight)}&nbsp;&nbsp;</>
        : printCount(data.length, totalData)
  const sorted = weight !== undefined ? data.sort((a, b) => weight(b) - weight(a) || a.localeCompare(b)) : data
  return (
    <Basics.TR style={{ height: '59px' }}>
      <Basics.TD style={SummaryStyles.type}>
        <div style={{ display: 'flex', alignItems: 'center', }}>
          {title}
          {Basics.TooltipOption(tooltip)}
        </div>
      </Basics.TD>
      <Basics.TD style={SummaryStyles.count}>{count}</Basics.TD>
      <Basics.TD>{data.length === 0 ? '—' : <ButtonList id={title} shorten={hideDetails ? 0 : 4} data={sorted} {...buttonListProps} />}</Basics.TD>
    </Basics.TR>
  )
}

export const TableSummary = ({ labels, children }: { labels?: [string, string, string], children: Array<React.ReactNode> }) => {
  const { I18n } = useContext(I18nContext);
  let [label_1, label_2, label_3] = labels || [I18n.tableTitle_Type, I18n.tableTitle_DelegatesCount, I18n.tableTitle_DelegatesAddresses]
  return (
    <Basics.Table>
      <Basics.TableHead>
        <Basics.TR>
          <Basics.TH style={{ ...SummaryStyles.type, ...SummaryStyles.headers }}>{label_1}</Basics.TH>
          <Basics.TH style={{ ...SummaryStyles.count, ...SummaryStyles.headers }}>{label_2}</Basics.TH>
          <Basics.TH>{label_3}</Basics.TH>
        </Basics.TR>
      </Basics.TableHead>
      <Basics.TableBody>
        {children}
      </Basics.TableBody>
    </Basics.Table>
  )
}

import TimeAgo from 'timeago-react';
export const Timestamp = ({ tm }: { tm: string }) => {
  return <TimeAgo title={tm} datetime={tm} locale='en_US' />
}


import { Global, css } from "@emotion/react";
import { Toaster as SonnerToaster, toast as sonnerToast } from "sonner";
import { Button, Stack } from '@mui/joy';
export namespace Toaster {
  /**
   * A thin wrapper over Sonner Toaster component that applies a joy UI feeling
   * to the components
   * @returns
   */
  export const Toaster = () => {
    const theme = Theme.useTheme();

    return (
      <>
        <Global
          styles={css`
          .sonner-toast {
            --normal-bg: ${theme.palette.background.popup};
            --normal-border: ${theme.palette.neutral.outlinedBorder};
            --normal-text: ${theme.palette.text.primary};
            --success-bg: ${theme.vars.palette.success.solidBg};
            --success-border: ${theme.vars.palette.success.outlinedBorder};
            --success-text: ${theme.vars.palette.success.solidColor};
            --error-bg: ${theme.vars.palette.danger.solidBg};
            --error-border: ${theme.vars.palette.danger.outlinedBorder};
            --error-text: ${theme.vars.palette.danger.solidColor};
          }
        `}
        />
        <SonnerToaster
          richColors
          closeButton
          toastOptions={{
            className: "sonner-toast",
          }}
          duration={2000}
          position='bottom-left'
        />
      </>
    );
  };

  export const toast = (message: string) => {
    sonnerToast(message, {
      icon: <Icons.CheckCircleFill size={20} />,
      style: { fontFamily: 'var(--JetBrains_Mono)', fontSize: '15px' }
    });

  }

}