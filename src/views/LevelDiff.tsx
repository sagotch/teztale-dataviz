import {
  useContext,
  useEffect,
  useState,
} from 'react';
import * as TeztaleServerData from '../lib/TeztaleServerData';
import {
  type DataByLevel,
  type endorsingPower,
  extractSources,
  filterSource,
  getEndorsingPowerAtLevel,
  populateV1,
  classifyOperationsWithReceptionTimeOutput,
  classifyOperationsWithReceptionTime,
} from '../lib/PopulateSortData';
import {
  Arithmetic,
  NumberMap_map,
  parseIntUndefined,
  useHashParamString,
  useHashParamStringOption,
} from '../lib/Utils';
import * as UI from '../ui/UI';
import { I18nContext } from '../lib/I18nContext';
import { ServerContext } from '../lib/TeztaleServerAPI';
import * as Raviger from 'raviger';
import * as Routes from '../lib/Routes';

type SummaryData = {
  endorsingPower: endorsingPower,
  operations1: classifyOperationsWithReceptionTimeOutput,
  operations2: classifyOperationsWithReceptionTimeOutput,
}

type CategoryDisplay = 'missed' | 'valid' | 'lost' | 'sequestered' | 'erroneous'

const Summary = ({ category, endorsingPower, operations1: op1, operations2: op2, threshold }: {
  category: CategoryDisplay,
  endorsingPower: endorsingPower,
  operations1: SummaryData['operations1'][0] | undefined,
  operations2: SummaryData['operations2'][0] | undefined,
  threshold: number | undefined,
}) => {
  const { I18n } = useContext(I18nContext);
  if (op1 === undefined || op2 === undefined) {
    return (
      <UI.Typography.P>
        {I18n.title_NoData}
      </UI.Typography.P>
    );
  } else {
    const endorsingPower_keys = Object.keys(endorsingPower);
    const aux = (data0: classifyOperationsWithReceptionTimeOutput[0]['endorsements']) => {
      switch (category) {
        case 'erroneous':
          return data0.erroneous
        case 'lost':
          return data0.lost
        case 'missed':
          return endorsingPower_keys.flatMap(d =>
            !(data0.valid.some(({ delegate }) => delegate === d)
              || data0.lost.some(({ delegate }) => delegate === d)
              || data0.sequestered.some(({ delegate }) => delegate === d))
              ? [{ delegate: d, reception_time: null }]
              : []
          )
        case 'valid':
          return data0.valid
        case 'sequestered':
          return data0.sequestered
      }
    };
    const data1 = aux(op1.endorsements)
    const data2 = aux(op2.endorsements)
    if (data1.length === 0 && data2.length === 0) {
      return (
        <UI.Typography.P>
          {I18n.title_NoData}
        </UI.Typography.P>
      );
    } else {
      const only = (
        data1: Array<{ delegate: string, reception_time: number | null }>,
        data2: Array<{ delegate: string, reception_time: number | null }>,
      ) =>
        data1.flatMap(({ delegate: d1, reception_time: t1 }) => {
          const x = data2.find(({ delegate: d2 }) => d1 === d2);
          return (
            x === undefined
              ? [d1]
              : t1 === null
                ? []
                : threshold === undefined
                  ? []
                  : x.reception_time === null
                    ? [d1]
                    : (t1 - x.reception_time < -threshold)
                      ? [d1]
                      : []
          )
        });
      const only1 = only(data1, data2)
      const only2 = only(data2, data1)
      const both = data1.flatMap(({ delegate }) => only1.includes(delegate) ? [] : [delegate])
      const all = [
        ...only1,
        ...both,
        ...only2,
      ]
      all.sort((a, b) => endorsingPower[b] - endorsingPower[a] || a.localeCompare(b))
      const displayPkh = (pkh: string, displayed: string) => {
        return only1.some(delegate => delegate === pkh) ? <del>{displayed}</del> : displayed
      }
      const print = (pkh: string) =>
        <UI.DelegatePkh pkh={pkh} endorsingPower={endorsingPower[pkh]} displayPkh={displayPkh} />
      const style = (pkh: string) =>
        only1.includes(pkh)
          ? { backgroundColor: 'rgba(209, 88, 74, 0.4)', height: '36px' }
          : only2.includes(pkh)
            ? { backgroundColor: 'rgba(57, 165, 122, 0.4)', height: '36px' }
            : { height: '36px' }
      // key: fix a weird issue where react does not refresh all button properly by forcing re-render
      // e.g. without this fix, for a same round and non null threshold, do:
      //   Start with two different sources, then change to same sources.
      //   Chips are likely not to be all greym whereas they should (and they will if you refresh the page).
      const key = only1.concat('') + '_' + only2.concat('')
      return (
        <UI.Stack direction='column' spacing={2} style={{ ...style, flexGrow: 1 }}>
          <UI.Typography.H1>
            <UI.Stack direction='row' spacing={2}>
              {only1.length > 0 ? <span style={{ color: '#d1584a' }}>- {only1.length}</span> : <></>}
              {only2.length > 0 ? <span style={{ color: '#39a57a' }}>+ {only2.length}</span> : <></>}
              {only1.length === 0 && only2.length === 0 ? <span>— / —</span> : <></>}
            </UI.Stack>
          </UI.Typography.H1>
          <UI.ButtonList key={key} id={'Summary'} data={all} noControls={true} print={print} shorten={Number.MAX_VALUE} style={style} />
        </UI.Stack>
      )
    }
  }
}

export const LevelDiff = () => {
  const { I18n } = useContext(I18nContext);
  const { HEAD } = useContext(ServerContext)

  const [Loading, setLoading] = useState(false)
  const [FetchingData, setFetchingData] = useState(false)
  const [FetchedData, setFetchedData] = useState(undefined as [number, DataByLevel] | undefined)
  const [SourceList, setSourceList] = useState([] as Array<string>)

  const {
    DebouncedParamInput: dBlock,
    ParamInput: BlockInput,
    setParamInput: setBlockInput,
  } = useHashParamString('block', 'HEAD-1' as string)
  const [Block, setBlock] = useState(undefined as number | undefined);
  useEffect(() => {
    const base = Arithmetic.parse(dBlock)
    const updated = base !== undefined && HEAD !== undefined ? Arithmetic.updateWithHEAD(base, HEAD) : base
    setBlock(updated !== undefined ? Arithmetic.toNumber(updated) : undefined)
  }, [dBlock, HEAD])

  const {
    ParamInput: CategoryDisplay,
    setParamInput: setCategoryDisplay,
  } = useHashParamString('category', 'valid')

  const [AvailableRounds, setAvailableRounds] = useState([] as Array<number>)
  const MinRound = AvailableRounds[0] || 0
  const MaxRound = AvailableRounds[AvailableRounds.length - 1] || 0

  const [SummaryData, setSummaryData] = useState(undefined as undefined | SummaryData)

  const {
    ParamInput: RoundInput1,
    setParamInput: setRoundInput1,
  } = useHashParamString('round1', '' as string)
  const Round1_0 = parseIntUndefined(RoundInput1)
  const Round1: number =
    Round1_0 === undefined
      ? MinRound || MaxRound || 0
      : (MinRound !== undefined && Round1_0 < MinRound) ? MinRound : MaxRound !== undefined && Round1_0 > MaxRound ? MaxRound : Round1_0
  if (Round1_0 !== Round1) setRoundInput1(Round1.toString())

  const {
    ParamInput: RoundInput2,
    setParamInput: setRoundInput2,
  } = useHashParamString('round2', '' as string)
  const Round2_0 = parseIntUndefined(RoundInput2)
  const Round2: number =
    Round2_0 === undefined
      ? MaxRound || MinRound || 0
      : (MinRound !== undefined && Round2_0 < MinRound) ? MinRound : MaxRound !== undefined && Round2_0 > MaxRound ? MaxRound : Round2_0
  if (Round2_0 !== Round2) setRoundInput2(Round2.toString())

  const {
    ParamURL: Source1_0,
    setParamURL: setSource1,
  } = useHashParamStringOption('source1', '' as string)
  const Source1: string | undefined = (Source1_0 === undefined || SourceList.indexOf(Source1_0) === -1) ? SourceList.length !== 0 ? SourceList[0] : undefined : Source1_0;
  const {
    ParamURL: Source2_0,
    setParamURL: setSource2,
  } = useHashParamStringOption('source2', '' as string)
  const Source2: string | undefined = (Source2_0 === undefined || SourceList.indexOf(Source2_0) === -1) ? SourceList.length !== 0 ? SourceList[0] : undefined : Source2_0;

  const {
    ParamInput: ThresholdInput,
    setParamInput: setThresholdInput,
  } = useHashParamString('threshold', '' as string)

  useEffect(() => {
    setFetchingData(true);
    setFetchedData(undefined);
    try {
      if (Block != undefined) {
        populateV1(Block, Block).then((data: DataByLevel) => {
          setSourceList(extractSources(data[Block]));
          setFetchedData([Block, data]);
          setFetchingData(false);
        })
      } else {
        setFetchingData(false);
      }
    } catch (e) {
      console.error(e);
      setFetchingData(false);
    }
  }, [Block])

  useEffect(() => {
    if (FetchedData !== undefined) {
      setLoading(true);
      setRoundInput1('');
      setRoundInput2('');
      setSummaryData(undefined);
      const [block, data00] = FetchedData
      const availableRounds = data00[block] === undefined ? [] : (data00[block].blocks || []).map(b => b.round || 0)
      availableRounds.sort((a, b) => a - b);
      if (availableRounds.length !== 0) {
        const aux = (Source: string | undefined) => {
          return Source === undefined ? data00[block] : NumberMap_map({ [block]: data00[block] }, x => filterSource(x, Source))[block];
        }
        const endorsingPower = getEndorsingPowerAtLevel(data00[block]);
        const maxRound = availableRounds[availableRounds.length - 1];
        const operations1 = classifyOperationsWithReceptionTime(aux(Source1));
        const operations2 = classifyOperationsWithReceptionTime(aux(Source2));
        setAvailableRounds(availableRounds);
        setRoundInput1(maxRound.toString());
        setRoundInput2(maxRound.toString());
        setSummaryData({ endorsingPower, operations1, operations2 });
      }
      setLoading(false);
    }
  }, [FetchedData, Source1, Source2])

  const page_LOADING = Loading
  const sticky_LOADING = FetchingData

  const auxSelector = (
    RoundInput: string | undefined,
    setRoundInput: React.Dispatch<React.SetStateAction<string>>,
    Source: string | undefined,
    setSource: (_: string) => void,
  ) => {
    return (
      <>
        <UI.LabelledInput
          datalist={AvailableRounds.map(String)}
          disabled={AvailableRounds.length < 2}
          endDecorator={<>/ {MaxRound}</>}
          label={I18n.label_Round}
          max={MaxRound}
          min={MinRound}
          readOnly={true} // Prevent the user to use arbitrary data
          onChange={setRoundInput}
          size={1}
          styleInput={{ maxWidth: '2ex', textAlign: 'right', }}
          tooltip={I18n.tooltip_Round}
          value={RoundInput}
        />
        <UI.Labelled label={I18n.label_Source} tooltip={I18n.tooltip_Source}>
          <UI.Select value={Source} onChange={setSource}>
            {SourceList.map(s => <UI.Option key={s} value={s}>{s}</UI.Option>)}
          </UI.Select>
        </UI.Labelled>
      </>
    )
  }

  const navigate = Raviger.useNavigate()

  const sticky =
    <UI.PageParameters>
      <UI.Stack direction='row' spacing={16} style={{ flex: '1' }}>
        <UI.Stack direction='row' spacing={2}>
          <UI.LabelledInput
            label={I18n.label_Level}
            required={true}
            onChange={setBlockInput}
            size={10}
            tooltip={I18n.tooltip_Level}
            value={BlockInput}
          />
          <UI.Labelled label={I18n.label_Display} tooltip={I18n.tooltip_ChartDisplayDelegateOrSlots}>
            <UI.Select value={CategoryDisplay} onChange={setCategoryDisplay}>
              <UI.Option value='valid'>{I18n.tableTitle_Valid}</UI.Option>
              <UI.Option value='missed'>{I18n.tableTitle_Missed}</UI.Option>
              <UI.Option value='lost'>{I18n.tableTitle_Lost}</UI.Option>
              <UI.Option value='sequestred'>{I18n.tableTitle_HeldCaptive}</UI.Option>
              <UI.Option value='erroneous'>{I18n.tableTitle_Erroneous}</UI.Option>
            </UI.Select>
          </UI.Labelled>
        </UI.Stack>
        <UI.Stack direction='row' spacing={2} style={{ alignItems: 'end' }}>
          {auxSelector(RoundInput1, setRoundInput1, Source1, setSource1)}
          <UI.Typography.H1 style={{ lineHeight: '40px' }}>vs</UI.Typography.H1>
          {auxSelector(RoundInput2, setRoundInput2, Source2, setSource2)}
        </UI.Stack>
        {
          sticky_LOADING
            ? <UI.Spinner style={{
              alignSelf: 'end',
              marginBottom: 8,
              marginLeft: '16px',
            }} />
            : <></>
        }
        <UI.LabelledInput
          label={I18n.label_Threshold_MS}
          required={true}
          onChange={s => setThresholdInput(s)}
          size={10}
          value={ThresholdInput}
        />
        <UI.IconButton style={{ alignSelf: 'end', marginLeft: 'auto', marginBottom: '2px' }} icon={UI.Close} onClick={() => navigate(Routes.path_Level + window.location.hash)} />
      </UI.Stack>
    </UI.PageParameters >

  const error = Block == undefined ? I18n.error_BlockUndefined : undefined

  return (
    <UI.PageWithStickySection
      sticky={sticky}
      loading={page_LOADING}
      error={error}
      data={SummaryData}
    >
      {
        (SummaryData) => {
          return (
            <UI.Stack direction={'column'} spacing={2}>
              <Summary
                category={CategoryDisplay}
                endorsingPower={SummaryData.endorsingPower}
                operations1={SummaryData.operations1[Round1]}
                operations2={SummaryData.operations2[Round2]}
                threshold={Number.parseInt(ThresholdInput) || undefined}
              />
            </UI.Stack>
          )
        }
      }
    </UI.PageWithStickySection >
  )
}
