import {
  useContext,
} from 'react'
import {
  type Partitioned,
  type delaysDistributionWDelegate,
  timeForPercIntegrationWEndorsingPower,
  ByLevel,
} from '../lib/PopulateSortData';
import {
  type StringMap,
  useHashParamString,
  parseIntUndefined,
  average,
} from '../lib/Utils';
import * as UI from '../ui/UI';
import { I18nContext } from '../lib/I18nContext';

/**
 * [result, didNotReachThreshold]
 */
type Result = Partitioned<[Array<{ block: number, t: number }>, Array<number>]>

const Chart = (props: { data: Result }) => {
  const { I18n } = useContext(I18nContext);
  const [scheme, _] = UI.useColorScheme();
  const colors = UI.chartTheme[scheme];
  const data: Array<{ block: number; pre_endorsement: number | undefined; endorsement: number | undefined; }> = []
  const findEndorsement = (block: number) => {
    let endorsement = props.data.endorsements[0].find((x) => x.block == block);
    return endorsement ? endorsement.t : undefined;
  }
  props.data.pre_endorsements[0].forEach(({ block, t }) => {
    data.push({ block: block, pre_endorsement: t, endorsement: findEndorsement(block) })
  })
  props.data.pre_endorsements[1].forEach((block) => {
    data.push({ block: block, pre_endorsement: undefined, endorsement: findEndorsement(block) })
  })
  data.sort((a, b) => a.block - b.block)
  const ticks = data.map(x => x.block)
  const xAxis = {
    key: 'block',
    label: I18n.chartLabel_BlockNumber(''),
    ticks: ticks,
  }
  const yAxis = [
    { key: 'pre_endorsement', label: I18n.chartLabel_Preendorsements, round: UI.AxisRoundPreEndorsment, color: colors.preendorsement },
    { key: 'endorsement', label: I18n.chartLabel_Endorsements, round: UI.AxisRoundEndorsment, color: colors.endorsement },
  ]
  const yAxisLabel = I18n.chartLabel_MinimumTimeMS
  const preendo = data.flatMap(({ pre_endorsement }) => pre_endorsement === undefined ? [] : [pre_endorsement])
  const endo = data.flatMap(({ endorsement }) => endorsement === undefined ? [] : [endorsement])
  const tooltipLabelFormatter = (value: string) => <UI.Chip>{I18n.chartLabel_BlockNumber(value)}</UI.Chip>
  const buttonStyle = { alignSelf: 'flex-start' }
  const chart =
    <UI.LineChart
      data={data}
      tooltipLabelFormatter={tooltipLabelFormatter}
      xAxis={xAxis}
      yAxisLabel={yAxisLabel}
      yAxis={yAxis}
    />
  const avg_preendo =
    preendo.length !== 0
      ? <UI.Labelled label={I18n.label_PreendorsementsAverage}>
        <UI.Button style={buttonStyle} onClick={() => { }}>{`${(average(preendo)/1000).toFixed(2)} s`}</UI.Button>{ /* FIXME: Do not use a button if it's not clickable */}
      </UI.Labelled> : undefined
  const avg_endo =
    endo.length !== 0
      ? <UI.Labelled label={I18n.label_EndorsementsAverage} >
        <UI.Button style={buttonStyle} onClick={() => { }}>{`${(average(endo)/1000).toFixed(2)} s`}</UI.Button>{ /* FIXME: Do not use a button if it's not clickable */}
      </UI.Labelled>
      : undefined
  const aside = avg_preendo === undefined && avg_endo === undefined ? undefined : <>{avg_preendo}{avg_endo}</>
  return (
    <UI.Chart aside={aside} chart={chart} title={I18n.title_EndorsementReceptionDelays} />
  )
}

const DidNotReach = ({ data }: { data: Result }) => {
  const { I18n } = useContext(I18nContext);
  const print = { print: (l: string) => <UI.BlockLevel level={l} /> }
  return (
    <UI.SectionWithTitle title={I18n.title_BlocksDidNotReach}>
      <UI.TableSummary labels={[I18n.tableTitle_OperationType, I18n.tableTitle_Count, I18n.tableTitle_BlockLevels]}>
        <UI.TableSummaryRow
          data={data.pre_endorsements[1].map(x => x.toString())}
          title={I18n.tableTitle_Preendorsements}
          tooltip={I18n.tooltip_Preendorsements}
          {...print}
        />
        <UI.TableSummaryRow
          data={data.endorsements[1].map(x => x.toString())}
          title={I18n.tableTitle_Endorsements}
          tooltip={I18n.tooltip_Endorsements}
          {...print}
        />
      </UI.TableSummary>
    </UI.SectionWithTitle>
  )
}

export const DelayToConsensus = () => {

  const { I18n } = useContext(I18nContext);
  const {
    ParamInput: Threshold0,
    setParamInput: setThreshold0,
  } = useHashParamString('threshold', '66' as string)
  const Threshold = parseIntUndefined(Threshold0) || 66

  const formatData = (Data: UI.TeztaleDataBase | undefined): Result | undefined => {
    if (undefined != Data) {
      try {
        const { distribution: distribution0, endorsingPower, data } = Data
        const rounds0: [string, number][] = Object.entries(data).flatMap(([level, levelData]) => {
          if (levelData.blocks === undefined) return [] as [string, number][];
          const hasSuccessor = (data[+level + 1]?.blocks || []).flatMap(b => b.predecessor ? [b.predecessor] : [])
          const candidateRounds = levelData.blocks.flatMap(b => hasSuccessor.includes(b.hash) ? [b.round || 0] : [])
          if (candidateRounds.length !== 0) { return [[level, Math.max(...candidateRounds)] as [string, number]] }
          else { return [[level, Math.max(...(Object.entries(levelData.blocks).map(([level, { round }]) => round || 0)))] as [string, number]] }
        })
        const rounds = Object.fromEntries(rounds0);
        const filterDistribution = (d: ByLevel<delaysDistributionWDelegate>) =>
          Object.fromEntries(Object.entries(d).map(([level, data]) => [level, data[rounds[level]]]))
        const distribution = {
          pre_endorsements: filterDistribution(distribution0.pre_endorsements),
          endorsements: filterDistribution(distribution0.endorsements),
        }
        const aux = (d: StringMap<delaysDistributionWDelegate[0]>) => timeForPercIntegrationWEndorsingPower(Threshold, d, endorsingPower)
        const results = { pre_endorsements: aux(distribution.pre_endorsements), endorsements: aux(distribution.endorsements) };
        return results;
      } catch (e) {
        console.error(e);
        return undefined;
      }
    } else {
      return undefined;
    }
  }

  const formatDataDeps = [Threshold]

  const sticky =
    <>
      <UI.LabelledInput size={2} label={I18n.label_PercentageThreshold} onChange={setThreshold0} value={Threshold0} />
    </>

  return (
    <UI.BlocksRangePage sticky={sticky} formatData={formatData} formatDataDeps={formatDataDeps} dataMsec={true}>
      {
        (data) => (
          <UI.Stack direction={'column'} spacing={2}>
            <Chart data={data} />
            <DidNotReach data={data} />
          </UI.Stack>
        )
      }
    </UI.BlocksRangePage >
  )
}
