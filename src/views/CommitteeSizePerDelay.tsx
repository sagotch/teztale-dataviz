import {
  useContext, useEffect, useState,
} from 'react'
import {
  type ByLevel,
  type ByPublicKeyHash,
  type delaysDistributionWDelegate,
} from '../lib/PopulateSortData';
import {
  average,
  sumValues,
  type StringMap,
  type NumberMap,
  range,
  unique,
  useHashParamString,
  parseIntUndefined,
} from '../lib/Utils';
import * as UI from '../ui/UI';
import { I18nContext } from '../lib/I18nContext';

type Result = {
  lowerBound: number,
  upperBound: number,
  data: Array<{
    time: number, // Time elapsed (in seconds)
    round: number,
    average: number,
    variation: [number, number],
  }>
}

type CategoryDisplayType = 'endorsements' | 'preendorsements'

// FIXME: series might be fix for a given set of levels if we can zoom in/out the chart
const SeriesPercentageIntegration = (lowerBound: number, upperBound: number, data0: ByLevel<delaysDistributionWDelegate>, endorsingPower: ByLevel<ByPublicKeyHash<number>> | undefined) => {
  const data: Result['data'] = [];
  range(lowerBound, upperBound).forEach((time) => {
    const pILevel: ByLevel<Array<number>> = {}; // For each threshold time, map each `level` with the `percentage` of data received at each round
    const allRounds: Array<number> = []; // All rounds present in the data set for the given serie 
    Object.entries(data0).forEach(([levelKey, rounds]) => {
      const level = parseInt(levelKey)
      pILevel[level] = [];
      rounds.forEach((delays, round) => {
        if (allRounds.indexOf(round) == -1) allRounds.push(round);
        var roundValue = 0;
        Object.entries(delays).forEach(([delegate, delay]) => {
          if (delay <= time) {
            roundValue += endorsingPower ? endorsingPower[level][delegate] : 1
          }
        });
        const numberOfSlots = endorsingPower ? sumValues(endorsingPower[level]) : Object.keys(delays).length
        const pI_level = roundValue / numberOfSlots;
        if (!isNaN(pI_level)) pILevel[level][round] = pI_level
      })
    });
    allRounds.forEach((round) => {
      const values: Array<number> = []
      Object.values(pILevel).forEach((percentages) => {
        const p = percentages[round]
        if (p !== undefined) values.push(p)
      })
      data.push({
        time,
        round,
        average: values.length > 0 ? average(values) : 0,
        variation: values.length > 0 ? [Math.min(...values), Math.max(...values)] : [0, 0],
      });
    })
  })
  return { lowerBound, upperBound, data }
}

// convert 0.dddddddddd... number to dd.dd 
const normalize = (n: number) => Math.trunc(n * 10000) / 100

const Chart = (props: { data: Result, display: CategoryDisplayType, endorsingPower: boolean }) => {
  const { I18n } = useContext(I18nContext);
  const [scheme, _] = UI.useColorScheme();
  const colors = UI.chartTheme[scheme];

  const GroupByTime: NumberMap<NumberMap<{ average: number, variation: [number, number] }>> = {}
  props.data.data.forEach(({ time, round, average, variation }) => {
    if (undefined == GroupByTime[time]) GroupByTime[time] = {}
    GroupByTime[time][round] = { average: normalize(average), variation: [normalize(variation[0]), normalize(variation[1])] }
  })
  const data = Object.entries(GroupByTime).map(([time, values]) => { return { time: parseInt(time), values } })
  data.sort((a, b) => a.time - b.time)
  const ticks = range(props.data.lowerBound, props.data.upperBound)
  let xAxis = {
    key: 'time',
    label: I18n.chartLabel_ThresholdTimeS,
    ticks: ticks,
  }
  const yAxis = unique(data.flatMap(({ values }) => Object.keys(values))).map((key) => {
    return {
      area: { key: 'values[' + key + '].variation', label: I18n.chartLabel_RoundVariation(key), round: parseInt(key) },
      line: { key: 'values[' + key + '].average', label: I18n.chartLabel_RoundAverage(key) },
    }
  })
  const yAxisLabel = I18n.chartLabel_PercentageReceivedBeforeThreshold
  const references: Array<Exclude<UI.Reference, { x: number, y?: never, }>> =
    props.endorsingPower ? [{ y: 66, label: I18n.chartLabel_Quorum, color: colors.quorum }] : []
  return (
    <UI.AreaChart
      data={data}
      references={references}
      xAxis={xAxis}
      yAxisLabel={yAxisLabel}
      yAxis={yAxis}
    />
  )
}

export const CommitteeSizePerDelay = () => {

  const { I18n } = useContext(I18nContext);

  const {
    ParamInput: LowerBoundInput,
    setParamInput: setLowerBoundInput,
  } = useHashParamString('lowerBound', '0' as string)

  const {
    ParamInput: UpperBoundInput,
    setParamInput: setUpperBoundInput,
  } = useHashParamString('upperBound', '8' as string)

  const {
    ParamInput: CategoryDisplay,
    setParamInput: setCategoryDisplay,
  } = useHashParamString('category', 'preendorsements' as CategoryDisplayType)

  const {
    ParamInput: WithEndorsingPower,
    setParamInput: setWithEndorsingPower,
  } = useHashParamString('endorsingPower', 'true' as 'true' | 'false')

  const LowerBound = parseIntUndefined(LowerBoundInput)
  const UpperBound = parseIntUndefined(UpperBoundInput)

  const formatData = (Data: UI.TeztaleDataBase | undefined): Result | undefined => {
    if (undefined != Data && LowerBound !== undefined && UpperBound !== undefined) {
      try {
        const { distribution, endorsingPower } = Data
        const aux = (d: StringMap<delaysDistributionWDelegate>) => SeriesPercentageIntegration(LowerBound, UpperBound, d, WithEndorsingPower == 'false' ? undefined : endorsingPower)
        const results = CategoryDisplay == 'preendorsements' ? aux(distribution.pre_endorsements) : aux(distribution.endorsements);
        return results;
      } catch (e) {
        console.error(e);
        return undefined;
      }
    } else {
      return undefined;
    }
  }

  const formatDataDeps = [LowerBound, UpperBound, CategoryDisplay, WithEndorsingPower]

  const title =
    CategoryDisplay == 'preendorsements'
      ? I18n.title_ProportionReceivedBeforeThresholdTimePreendorsements
      : I18n.title_ProportionReceivedBeforeThresholdTimeEndorsements

  return (
    <UI.BlocksRangePage formatData={formatData} formatDataDeps={formatDataDeps}>
      {
        (data) => (
          <UI.SectionWithTitle title={title}>
            <UI.PageParameters>
              <UI.LabelledInputCtrlNumber size={2} label={I18n.label_LowerBoundThreshold} setState={setLowerBoundInput} value={LowerBoundInput} />
              <UI.LabelledInputCtrlNumber size={2} label={I18n.label_UpperBoundThreshold} setState={setUpperBoundInput} value={UpperBoundInput} />
              <UI.Labelled label={I18n.label_Display}>
                <UI.Select value={CategoryDisplay} onChange={setCategoryDisplay}>
                  <UI.Option value='preendorsements'>{I18n.option_Preendorsements}</UI.Option>
                  <UI.Option value='endorsements'>{I18n.option_Endorsements}</UI.Option>
                </UI.Select>
              </UI.Labelled>
              <UI.Labelled label={I18n.label_EndorsingPower}>
                <UI.Select value={WithEndorsingPower} onChange={setWithEndorsingPower}>
                  <UI.Option value='true'>{I18n.generic_Yes}</UI.Option>
                  <UI.Option value='false'>{I18n.generic_No}</UI.Option>
                </UI.Select>
              </UI.Labelled>
            </UI.PageParameters>
            <Chart data={data} display={CategoryDisplay} endorsingPower={WithEndorsingPower !== 'false'} />
          </UI.SectionWithTitle>
        )
      }
    </UI.BlocksRangePage >
  )
}
