import {
  useContext,
} from 'react'
import {
  type ByLevel,
  type ByPublicKeyHash,
} from '../lib/PopulateSortData';
import {
  type delaysDistributionWDelegate,
} from '../lib/PopulateSortData';
import {
  sumValues,
  type StringMap,
  useHashParamString,
  parseIntUndefined,
  useHashParamInt,
} from '../lib/Utils';
import * as UI from '../ui/UI';
import { I18nContext } from '../lib/I18nContext';

type Result = Array<{ block: number; round: number; pI: number; }>

type DisplayType = 'endorsements' | 'preendorsements'

const percIntegrationWEndorsingPower = (threshold: number, data: ByLevel<delaysDistributionWDelegate>, endorsingPower: ByLevel<ByPublicKeyHash<number>>) => {
  var result: Array<{ block: number, round: number, pI: number }> = [];
  Object.entries(data).forEach(([levelKey, rounds]) => {
    const level = parseInt(levelKey)
    try {
      Object.entries(rounds).forEach(([round, delayPerDelegate]) => {
        var card_valid_tcible = 0;
        Object.entries(delayPerDelegate).forEach(([delegate, delay]) => {
          if (delay <= threshold) {
            card_valid_tcible += endorsingPower[level][delegate];
          }
        });
        const numberOfSlots = sumValues(endorsingPower[level])
        const pI_level = card_valid_tcible / numberOfSlots;
        if (!isNaN(pI_level)) {
          result.push({ block: level, round: parseInt(round), pI: pI_level })
        }
      });
    } catch (e) { console.error(level, e) }
  });
  return result
}

const Chart = (props: { data: Result, display: DisplayType }) => {
  const { I18n } = useContext(I18nContext);
  const [scheme, _] = UI.useColorScheme();
  const colors = UI.chartTheme[scheme];

  const data0: ByLevel<Array<number>> = {}
  const allRounds: Array<number> = []
  props.data.forEach(({ block, pI, round }) => {
    if (allRounds.indexOf(round) == -1) allRounds.push(round);
    if (data0[block] === undefined) data0[block] = [];
    data0[block][round] = pI * 100
  })
  const data = Object.entries(data0).map(([block, values]) => { return { block: parseInt(block), values } });
  data.sort((a, b) => a.block - b.block)
  allRounds.sort((a, b) => a - b)
  const ticks = data.map(x => x.block)
  let xAxis = {
    key: 'block',
    label: I18n.chartLabel_BlockNumber(''),
    ticks: ticks,
  }
  const yAxis = allRounds.map((key) => { return { key: 'values[' + key + ']', label: 'Round ' + key, round: key } })
  const yAxisLabel = I18n.chartLabel_PercentageReceivedBeforeThreshold
  const references: Array<Exclude<UI.Reference, { x: number, y?: never, }>> =
    [{ y: 66, label: I18n.chartLabel_Quorum, color: colors.quorum }]
  return (
    <UI.ScatterChart
      data={data}
      references={references}
      xAxis={xAxis}
      yAxisLabel={yAxisLabel}
      yAxis={yAxis}
      tooltipLabelFormatter={_ => <></>}
    />
  )
}

export const CommitteeSizePerLevel = () => {
  const { I18n } = useContext(I18nContext);

  const {
    ParamInput: ThresholdInput0,
    setParamInput: setThresholdInput0,
  } = useHashParamString('threshold', '5' as string)
  const Threshold = parseIntUndefined(ThresholdInput0) || 5

  const {
    ParamInput: CategoryDisplay,
    setParamInput: setCategoryDisplay,
  } = useHashParamString('category', 'preendorsements' as DisplayType)

  const formatData = (Data: UI.TeztaleDataBase | undefined): Result | undefined => {
    if (undefined != Data) {
      try {
        let { distribution, endorsingPower } = Data
        let aux = (d: StringMap<delaysDistributionWDelegate>) => percIntegrationWEndorsingPower(Threshold, d, endorsingPower)
        let results = CategoryDisplay == 'preendorsements' ? aux(distribution.pre_endorsements) : aux(distribution.endorsements);
        return results;
      } catch (e) {
        console.error(e);
        return undefined;
      }
    } else {
      return undefined;
    }
  }

  const formatDataDeps = [Threshold, CategoryDisplay]

  const sticky =
    <>
      <UI.LabelledInput size={2} label={I18n.label_TemporalThreshold} onChange={setThresholdInput0} value={ThresholdInput0} />
      <UI.Labelled label={I18n.label_Display}>
        <UI.Select value={CategoryDisplay} onChange={setCategoryDisplay}>
          <UI.Option value='preendorsements'>{I18n.option_Preendorsements}</UI.Option>
          <UI.Option value='endorsements'>{I18n.option_Endorsements}</UI.Option>
        </UI.Select>
      </UI.Labelled>
    </>

  const title =
    CategoryDisplay == 'preendorsements'
      ? I18n.title_ProportionReceivedBeforeThresholdTimePreendorsements
      : I18n.title_ProportionReceivedBeforeThresholdTimeEndorsements

  return (
    <UI.BlocksRangePage sticky={sticky} formatData={formatData} formatDataDeps={formatDataDeps}>
      {
        (data) => (
          <UI.SectionWithTitle title={title}>
            <Chart data={data} display={CategoryDisplay} />
          </UI.SectionWithTitle>
        )
      }
    </UI.BlocksRangePage >
  )
}
