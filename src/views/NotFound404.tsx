import * as UI from '../ui/UI';
import TeztaleLogo from '../assets/teztale.svg';

export const NotFound404 = (props: {}) => {
  return (
    <UI.Stack direction='column' spacing={2}>
      <UI.Typography.H1>Not Found!</UI.Typography.H1>
      <UI.Centered>
        <img src={TeztaleLogo} className="logo" width={480} alt="Teztale logo" />
      </UI.Centered>
    </UI.Stack>
  )
}
