import {
  useContext,
  useState,
} from 'react'
import {
  type ByLevel,
  type delaysDistributionWDelegate,
  type classifyOperationsForDelegateOutput,
  type Partitioned,
  classifyOperationsForDelegate,
  extractDelegates,
} from '../lib/PopulateSortData';
import {
  average,
  unique,
  useHashParamString,
} from '../lib/Utils';
import * as UI from '../ui/UI';
import { I18nContext } from '../lib/I18nContext';
import * as TeztaleServerAPI from '../lib/TeztaleServerAPI';
import { PublicKeyHash } from '../lib/TeztaleServerData';
import * as Raviger from 'raviger';

type Result000 = {
  average: number,
  delegate: number,
}
type Result0 = ByLevel<Array<Result000>>
type Result = {
  chart: Partitioned<Result0> | undefined,
  delegates: Array<PublicKeyHash>,
  delegatesSortingFunction: (a: PublicKeyHash, b: PublicKeyHash) => number,
  summary: classifyOperationsForDelegateOutput | undefined,
}

type Result000Diff = {
  averageDiff: number,
  delegate: number,
}

type ChartData = {
  allRounds: Array<number>,
  data: Array<{
    block: number, // Block level
    values: {
      endorsement: Result000Diff | undefined, // Last attestation produced by the delegate for this level
      rounds: Array<Result000Diff>, // Pre-attestations produced by the delegate for each round for this level
    };
  }>
}

const PrepareData = (data00: Exclude<Result['chart'], undefined>): ChartData => {
  const data0: ByLevel<{ endorsement: Result000Diff | undefined, rounds: Array<Result000Diff> }> = {}
  const allRounds: Array<number> = []
  const levels: Array<number> = []
  const aux = (roundData: Result000): Result000Diff => {
    return {
      delegate: roundData.delegate,
      averageDiff: roundData.delegate - roundData.average
    }
  }
  Object.keys(data00.pre_endorsements).forEach((level) => { if (levels.indexOf(+level) == -1) levels.push(+level); })
  Object.keys(data00.endorsements).forEach((level) => { if (levels.indexOf(+level) == -1) levels.push(+level); })
  levels.forEach((level) => {
    const preendo: Array<Result000> | undefined = data00.pre_endorsements[level];
    const endo = data00.endorsements[level];
    (preendo || []).forEach((roundData, round) => {
      if (allRounds.indexOf(round) == -1) allRounds.push(round);
      if (data0[level] === undefined) data0[level] = {
        endorsement: endo ? aux(endo[endo.length - 1]) : undefined,
        rounds: [],
      };
      data0[level].rounds[round] = aux(roundData)
    })
  })
  allRounds.sort((a, b) => a - b)
  const data = Object.entries(data0).map(([block, values]) => { return { block: parseInt(block), values } })
  data.sort((a, b) => a.block - b.block)
  return { allRounds, data, }
}

const ChartAux = (props: {
  data: ChartData,
  dataKey: string,
  references?: Array<Exclude<UI.Reference, { x: number, y?: never, }>>
}) => {
  const { I18n } = useContext(I18nContext);
  const ticks = props.data.data.map(x => x.block)
  let xAxis = {
    key: 'block',
    label: I18n.chartLabel_BlockNumber(''),
    ticks: ticks,
  }
  const yAxis = [
    { key: 'values.endorsement.' + props.dataKey, label: I18n.chartLabel_Endorsement, round: UI.AxisRoundEndorsment },
    ...props.data.allRounds.map((key) => { return { key: 'values.rounds[' + key + '].' + props.dataKey, label: 'Round ' + key, round: key } })
  ]
  const yAxisLabel = I18n.chartLabel_DelayMS
  return (
    <UI.ScatterChart
      data={props.data.data}
      xAxis={xAxis}
      yAxisLabel={yAxisLabel}
      yAxis={yAxis}
      tooltipLabelFormatter={x => <>{I18n.label_Level} {x}</>}
      references={props.references}
    />
  )
}

const ChartDelays = (props: { data: ChartData }) => {
  return <ChartAux dataKey='delegate' {...props} />
}

const ChartDeviation = (props: { data: ChartData }) => {
  return <ChartAux dataKey='averageDiff' {...props} references={[{ label: '', y: 0 }]} />
}

const Summary = ({ data }: { data: classifyOperationsForDelegateOutput }) => {
  const { I18n } = useContext(I18nContext);
  const { ServerURL } = useContext(TeztaleServerAPI.ServerContext);
  const navigate = Raviger.useNavigate();
  const print = (l: string) => <UI.BlockLevel level={l} />
  const ext = UI.ButtonListLevelExt({ server: ServerURL, I18n, navigate })
  const common = { print, ext }
  return (
    <UI.TableSummary labels={[I18n.tableTitle_Type, I18n.tableTitle_Count, I18n.tableTitle_BlockLevels]} >
      <UI.TableSummaryRow title={I18n.tableTitle_Missed} tooltip={I18n.tooltip_MissedBlock} data={data.endorsements.missed.map(String)} {...common} />
      <UI.TableSummaryRow title={I18n.tableTitle_Valid} tooltip={I18n.tooltip_ValidBlock} data={data.endorsements.valid.map(String)} {...common} hideDetails />
      <UI.TableSummaryRow title={I18n.tableTitle_Lost} tooltip={I18n.tooltip_LostBlock} data={data.endorsements.lost.map(String)} {...common} />
      <UI.TableSummaryRow title={I18n.tableTitle_HeldCaptive} tooltip={I18n.tooltip_HeldCaptiveBlock} data={data.endorsements.sequestered.map(String)} {...common} />
      <UI.TableSummaryRow title={I18n.tableTitle_Erroneous} tooltip={I18n.tooltip_ErroneousBlock} data={data.endorsements.erroneous.map(String)} {...common} />
    </UI.TableSummary>
  )
}

export const DelegateStats = () => {

  const { I18n } = useContext(I18nContext);

  const [Delegates, setDelegates] = useState([] as Array<string>)

  const {
    DebouncedParamInput: Delegate,
    ParamInput: DelegateInput,
    setParamInput: setDelegateInput,
  } = useHashParamString('delegate', '' as string)

  const {
    ParamInput: DisplayType,
    setParamInput: setDisplayType,
  } = useHashParamString('display', 'delay' as 'delay' | 'deviation')

  const DelegateIsUndefined = Delegate === undefined || Delegate === ''

  const formatData = (Data: UI.TeztaleDataBase | undefined): Result | undefined => {
    if (undefined !== Data) {
      try {
        const { distribution, endorsingPower, data } = Data
        const summary = DelegateIsUndefined ? undefined : classifyOperationsForDelegate(data, Delegate)

        const cumulatedEndorsingPower: { [key: PublicKeyHash]: number } = {}
        Object.values(endorsingPower).forEach(x => Object.entries(x).forEach(([delegate, power]) => {
          cumulatedEndorsingPower[delegate] = (cumulatedEndorsingPower[delegate] || 0) + power
        }));
        const delegatesSortingFunction = (a: PublicKeyHash, b: PublicKeyHash) => cumulatedEndorsingPower[b] - cumulatedEndorsingPower[a];

        const delegates = unique(Object.values(endorsingPower).flatMap(extractDelegates));

        const aux = (data0: ByLevel<delaysDistributionWDelegate>, addr: string): Result0 => {
          const data: Result0 = {}
          Object.entries(data0).forEach(([levelKey, rounds]) => {
            const level = parseInt(levelKey)
            const value: Array<Result000> = []
            rounds.forEach((delays, round) => {
              value[round] = { delegate: delays[addr], average: average(Object.values(delays)) }
            })
            data[level] = value
          })
          return data;

        }

        const chart =
          DelegateIsUndefined
            ? undefined
            : { pre_endorsements: aux(distribution.pre_endorsements, Delegate), endorsements: aux(distribution.endorsements, Delegate) }

        return {
          chart,
          delegates,
          delegatesSortingFunction,
          summary,
        };
      } catch (e) {
        console.error(e);
        return undefined;
      }
    } else {
      return undefined;
    }
  }

  const formatDataDeps = [Delegate]

  return (
    <UI.BlocksRangePage formatData={formatData} formatDataDeps={formatDataDeps} dataMsec>
      {
        ({ delegates, delegatesSortingFunction, chart, summary }) => {
          setDelegates(delegates)
          const chartData = chart === undefined ? undefined : PrepareData(chart)
          return (
            <UI.Stack direction='column' spacing={2}>
              <UI.PageParameters>
                <UI.Labelled label={I18n.label_Delegate}>
                  <UI.DelegateSelector
                    onChange={setDelegateInput}
                    value={DelegateInput}
                    datalist={Delegates}
                    style={{ alignSelf: 'start' }}
                    sort={delegatesSortingFunction}
                  />
                </UI.Labelled>
                <UI.Labelled label={I18n.label_Display}>
                  <UI.Select value={DisplayType} onChange={setDisplayType}>
                    <UI.Option value='delay'>{I18n.option_DelegateStatsDelay}</UI.Option>
                    <UI.Option value='deviation'>{I18n.option_DelegateStatsDeviation}</UI.Option>
                  </UI.Select>
                </UI.Labelled>
              </UI.PageParameters>
              <UI.SectionWithTitle
                title={DisplayType == 'delay' ? I18n.title_DelegateDelays : I18n.title_DelegateDeviations}
                titleTooltip={I18n.toottip_DelegateTitle}>
                {
                  chartData == undefined
                    ? DelegateIsUndefined
                      ? <UI.Typography.P>{I18n.error_DelegateUndefined}</UI.Typography.P>
                      : <UI.Typography.P>{I18n.title_NoData}</UI.Typography.P>
                    : DisplayType == 'delay' ? <ChartDelays data={chartData} /> : <ChartDeviation data={chartData} />
                }
              </UI.SectionWithTitle>
              <UI.SectionWithTitle title={I18n.title_Operations}>
                {
                  summary == undefined
                    ? DelegateIsUndefined ? <>{I18n.error_DelegateUndefined}</> : <>{I18n.title_NoData}</>
                    : <Summary data={summary} />
                }
              </UI.SectionWithTitle >
            </UI.Stack>
          )
        }
      }
    </UI.BlocksRangePage >
  )
}
