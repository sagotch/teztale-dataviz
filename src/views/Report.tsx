import { useContext, useState } from 'react';
import {
  type DataByLevel,
  type endorsingPower,
  delaysDistributionWDelegate,
  timeForPercIntegrationWEndorsingPower0,
  Partitioned,
} from '../lib/PopulateSortData';
import * as TeztaleServerData from '../lib/TeztaleServerData';
import * as TeztaleServerAPI from '../lib/TeztaleServerAPI';
import * as Utils from '../lib/Utils';
import * as UI from '../ui/UI';
import { I18nContext } from '../lib/I18nContext';
import * as Raviger from 'raviger';

type T<A> = {
  timestamp?: A,
  round: A,
  // (reception + validation) delay
  validation_delay?: A,
  // (application - validation) delay
  application_delay?: A,
  // Attestation rate (number of attestation slots fulfilled by attestations included by the successor block)
  attestation_rate: A,
  // time we received from a delegate a pre-attestation but not the corresponding attestation (or the reverse)
  pre_attestation_only: A,
  attestation_only: A,
  // Distribution of delays to reach the quorum (66% of attestation)
  delay_66?: A,
  // Minimal block time still allowing to receive 90% of endorsements
  delay_90?: A,
  delay_66_pre?: A,
  delay_90_pre?: A,
  delay_validation_pqc?: A,
  delay_validation_qc?: A,
  delay_pqc_qc?: A,
}

const computeBlockStats = (
  level: number,
  data: DataByLevel[0],
  distribution: Partitioned<delaysDistributionWDelegate>,
  endorsingPower: endorsingPower,
  block: TeztaleServerData.Block,
): T<number> & { level: number } => {
  const round = block.round || 0;
  const endorsements = (data.endorsements || []).flatMap(e => {
    const operations = (e.operations || []).flatMap(o => o.round == round ? [o] : [])
    if (operations.length > 0) {
      return [{ ...e, operations }]
    } else {
      return []
    }
  });
  const timestamp = block.timestamp !== undefined ? new Date(block.timestamp).getTime() : undefined;
  const validation_times = (block.reception_times || []).flatMap(r => r.validation ? [new Date(r.validation).getTime()] : []);
  const application_times = (block.reception_times || []).flatMap(r => r.application ? [new Date(r.application).getTime()] : []);
  const validation_time = validation_times.length === 0 ? undefined : Math.min(...validation_times);
  const application_time = application_times.length === 0 ? undefined : Math.min(...application_times);
  const validation_delay = (timestamp && validation_time) ? validation_time - timestamp : undefined;
  const application_delay = (application_time && validation_time) ? application_time - validation_time : undefined;
  const delay_66 = timeForPercIntegrationWEndorsingPower0(0.66, distribution.endorsements[round], endorsingPower);
  const delay_90 = timeForPercIntegrationWEndorsingPower0(0.90, distribution.endorsements[round], endorsingPower);
  const delay_66_pre = timeForPercIntegrationWEndorsingPower0(0.66, distribution.pre_endorsements[round], endorsingPower);
  const delay_90_pre = timeForPercIntegrationWEndorsingPower0(0.90, distribution.pre_endorsements[round], endorsingPower);
  const delay_validation_pqc = validation_delay && delay_66_pre ? delay_66_pre - validation_delay : undefined;
  const delay_validation_qc = validation_delay && delay_66 ? delay_66 - validation_delay : undefined;
  const delay_pqc_qc = delay_66 && delay_66_pre ? delay_66 - delay_66_pre : undefined;

  let pre_attestation_only = 0;
  let attestation_only = 0;
  let attestation_rate0 = 0;
  let total_endorsingPower = 0;
  endorsements.forEach(o => {
    total_endorsingPower += o.endorsing_power || 1;
    if (o.operations.some(oo => (!oo.kind) && ((oo.included_in_blocks?.length || 0) > 0))) {
      attestation_rate0 += o.endorsing_power || 1;
    }
    if (o.operations.some(oo => oo.kind)) {
      if (!o.operations.some(oo => !oo.kind)) pre_attestation_only++;
    } else {
      if (o.operations.some(oo => !oo.kind)) attestation_only++;
    }
  });
  const attestation_rate = total_endorsingPower ? attestation_rate0 / total_endorsingPower : 0;
  return {
    level,
    timestamp,
    round,
    validation_delay,
    application_delay,
    attestation_rate,
    pre_attestation_only,
    attestation_only,
    delay_66,
    delay_90,
    delay_66_pre,
    delay_90_pre,
    delay_validation_pqc,
    delay_validation_qc,
    delay_pqc_qc,
  }
}

const computeLevelStats = (
  level: number,
  data: DataByLevel[0],
  distribution: Partitioned<delaysDistributionWDelegate>,
  endorsingPower: endorsingPower,
): Array<T<number> & { level: number }> => {
  return (data.blocks || []).map(b => computeBlockStats(level, data, distribution, endorsingPower, b));
}

type numbers = {
  min?: number,
  max?: number,
  avg?: number,
  var?: number,
  shards?: { [key: string]: Array<number> },
}

type formattedData = {
  data: T<numbers>;
  numbers: T<number>;
}

const formatData0 = (acc0: formattedData | undefined, data0: UI.TeztaleDataBase): formattedData => {
  const empty = () => ({
    min: undefined,
    max: undefined,
    avg: undefined,
    var: undefined,
    shards: undefined
  });
  const {
    data,
    distribution,
    endorsingPower,
  } = data0
  const acc: formattedData = acc0 || {
    data: {
      round: empty(),
      validation_delay: empty(),
      application_delay: empty(),
      attestation_rate: empty(),
      pre_attestation_only: empty(),
      attestation_only: empty(),
      delay_66_pre: empty(),
      delay_90_pre: empty(),
      delay_validation_pqc: empty(),
      delay_validation_qc: empty(),
      delay_pqc_qc: empty(),
    },
    numbers: {
      round: 0,
      validation_delay: 0,
      application_delay: 0,
      attestation_rate: 0,
      pre_attestation_only: 0,
      attestation_only: 0,
      delay_66: 0,
      delay_90: 0,
      delay_66_pre: 0,
      delay_90_pre: 0,
      delay_validation_pqc: 0,
      delay_validation_qc: 0,
      delay_pqc_qc: 0,
    },
  }
  const stats = Object.entries(data).map(([level, data]) => computeLevelStats(
    +level,
    data,
    { endorsements: distribution.endorsements[+level], pre_endorsements: distribution.pre_endorsements[+level] },
    endorsingPower[+level])
  ).flat();
  const agg_timestamp: Array<[number, number]> = [];
  const agg_round: Array<[number, number]> = [];
  const agg_validation_delay: Array<[number, number]> = [];
  const agg_application_delay: Array<[number, number]> = [];
  const agg_attestation_rate: Array<[number, number]> = [];
  const agg_pre_attestation_only: Array<[number, number]> = [];
  const agg_attestation_only: Array<[number, number]> = [];
  const agg_delay_66: Array<[number, number]> = [];
  const agg_delay_90: Array<[number, number]> = [];
  const agg_delay_66_pre: Array<[number, number]> = [];
  const agg_delay_90_pre: Array<[number, number]> = [];
  const agg_delay_validation_pqc: Array<[number, number]> = [];
  const agg_delay_validation_qc: Array<[number, number]> = [];
  const agg_delay_pqc_qc: Array<[number, number]> = [];
  stats.forEach(s => {
    if (s === undefined) return;
    const {
      level,
      timestamp,
      round,
      validation_delay,
      application_delay,
      attestation_rate,
      pre_attestation_only,
      attestation_only,
      delay_66,
      delay_90,
      delay_66_pre,
      delay_90_pre,
      delay_validation_pqc,
      delay_validation_qc,
      delay_pqc_qc,
    } = s;
    if (timestamp !== undefined) agg_timestamp.push([level, timestamp]);
    if (round !== undefined) agg_round.push([level, round]);
    if (validation_delay !== undefined) agg_validation_delay.push([level, validation_delay]);
    if (application_delay !== undefined) agg_application_delay.push([level, application_delay]);
    if (attestation_rate !== undefined) agg_attestation_rate.push([level, attestation_rate]);
    if (pre_attestation_only !== undefined) agg_pre_attestation_only.push([level, pre_attestation_only]);
    if (attestation_only !== undefined) agg_attestation_only.push([level, attestation_only]);
    if (delay_66 !== undefined) agg_delay_66.push([level, delay_66]);
    if (delay_90 !== undefined) agg_delay_90.push([level, delay_90]);
    if (delay_66_pre !== undefined) agg_delay_66_pre.push([level, delay_66_pre]);
    if (delay_90_pre !== undefined) agg_delay_90_pre.push([level, delay_90_pre]);
    if (delay_validation_pqc !== undefined) agg_delay_validation_pqc.push([level, delay_validation_pqc]);
    if (delay_validation_qc !== undefined) agg_delay_validation_qc.push([level, delay_validation_qc]);
    if (delay_pqc_qc !== undefined) agg_delay_pqc_qc.push([level, delay_pqc_qc]);
  });
  // TODO: someone with math skills needs to review this.
  // TODO: check if we need to use sample or population case
  // See https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Parallel_algorithm
  const computeVariance = (
    acc_n: number,
    acc_avg: number,
    acc_m2: number,
    agg_n: number,
    agg_avg: number,
    agg_m2: number
  ) => {
    const n = agg_n + acc_n;
    if (n < 2) return 0;
    const delta = acc_avg - agg_avg
    const m2 = acc_m2 + agg_m2 + Math.pow(delta, 2) * acc_n * agg_n / n
    return m2 / (n - 1);
  }
  const aux = (field: keyof formattedData['numbers'], agg: Array<[number, number]>, shard: ((_: number) => string)): numbers => {
    if (agg.length === 0) {
      return acc.data[field] || empty()
    } else {
      const acc_min = acc.data[field]?.min;
      const min0 = acc_min === undefined ? Math.min(...agg.map(x => x[1])) : Math.min(acc_min, ...agg.map(x => x[1]));
      const min = isNaN(min0) ? undefined : min0;
      const acc_max = acc.data[field]?.max;
      const max0 = acc_max === undefined ? Math.max(...agg.map(x => x[1])) : Math.max(acc_max, ...agg.map(x => x[1]));
      const max = isNaN(max0) ? undefined : max0;
      const agg_avg = agg.length === 0 ? undefined : Utils.average(agg.map(x => x[1]));
      const agg_m2 = agg_avg === undefined ? undefined : agg.reduce((acc, x) => acc + Math.pow(x[1] - agg_avg, 2), 0);
      const agg_n = agg.length;
      const acc_avg = acc.data[field]?.avg;
      const acc_var = acc.data[field]?.var;
      const acc_n = acc.numbers[field] || 0;
      const acc_m2 = acc_var === undefined ? undefined : acc_n > 1 ? acc_var * (acc_n - 1) : 0;
      const avg =
        agg_avg === undefined
          ? acc_avg
          : acc_avg === undefined
            ? agg_avg
            : (agg_avg * agg_n + acc_avg * acc_n) / (agg_n + acc_n)
      const variance =
        acc_n === 0
          ? agg_m2 === undefined ? undefined : agg_m2 / (agg_n - 1)
          : acc_avg === undefined || acc_m2 === undefined || agg_avg === undefined || agg_m2 === undefined
            ? undefined
            : computeVariance(acc_n, acc_avg, acc_m2, agg_n, agg_avg, agg_m2);
      const shards = acc.data[field]?.shards || {};
      agg.forEach(k => {
        const s = shard(k[1]);
        if (shards[s] === undefined) shards[s] = [];
        if (!shards[s].includes(k[0])) shards[s].push(k[0]);
        // shards[s] = (shards[s] || 0) + 1;
      })
      return {
        avg,
        min,
        max,
        var: variance,
        shards,
      }
    }
  };
  const shardRawNumber = (x: number) => x.toString();
  const shardMillisecondToSecond = (t: number) => Math.ceil(t / 1000).toString();
  const shardPercent10 = (t: number) => (Math.floor(t * 10) * 10).toString();
  const d = {
    round: aux('round', agg_round, shardRawNumber),
    validation_delay: aux('validation_delay', agg_validation_delay, shardMillisecondToSecond),
    application_delay: aux('application_delay', agg_application_delay, shardMillisecondToSecond),
    attestation_rate: aux('attestation_rate', agg_attestation_rate, shardPercent10),
    pre_attestation_only: aux('pre_attestation_only', agg_pre_attestation_only, shardRawNumber),
    attestation_only: aux('attestation_only', agg_attestation_only, shardRawNumber),
    delay_66: aux('delay_66', agg_delay_66, shardMillisecondToSecond),
    delay_90: aux('delay_90', agg_delay_90, shardMillisecondToSecond),
    delay_66_pre: aux('delay_66_pre', agg_delay_66_pre, shardMillisecondToSecond),
    delay_90_pre: aux('delay_90_pre', agg_delay_90_pre, shardMillisecondToSecond),
    delay_validation_pqc: aux('delay_validation_pqc', agg_delay_validation_pqc, shardMillisecondToSecond),
    delay_validation_qc: aux('delay_validation_qc', agg_delay_validation_qc, shardMillisecondToSecond),
    delay_pqc_qc: aux('delay_pqc_qc', agg_delay_pqc_qc, shardMillisecondToSecond),
  };
  const n = {
    round: agg_round.length + acc.numbers.round,
    validation_delay: agg_validation_delay.length + (acc.numbers.validation_delay || 0),
    application_delay: agg_application_delay.length + (acc.numbers.application_delay || 0),
    attestation_rate: agg_attestation_rate.length + acc.numbers.attestation_rate,
    pre_attestation_only: agg_pre_attestation_only.length + (acc.numbers.pre_attestation_only || 0),
    attestation_only: agg_attestation_only.length + (acc.numbers.attestation_only || 0),
    delay_66: agg_delay_66.length + (acc.numbers.delay_66 || 0),
    delay_90: agg_delay_90.length + (acc.numbers.delay_90 || 0),
    delay_66_pre: agg_delay_66_pre.length + (acc.numbers.delay_66_pre || 0),
    delay_90_pre: agg_delay_90_pre.length + (acc.numbers.delay_90_pre || 0),
    delay_validation_pqc: agg_delay_validation_pqc.length + (acc.numbers.delay_validation_pqc || 0),
    delay_validation_qc: agg_delay_validation_qc.length + (acc.numbers.delay_validation_qc || 0),
    delay_pqc_qc: agg_delay_pqc_qc.length + (acc.numbers.delay_pqc_qc || 0),
  }
  return { data: d, numbers: n }
}

const color_warning = '#d7991e'
const color_error = '#e85252'

const color = (
  warning: (_: number) => boolean,
  error: (_: number) => boolean,
) => (x: number) => {
  return error(x) ? color_error : warning(x) ? color_warning : undefined
}

const printColored = (
  print: (_: number) => string,
  color0: (_: number) => (string | undefined),
) => (x: number) => {
  const txt = print(x);
  const color = color0(x);
  if (undefined !== color) {
    return <span style={{ color }}>{txt}</span>
  } else {
    return txt
  }
}

const print_int = (x: number) => Math.trunc(x).toString()
const print_raw = (x: number) => x.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })
const print_percentage = (x: number) => x.toLocaleString(undefined, { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 })
const print_thousandth = (x: number) => (x / 100).toLocaleString(undefined, { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 })
const print_milliseconds = (x: number) => `${(x / 1000).toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })}s`
const print_seconds = (x: number) => `${x}s`
const lte = (x: number) => (y: number) => y <= x
const gte = (x: number) => (y: number) => y >= x

const color_Round = color(gte(1), gte(4));
const color_ValidationDelay = color(gte(2000), gte(3000));
const color_ApplicationDelay = color(gte(2000), gte(3000));
const color_AttestationRate = color(lte(0.90), lte(0.75));
const color_AttestationOnly = color(gte(2), gte(5));
const color_Delay66 = color(gte(5000), gte(7000));
const color_Delay90 = color(gte(6000), gte(8000));
const color_NOP = color((_) => false, (_) => false);

const ShardsBarChart = (props: {
  color: (_: number) => string | undefined,
  data: Array<[number, Array<number>]>,
  label: string,
  print: (_: number) => string,
  source?: string,
}) => {
  const { I18n } = useContext(I18nContext);
  const { ServerURL } = useContext(TeztaleServerAPI.ServerContext);
  const navigate = Raviger.useNavigate();
  const [scheme, _] = UI.useColorScheme();
  const colors = UI.chartTheme[scheme]
  const maxTick = Math.max(...(props.data.map(([name, _]) => name)));
  const data = props.data.map(([k, v]) => {
    const color = props.color(k) || colors.endorsement;
    const name = props.print(k);
    const value = v.length;
    return { name, value, color }
  });
  const xAxis = {
    key: 'name',
    label: 'xAxis',
    ticks: Utils.range(0, maxTick + 1),
  }
  const tooltip = (value0: string) => {
    const k = parseInt(value0);
    const x = props.data.find(([k0, _]) => k === k0);
    return x === undefined
      ? <></>
      : <UI.Card style={{ backgroundColor: colors.background }}>{props.label} {props.print(x[0])}: {x[1].length}</UI.Card>
  }
  const [PopupContent, setPopupContent] = useState(<></>)
  const PopupOpen = useState(false);
  const Popup = <UI.Popup open={PopupOpen}>{PopupContent}</UI.Popup>
  const onClick = ({ tick }: { tick: string }) => {
    const k = parseInt(tick);
    const x = props.data.find(([k0, _]) => k === k0);
    const title = props.label + ': ' + props.print(k);
    const print = (x: string) => <UI.BlockLevel level={x} />
    if (x !== undefined) {
      const ext = UI.ButtonListLevelExt({ server: ServerURL, I18n, navigate, ext: props.source ? "&source=" + props.source : '' })
      setPopupContent(<UI.PopupSection title={title} data={x[1].map(x => x.toString())} print={print} buttonListProps={{ ext }} />);
      PopupOpen[1](true)
    }
  }
  return (
    <>
      <UI.BarChart
        onClick={onClick}
        data={data}
        height={240}
        margin={{ top: 16, right: 16, bottom: 16, left: 16, }}
        tooltip={tooltip}
        xAxis={xAxis}
      />
      {Popup}
    </>
  )
}

const ShardsChart = ({
  data: data0,
  color,
  label,
  print,
  source,
}: {
  data: { [key: string]: Array<number> },
  color: (_: number) => (string | undefined),
  label: string,
  print: (_: number) => string,
  source?: string,
}) => {
  const data = Object.entries(data0).map(([key, value]) => [parseInt(key), value] as [number, Array<number>]);
  const [scheme, _] = UI.useColorScheme();
  const colors = UI.chartTheme[scheme]
  data.sort((([k1, v1], [k2, v2]) => k1 > k2 ? 1 : -1));
  const [checked, setChecked] = useState(data.map(_ => true));
  return (
    <UI.Stack direction='row' spacing={0} style={{ backgroundColor: UI.chartTheme[scheme].background }} alignItems='center'>
      <UI.Container style={{ flex: '0 0 max-content', margin: '0' }} >
        {data.map(([key, value], i) => {
          const backgroundColor = color(key) || colors.endorsement;
          return (
            <UI.Stack direction='row' spacing={2} alignItems='center'>
              <UI.Checkbox checked={checked[i]} onChange={() => setChecked(checked.map((x, j) => i === j ? !x : x))} />
              <span style={{ width: '1rem', height: '1rem', display: 'inline-block', backgroundColor }}></span>
              <span>{print(key)}</span>
              <span>{value.length.toString()}</span>
            </UI.Stack>
          )
        })}
      </UI.Container>
      <UI.Container style={{ flex: '1 1 100%', margin: '0' }} >
        <ShardsBarChart label={label} color={color} data={data.filter((_, i) => checked[i])} print={print} source={source} />
      </UI.Container>
    </UI.Stack>
  )
}

const DataHeaders = ({ label }: { label?: boolean }) => {
  const { I18n } = useContext(I18nContext);
  return (
    <UI.TableHead>
      <UI.TR>
        {label === undefined || label ? <UI.TH style={{ width: '310px' }}></UI.TH> : <></>}
        <UI.TH style={{ textAlign: 'right' }}>{I18n.tableTitle_Average}</UI.TH>
        <UI.TH style={{ textAlign: 'right' }}>{I18n.tableTitle_Min}</UI.TH>
        <UI.TH style={{ textAlign: 'right' }}>{I18n.tableTitle_Max}</UI.TH>
        <UI.TH style={{ textAlign: 'right' }}>{I18n.tableTitle_StandardDeviation}</UI.TH>
      </UI.TR>
    </UI.TableHead>
  )
}

const DataSummary = ({
  label,
  data,
  field,
  color,
  print: print0,
}: {
  label?: string,
  data: formattedData,
  field: keyof formattedData['data'],
  color: (_: number) => (string | undefined),
  print: (_: number) => string,
}) => {
  const variance = data.data[field]?.var;
  const avg = data.data[field]?.avg;
  const min = data.data[field]?.min;
  const max = data.data[field]?.max;
  const p = (x: number) => avg === undefined || avg === 0 ? '' : ' (' + (x / avg).toLocaleString(undefined, { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2 }) + ')';
  const print = (x: number | undefined) => x === undefined ? '' : printColored(print0, color)(x);
  const pprint = (x: number | undefined) => x === undefined ? <></> : <>{print0(x)} {p(x)}</>;
  return (
    <UI.TR>
      {undefined === label ? <></> : (
        <UI.TD style={{ textAlign: 'left' }}>
          <span
            id={field + '-summary'}
            style={{ cursor: 'pointer' }}
            onClick={(_) => document?.getElementById(field + '-details')?.scrollIntoView()}>
            {label}
          </span>
        </UI.TD>
      )
      }
      <UI.TD>{print(avg)}</UI.TD>
      <UI.TD>{print(min)}</UI.TD>
      <UI.TD>{print(max)}</UI.TD>
      <UI.TD>{variance === undefined ? undefined : pprint(Math.sqrt(variance))}</UI.TD>
    </UI.TR>
  )
}

const DataDetails = ({
  label,
  data,
  field,
  color,
  colorShardCategory,
  print,
  printShardCategory,
  source,
}: {
  label: string,
  data: formattedData,
  field: keyof formattedData['data'],
  color: (_: number) => (string | undefined),
  colorShardCategory?: (_: number) => (string | undefined),
  print: (_: number) => string,
  printShardCategory?: (_: number) => string,
  source?: string,
}) => {
  const shards = data.data[field]?.shards;
  return (
    <UI.Stack spacing={2}>
      <UI.Typography.H1>
        <UI.Stack direction='row' spacing={2}>
          <span id={field + '-details'}>{label} ({data.numbers[field]})</span>
          <span
            style={{ cursor: 'pointer' }}
            onClick={(_) => document?.getElementById(field + '-summary')?.scrollIntoView()}>
            <UI.ArrowUpward />
          </span>
        </UI.Stack>
      </UI.Typography.H1>
      <UI.Table style={{ textAlign: 'right' }}>
        <DataHeaders label={false} />
        <UI.TableBody>
          <DataSummary data={data} field={field} color={color} print={print} />
        </UI.TableBody>
      </UI.Table>
      {
        shards !== undefined
          ? <ShardsChart label={label} data={shards} color={colorShardCategory || color} print={printShardCategory || print} source={source} />
          : <></>
      }
    </UI.Stack>
  )
}

export const Report = () => {

  const { I18n } = useContext(I18nContext);


  const formatData = (acc: formattedData | undefined, Data: UI.TeztaleDataBase | undefined) => {
    if (Data !== undefined) {
      return formatData0(acc, Data)
    }
  }

  const wrapMSColorForSShard = (color: ((x: number) => string | undefined)) => (x: number) => color(x * 1000)

  return (
    <UI.BlocksRangePageChunks formatData={formatData} formatDataDeps={[]} dataMsec={true}>
      {
        (data, fst, lst, source) => (
          <UI.Stack direction={'column'} spacing={2}>
            <UI.Table style={{ textAlign: 'right' }} stripe={true} hover={true}>
              <DataHeaders />
              <UI.TableBody>
                <DataSummary label={I18n.label_Round} data={data} field='round' color={color_Round} print={print_raw} />
                <DataSummary label={I18n.label_ValidationDelay} data={data} field='validation_delay' color={color_ValidationDelay} print={print_milliseconds} />
                <DataSummary label={I18n.label_ApplicationDelay} data={data} field='application_delay' color={color_ApplicationDelay} print={print_milliseconds} />
                <DataSummary label={I18n.label_AttestationRate} data={data} field='attestation_rate' color={color_AttestationRate} print={print_percentage} />
                <DataSummary label={I18n.label_PreAttestationOnly} data={data} field='pre_attestation_only' color={color_NOP} print={print_raw} />
                <DataSummary label={I18n.label_AttestationOnly} data={data} field='attestation_only' color={color_AttestationOnly} print={print_raw} />
                <DataSummary label={I18n.label_Delay66_pre} data={data} field='delay_66_pre' color={color_NOP} print={print_milliseconds} />
                <DataSummary label={I18n.label_Delay90_pre} data={data} field='delay_90_pre' color={color_NOP} print={print_milliseconds} />
                <DataSummary label={I18n.label_Delay66} data={data} field='delay_66' color={color_Delay66} print={print_milliseconds} />
                <DataSummary label={I18n.label_Delay90} data={data} field='delay_90' color={color_Delay90} print={print_milliseconds} />
                <DataSummary label={I18n.label_Delay_validation_pqc} data={data} field='delay_validation_pqc' color={color_NOP} print={print_milliseconds} />
                <DataSummary label={I18n.label_Delay_validation_qc} data={data} field='delay_validation_qc' color={color_NOP} print={print_milliseconds} />
                <DataSummary label={I18n.label_Delay_pqc_qc} data={data} field='delay_pqc_qc' color={color_NOP} print={print_milliseconds} />
              </UI.TableBody>
            </UI.Table>
            <DataDetails source={source} label={I18n.label_Round} data={data} field='round' color={color_Round} print={print_raw} printShardCategory={print_int} />
            <DataDetails source={source} label={I18n.label_ValidationDelay} data={data} field='validation_delay' color={color_ValidationDelay} colorShardCategory={wrapMSColorForSShard(color_ValidationDelay)} print={print_milliseconds} printShardCategory={print_seconds} />
            <DataDetails source={source} label={I18n.label_ApplicationDelay} data={data} field='application_delay' color={color_ApplicationDelay} colorShardCategory={wrapMSColorForSShard(color_ApplicationDelay)} print={print_milliseconds} printShardCategory={print_seconds} />
            <DataDetails source={source} label={I18n.label_AttestationRate} data={data} field='attestation_rate' color={color_AttestationRate} print={print_percentage} printShardCategory={print_thousandth} />
            <DataDetails source={source} label={I18n.label_PreAttestationOnly} data={data} field='pre_attestation_only' color={color_NOP} print={print_raw} printShardCategory={print_raw} />
            <DataDetails source={source} label={I18n.label_AttestationOnly} data={data} field='attestation_only' color={color_AttestationOnly} print={print_raw} printShardCategory={print_raw} />
            <DataDetails source={source} label={I18n.label_Delay66_pre} data={data} field='delay_66_pre' color={color_NOP} print={print_milliseconds} printShardCategory={print_seconds} />
            <DataDetails source={source} label={I18n.label_Delay90_pre} data={data} field='delay_90_pre' color={color_NOP} print={print_milliseconds} printShardCategory={print_seconds} />
            <DataDetails source={source} label={I18n.label_Delay66} data={data} field='delay_66' color={color_Delay66} colorShardCategory={wrapMSColorForSShard(color_Delay66)} print={print_milliseconds} printShardCategory={print_seconds} />
            <DataDetails source={source} label={I18n.label_Delay90} data={data} field='delay_90' color={color_Delay90} colorShardCategory={wrapMSColorForSShard(color_Delay90)} print={print_milliseconds} printShardCategory={print_seconds} />
            <DataDetails source={source} label={I18n.label_Delay_validation_pqc} data={data} field='delay_validation_pqc' color={color_NOP} print={print_milliseconds} printShardCategory={print_seconds} />
            <DataDetails source={source} label={I18n.label_Delay_validation_qc} data={data} field='delay_validation_qc' color={color_NOP} print={print_milliseconds} printShardCategory={print_seconds} />
            <DataDetails source={source} label={I18n.label_Delay_pqc_qc} data={data} field='delay_pqc_qc' color={color_NOP} print={print_milliseconds} printShardCategory={print_seconds} />
          </UI.Stack>
        )
      }
    </UI.BlocksRangePageChunks >
  )
}