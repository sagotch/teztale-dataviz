import React, {
  useContext,
  useEffect,
  useState,
} from 'react';
import * as TeztaleServerAPI from '../lib/TeztaleServerAPI';
import * as TeztaleServerData from '../lib/TeztaleServerData';
import {
  type DataByLevel,
  type Partitioned,
  type delaysDistributionWDelegate,
  type endorsingPower,
  type classifyOperationsOutput,
  classifyOperations,
  delaysDistributionOfOperationsAtLevelWDelegate,
  extractDelegates,
  extractSources,
  filterSource,
  getEndorsingPowerAtLevel,
  getInfoBlock,
  populateV1,
  reproposalOutput,
  reproposal,
} from '../lib/PopulateSortData';
import {
  type NumberMap,
  Arithmetic,
  NumberMap_map,
  parseIntUndefined,
  quorum,
  range,
  sumValues,
  useHashParamString,
  useHashParamStringOption,
  Batch,
  shortenHash,
  StringMap,
} from '../lib/Utils';
import * as UI from '../ui/UI';
import { I18nContext } from '../lib/I18nContext';
import { ServerContext } from '../lib/TeztaleServerAPI';
import * as DelegateAliases from '../lib/DelegateAliases';
import * as Raviger from 'raviger';
import * as Routes from '../lib/Routes';
import { I18n } from '../lib/I18n_interface';
import { ProtocolsContext } from '../lib/Protocols';

type PublicKeyHash = TeztaleServerData.PublicKeyHash

type ChartData = {
  applicationBlock: Array<number>, // Minimum application delay for each round
  distribution: Partitioned<delaysDistributionWDelegate>,
  endorsingPower: endorsingPower,
  nextBlock?: number,
  timestamps: Array<number>, // Declared timestamp for each round
  validationBlock: Array<number>, // Minimum validation delay for each round
}

type SummaryData = {
  endorsingPower: endorsingPower,
  operations: classifyOperationsOutput,
  timestamps: NumberMap<number>, // Associate each round with its block declared timestamp
}

type BlockData = {
  blocks: Array<
    Omit<TeztaleServerData.Block, 'predecessor'>
    & {
      predecessor?: Omit<TeztaleServerData.Block, 'reception_times'>
      & { maxRound: number }
      & { level: number }
    }
  >,
  cycle?: TeztaleServerData.Cycleinfo,
  prevCycle?: TeztaleServerData.Cycleinfo,
  endorsingPower: endorsingPower,
  reproposal: reproposalOutput,
  missingBlocks: Array<TeztaleServerData.MissingBlock>,
}

type DisplayType = 'delegates' | 'slots'

const DelegatesPopupSection = ({ title, data, endorsingPower }: { title: string, data: Array<string>, endorsingPower: endorsingPower }) => {
  const print = (x: string) => printDelegate0(endorsingPower, x)
  return <UI.PopupSection subtitle={title} data={data} print={print} />
}

const Chart = ({
  delegate,
  distribution,
  endorsingPower,
  LowerBoundState: [LowerBound, setLowerBound],
  minDelayValidation,
  minDelayApplication,
  nextBlockReception,
  nextRoundReception,
  StepState: [Step, setStep],
  timestamp,
  UpperBoundState: [UpperBound, setUpperBound],
  withEndorsingPower,
}: {
  delegate?: PublicKeyHash,
  distribution: Partitioned<delaysDistributionWDelegate[number]>,
  endorsingPower: endorsingPower,
  LowerBoundState: [string, React.Dispatch<React.SetStateAction<string>>],
  minDelayApplication?: number,
  minDelayValidation?: number,
  nextBlockReception?: number,
  nextRoundReception?: number,
  StepState: [string, React.Dispatch<React.SetStateAction<string>>],
  timestamp: number,
  UpperBoundState: [string, React.Dispatch<React.SetStateAction<string>>],
  withEndorsingPower: boolean,
}) => {
  const step = parseIntUndefined(Step) || 100;
  const { I18n } = useContext(I18nContext);
  const [scheme, _] = UI.useColorScheme();
  const colors = UI.chartTheme[scheme];
  const incr =
    withEndorsingPower
      ? (delegate: string) => endorsingPower[delegate]
      : (_: string) => 1
  const endorsements_entries = Object.entries(distribution.endorsements)
  const pre_endorsements_entries = Object.entries(distribution.pre_endorsements)
  const PreQuorum = quorum(pre_endorsements_entries, endorsingPower)
  const Quorum = quorum(endorsements_entries, endorsingPower)
  const [minBoundary, maxBoundary] = Arithmetic.computeRange([
    LowerBound === undefined ? undefined : Arithmetic.parse(LowerBound),
    UpperBound === undefined ? undefined : Arithmetic.parse(UpperBound),
  ]);
  const filterMin = minBoundary !== undefined ? (x: number) => x >= minBoundary : (x: number) => true
  const filterMax = maxBoundary !== undefined ? (x: number) => x <= maxBoundary : (x: number) => true
  const filter = (x: number) => filterMin(x) && filterMax(x)
  const aux = (entries: Array<[string, number]>) => {
    const result: NumberMap<Array<PublicKeyHash>> = {}
    entries.forEach(([delegate, delay]) => {
      if (filter(delay)) {
        const r = Math.trunc(delay / step) * step
        if (!result[r]) { result[r] = [] }
        result[r].push(delegate)
      }
    })
    Object.values(result).forEach(data => {
      data.sort((a, b) => endorsingPower[b] - endorsingPower[a] || a.localeCompare(b))
    })
    return result;
  }
  const endorsementsDelegates = aux(endorsements_entries);
  const pre_endorsementsDelegates = aux(pre_endorsements_entries);
  const endorsements = NumberMap_map(endorsementsDelegates, (delegates) => sumValues(delegates.map(incr)));
  const pre_endorsements = NumberMap_map(pre_endorsementsDelegates, (delegates) => sumValues(delegates.map(incr)));
  const data0: Array<{ range: number; pre_endorsement: number, endorsement: number, }> = []
  const findEndorsement = (range: number) => {
    const endorsement = Object.entries(endorsements).find(([r, _]) => parseInt(r) == range);
    return endorsement ? endorsement[1] : undefined;
  }
  Object.entries(pre_endorsements).forEach(([r, value]) => {
    const range = parseInt(r)
    data0.push({ range: range, pre_endorsement: value, endorsement: findEndorsement(range) || 0 })
  })
  Object.entries(endorsements).forEach(([r, value]) => {
    const range = parseInt(r)
    if (!data0[range]) {
      data0.push({ range: range, pre_endorsement: 0, endorsement: value })
    }
  })
  data0.sort((a, b) => a.range - b.range);
  let maxTick = maxBoundary !== undefined ? Math.trunc(maxBoundary / step) : data0.length !== 0 ? data0[data0.length - 1].range / step + 1 : 1
  let minTick = minBoundary !== undefined && minBoundary >= step ? Math.trunc(minBoundary / step) - 1 : data0.length !== 0 ? data0[0].range / step : 0
  const withinTicks = (x: number) => x >= minTick * step && x <= maxTick * step
  // Some events are not necessarily contained within ticks defined by (pre)attestation operations,
  // but need to presented on the graph. In that case, we use adjustTicks in order to make sure
  // that ticks contain these values.
  // Make sure that data that should be included only if within ticks are tested after all the ones
  // that can modify these ticks.
  const adjustTicks = (x0: number) => {
    const x = Math.trunc(x0 / step);
    if (x < minTick) minTick = x;
    else if (x > maxTick) maxTick = x;
  }
  const yAxis = [
    { key: 'pre_endorsement', label: I18n.chartLabel_Preendorsements, round: UI.AxisRoundPreEndorsment },
    { key: 'endorsement', label: I18n.chartLabel_Endorsements, round: UI.AxisRoundEndorsment },
  ]
  const yAxisLabel = withEndorsingPower ? I18n.chartLabel_NbSlots : I18n.chartLabel_NbDelegates
  const references: Array<UI.Reference> = []
  if (minDelayValidation !== undefined && filter(minDelayValidation)) {
    adjustTicks(minDelayValidation);
    references.push({ x: minDelayValidation, label: I18n.chartLabel_EndOfValidation, color: 'rgb(232, 82, 82)', dash: true })
  }
  if (minDelayApplication !== undefined && filter(minDelayApplication)) {
    adjustTicks(minDelayApplication);
    references.push({ x: minDelayApplication, label: I18n.chartLabel_EndOfApplication, color: 'rgb(232, 82, 82)' })
  }
  if (nextRoundReception !== undefined && withinTicks(nextRoundReception)) {
    references.push({ x: nextRoundReception, label: I18n.chartLabel_NextRound, color: colors.grid, dash: true })
  }
  if (nextBlockReception !== undefined && withinTicks(nextBlockReception)) {
    references.push({ x: nextBlockReception, label: I18n.chartLabel_NextBlockReception, color: colors.grid })
  }
  if (PreQuorum !== undefined && filter(PreQuorum)) {
    // No need to adjust ticks, quorum is always reached when you receive at least one attestation operation
    references.push({ x: PreQuorum, label: I18n.chartLabel_PreQuorum, color: colors.quorum, dash: true })
  }
  if (Quorum !== undefined && filter(Quorum)) {
    // No need to adjust ticks, quorum is always reached when you receive at least attestation operation
    references.push({ x: Quorum, label: I18n.chartLabel_Quorum, color: colors.quorum })
  }
  if (delegate !== undefined) {
    const e = endorsements_entries.find(([d, _]) => d === delegate);
    const p = pre_endorsements_entries.find(([d, _]) => d === delegate);
    if (p !== undefined && filter(p[1])) {
      // Dirty hack in order to make it visible on preendorsement background
      references.push({ x: p[1], label: '', color: colors.background, width: 4 });
      references.push({ x: p[1], label: '', color: colors.preendorsement, width: 4, dash: true });
    }
    if (e !== undefined && filter(e[1])) {
      // Dirty hack in order to make it visible on endorsement background
      references.push({ x: e[1], label: '', color: colors.background, width: 4 });
      references.push({ x: e[1], label: '', color: colors.endorsement, width: 4, dash: true });
    }
  }
  const ticks = range(minTick, maxTick).map((x) => x * step)
  const xAxis = {
    key: 'range',
    label: I18n.chartLabel_ReceptionTimeMS,
    ticks: ticks,
  }
  // Normalize data: fill missing values with { pre_endorsement: 0, endorsement: 0 }
  const data = ticks.map(t => data0.find(x => x.range == t) || { range: t, pre_endorsement: 0, endorsement: 0 })
  const titleFormatter = (range: number): [[[string, string], [string, string]], [string, string]] => {
    const value = range / 1000
    const prec = `${step}`.length - 1
    const formatDate = (t: number) => {
      const d = new Date(t).toISOString();
      // Will be 24 characters long in our case
      return [d.slice(0, 10), d.slice(11, 23)]
    }
    return [
      [formatDate(range + timestamp), formatDate(timestamp + range + step)],
      [value.toFixed(prec), ((value + step / 1000) * 100 / 100).toFixed(prec)]
    ] as [[[string, string], [string, string]], [string, string]]
  }
  const modalContentAux = (title: string, range: number, data: Array<string>) => {
    return <DelegatesPopupSection title={title} data={data} endorsingPower={endorsingPower} />
  }
  const [PopupContent, setPopupContent] = useState(<></>)
  const popupOpen = useState(false);
  const Popup = <UI.Popup open={popupOpen}>{PopupContent}</UI.Popup>
  const onClick = ({ tick: tick0 }: { tick: string }) => {
    const tick = parseInt(tick0)
    const [[[d1, t1], [d2, t2]], [title_1, title_2]] = titleFormatter(tick);
    const title = I18n.title_DelegatesWithinTimeRangeS(title_1, title_2);
    const subtitle = '([ ' + t1 + ' ; ' + t2 + ' ])'
    const preendorsements = pre_endorsementsDelegates[tick] || []
    const endorsements = endorsementsDelegates[tick] || []
    if (preendorsements.length > 0 || endorsements.length > 0) {
      setPopupContent(
        <UI.SectionWithTitle title={title} subtitle={subtitle}>
          {modalContentAux(I18n.tableTitle_Preendorsements, tick, preendorsements)}
          {modalContentAux(I18n.tableTitle_Endorsements, tick, endorsements)}
        </UI.SectionWithTitle >
      )
      popupOpen[1](true)
    }
  }
  const onSelection = (start0: number, stop0: number) => {
    const start = start0.toString()
    const stop = stop0.toString()
    const step = Math.max(Math.floor((stop0 - start0) / 30 / 100) * 100, 100).toString()
    setLowerBound(start)
    setUpperBound(stop)
    // Try to have 30 subdivisions,
    // but keep step a multiple of 100 and at least 100
    setStep(step)
  }
  const resetZoom = () => {
    setLowerBound('');
    setUpperBound('');
    setStep('100');
  }
  const styleZoomOut: React.CSSProperties = {
    ...UI.IconButtonStyle,
    position: 'absolute',
    left: '8px',
    top: '8px',
    zIndex: 1,
  }
  const tooltipLabelFormatter = (value0: string) => {
    const range = parseInt(value0)
    const [[[d1, t1], [d2, t2]], [s1, s2]] = titleFormatter(range);
    const seconds = I18n.chartLabel_TooltipDelegatesWithinTimeRangeS(s1, s2);
    return <UI.Stack direction={'column'} style={{ alignItems: 'center', justifyContent: 'center' }}>
      <div>{t1 + ' < ' + t2}</div>
      <div>{seconds}</div>
    </UI.Stack>
  }
  return (
    <div style={{ position: 'relative' }}>
      {
        LowerBound !== '' || UpperBound !== ''
          ? <UI.IconButton variant={'solid'} onClick={resetZoom} icon={UI.MagnifierMinus} style={styleZoomOut} />
          : <></>
      }
      <UI.SuperimposedBarsChart
        data={data}
        xAxis={xAxis}
        yAxisLabel={yAxisLabel}
        yAxis={yAxis}
        references={references}
        tooltipLabelFormatter={tooltipLabelFormatter}
        onClick={onClick}
        onSelection={onSelection}
        placeholder={data0.length === 0 ? I18n.chartPlaceholder_Level : undefined}
      />
      {Popup}
    </div>
  )
}

const Charts = ({
  data: ChartData,
  delegate,
  display,
  level,
  LowerBoundState,
  round,
  UpperBoundState,
  StepState,
}: {
  data: ChartData,
  delegate?: PublicKeyHash,
  display: DisplayType,
  level: number,
  LowerBoundState: [string, React.Dispatch<React.SetStateAction<string>>],
  round: number,
  UpperBoundState: [string, React.Dispatch<React.SetStateAction<string>>],
  StepState: [string, React.Dispatch<React.SetStateAction<string>>],
}) => {
  const { I18n } = useContext(I18nContext);
  const { distribution, applicationBlock, endorsingPower, nextBlock, timestamps, validationBlock } = ChartData;
  const chartData = {
    endorsements: distribution.endorsements[round] || [],
    pre_endorsements: distribution.pre_endorsements[round] || [],
  }
  if (chartData.endorsements.length == 0 && chartData.pre_endorsements.length == 0) {
    return (
      <UI.Typography.P>{I18n.p_MissingBlockData(level, round)}</UI.Typography.P>
    )
  } else {
    return (
      <Chart
        delegate={delegate}
        distribution={chartData}
        endorsingPower={endorsingPower}
        LowerBoundState={LowerBoundState}
        nextRoundReception={
          timestamps[round + 1] !== undefined && validationBlock[round + 1] !== undefined
            ? timestamps[round + 1] - timestamps[round] + validationBlock[round + 1]
            : undefined}
        minDelayApplication={applicationBlock[round]}
        minDelayValidation={validationBlock[round]}
        nextBlockReception={nextBlock}
        UpperBoundState={UpperBoundState}
        StepState={StepState}
        timestamp={ChartData.timestamps[round]}
        withEndorsingPower={display == 'slots'}
      />
    )
  }
}

const printDelegate0 = (endorsingPower: endorsingPower, x: string) => {
  return <UI.DelegatePkh pkh={x} endorsingPower={endorsingPower[x]} />
}

const SummaryCommonProps = (
  Aliases: StringMap<string> | undefined,
  server: string,
  level: number,
  endorsingPower: endorsingPower,
  I18n: I18n,
  navigate: (url: string) => void
) => {
  const weight = (x: string) => endorsingPower[x]
  const printDelegate = (x: string) => printDelegate0(endorsingPower, x)
  const ext = UI.ButtonListDelegateExt({ server, level, I18n, navigate })
  const dataTitle = (x: string) => Aliases === undefined ? "" : Aliases[x] || "";
  return { weight: weight, print: printDelegate, ext: ext, dataTitle }
}

const SummaryOperationsReceived = ({ data, endorsingPower, level }: {
  data: classifyOperationsOutput[0]['endorsements'],
  endorsingPower: endorsingPower,
  level: number,
}) => {
  const { I18n } = useContext(I18nContext);
  const { ServerURL } = useContext(TeztaleServerAPI.ServerContext);
  const { Aliases } = useContext(DelegateAliases.AliasesContext);
  const navigate = Raviger.useNavigate();
  const endorsingPower_keys = Object.keys(endorsingPower);
  const totalData = endorsingPower_keys.length
  const totalWeight = sumValues(endorsingPower)
  const common = {
    totalData,
    totalWeight,
    ...SummaryCommonProps(Aliases, ServerURL, level, endorsingPower, I18n, navigate),
  }
  const missed = endorsingPower_keys.filter(d => !(data.valid.includes(d) || data.lost.includes(d) || data.sequestered.includes(d)))
  return (
    <UI.TableSummary>
      <UI.TableSummaryRow title={I18n.tableTitle_Missed} tooltip={I18n.tooltip_MissedRound} data={missed} {...common} />
      <UI.TableSummaryRow title={I18n.tableTitle_Valid} tooltip={I18n.tooltip_ValidRound} data={data.valid} {...common} />
      <UI.TableSummaryRow title={I18n.tableTitle_Lost} tooltip={I18n.tooltip_LostRound} data={data.lost} {...common} />
      <UI.TableSummaryRow title={I18n.tableTitle_HeldCaptive} tooltip={I18n.tooltip_HeldCaptiveRound} data={data.sequestered} {...common} />
      <UI.TableSummaryRow title={I18n.tableTitle_Erroneous} tooltip={I18n.tooltip_ErroneousRound} data={data.erroneous} {...common} />
    </UI.TableSummary>
  )
}

const SummaryIncomingOutgoingDelegates = ({ availableRounds, endorsingPower, level, operations, round }: {
  availableRounds: Array<number>,
  endorsingPower: endorsingPower,
  level: number,
  operations: SummaryData["operations"],
  round: number,
}) => {
  const { I18n } = useContext(I18nContext);
  const { ServerURL } = useContext(TeztaleServerAPI.ServerContext);
  const { Aliases } = useContext(DelegateAliases.AliasesContext);
  const navigate = Raviger.useNavigate();
  let incoming: Array<PublicKeyHash> = []
  let outgoing: Array<PublicKeyHash> = []
  const prevRound: number | undefined = availableRounds[availableRounds.indexOf(round) - 1]
  if (prevRound !== undefined && operations[round] !== undefined && operations[prevRound] !== undefined) {
    const next = operations[round].pre_endorsements.valid;
    const prev = operations[prevRound].pre_endorsements.valid;
    incoming = next.flatMap(x => prev.includes(x) ? [] : [x])
    outgoing = prev.flatMap(x => next.includes(x) ? [] : [x])
  }
  const common = SummaryCommonProps(Aliases, ServerURL, level, endorsingPower, I18n, navigate)
  return (
    <UI.TableSummary>
      <UI.TableSummaryRow title={I18n.tableTitle_Arriving} tooltip={I18n.tooltip_Arriving} data={incoming} {...common} />
      <UI.TableSummaryRow title={I18n.tableTitle_Exiting} tooltip={I18n.tooltip_Exiting} data={outgoing} {...common} />
    </UI.TableSummary>
  )
}

const Summary = ({ availableRounds, data, level, round }: { availableRounds: Array<number>, data: SummaryData, level: number, round: number }) => {
  const { I18n } = useContext(I18nContext);
  const {
    ParamURL: SummaryKind,
    setParamURL: setSummaryKind,
  } = useHashParamStringOption('summary', 'attestations' as string)
  const option_attestations = 'attestations';
  const option_preattestations = 'preattestations';
  let delegates =
    Object.keys(data.operations).length <= 1
      ? <></>
      : (
        <UI.SectionWithTitle title={I18n.title_Delegates}>
          {data.operations[round] === undefined
            ? <UI.Typography.P>{I18n.p_MissingBlockData(level, round)}</UI.Typography.P>
            : <SummaryIncomingOutgoingDelegates
              availableRounds={availableRounds}
              endorsingPower={data.endorsingPower}
              level={level}
              operations={data.operations}
              round={round}
            />
          }
        </UI.SectionWithTitle>
      )
  let operations =
    <UI.SectionWithTitle title={I18n.title_Operations}>
      {data.operations[round] === undefined
        ? <UI.Typography.P>{I18n.p_MissingBlockData(level, round)}</UI.Typography.P>
        : (
          <div style={{ position: 'relative' }}>
            <UI.Select style={{ position: 'absolute', right: 0, bottom: 'calc(100% + ' + ((30 + 16 * 2) - 40) / 2 + 'px)' }} value={SummaryKind} onChange={setSummaryKind}>
              {[
                [option_attestations, I18n.option_Endorsements]
                , [option_preattestations, I18n.option_Preendorsements]
              ].map(s => <UI.Option key={'option_SummaryKind_' + s[0]} value={s[0]}>{s[1]}</UI.Option>)}
            </UI.Select>
            <SummaryOperationsReceived
              data={data.operations[round][SummaryKind === option_preattestations ? 'pre_endorsements' : 'endorsements']}
              endorsingPower={data.endorsingPower}
              level={level}
            />
          </div>
        )
      }
    </UI.SectionWithTitle >
  return (
    <>
      <UI.Stack direction='column' spacing={2}>
        {delegates}
        {operations}
      </UI.Stack>
    </>
  );
}

/**
 * Display available informations about the block selected (timestamp, baker)
 */
const BlockDataSummary = ({
  data: data0,
  level,
  round,
  successors
}: {
  data: BlockData,
  level: number,
  round: number,
  successors: { [key: number]: Array<TeztaleServerData.BlockHash> },
}) => {
  const { I18n } = useContext(I18nContext);
  const { Protocols } = useContext(ProtocolsContext);
  const theme = UI.useTheme()
  const { Aliases } = useContext(DelegateAliases.AliasesContext);
  const buttonStyle = { alignSelf: 'flex-start' }
  const findRound = (r: number) => data0.blocks.find(x => (x.round || 0) === r)
  const data = findRound(round)
  const reproposal0 = Object.entries(data0.reproposal).flatMap(([round, blocs]) => {
    return data?.hash && blocs[data.hash] !== undefined ? [parseInt(round)] : []
  });
  const currentProtocol = (Protocols || []).find(p => p.firstLevel <= level && (p.lastLevel === undefined || p.lastLevel >= level));
  const reproposal = reproposal0.length === 0 ? undefined : findRound(Math.max(...reproposal0));
  const reproposalMissing = reproposal && data ? data0.reproposal[reproposal.round || 0][data.hash] : undefined;
  const reproposalDetailsOpen = useState(false);
  const reproposalDetails =
    !reproposalMissing || reproposalMissing.delegates.length === 0
      ? undefined
      : <UI.Popup open={reproposalDetailsOpen}>
        <DelegatesPopupSection
          title={I18n.title_ReproposalMissingDelegates}
          data={reproposalMissing.delegates}
          endorsingPower={data0.endorsingPower} />
      </UI.Popup>
  const protocolDetailsOpen = useState(false);
  const protocolDetails =
    currentProtocol === undefined
      ? undefined
      : <UI.Popup open={protocolDetailsOpen}>
        <pre><UI.Typography.CODE>{JSON.stringify(currentProtocol, undefined, 2)}</UI.Typography.CODE></pre>
      </UI.Popup>;
  const printBlockHash = (s: string) => <UI.BlockHash hash={s} />
  const printDelegate = (s: string) => Aliases ? Aliases[s] || shortenHash(s) : shortenHash(s)
  const printDelegateWIcon = (s: string) => <UI.DelegatePkh pkh={printDelegate(s)} short={false} />
  const copyOnClick = { onClick: (s: string) => UI.copyToClipboard(s, I18n.toast_CopiedToClipboard) }
  const aside: Array<{
    color?: string,
    data: Array<string>,
    display?: (_: string) => (JSX.Element | string),
    ext?: JSX.Element,
    label: string,
    onClick?: (_: string) => void,
    tooltip: string,
  }> = []
  if (currentProtocol !== undefined) {
    const currentProtocolOnclick = protocolDetails === undefined ? undefined : () => protocolDetailsOpen[1](true);
    aside.push({
      data: [`${shortenHash(currentProtocol.hash)}`],
      label: I18n.label_Protocol,
      tooltip: I18n.tooltip_Protocol,
      onClick: currentProtocolOnclick,
      ext: protocolDetails === undefined ? undefined : protocolDetails,
    })
  }
  if (data0.cycle !== undefined) {
    aside.push({
      data: [`${data0.cycle.cycle} (${data0.cycle.cycle_position + 1}/${data0.cycle.cycle_size})`],
      label: I18n.label_Cycle,
      tooltip: I18n.tooltip_Cycle,
    })
  }
  if (data && data.timestamp !== undefined)
    aside.push({
      data: [data.timestamp],
      label: I18n.label_Timestamp,
      tooltip: I18n.tooltip_Timestamp,
      display: (tm: string) => <UI.Timestamp tm={tm} />,
    })
  if (data?.delegate || reproposalMissing?.proposer)
    aside.push({
      data: [(reproposalMissing?.proposer || data?.delegate) as string], // verified by the if condition
      label: I18n.label_Proposer,
      tooltip: I18n.tooltip_Proposer,
      display: printDelegateWIcon,
      ...copyOnClick,
    })
  if (data && data.delegate !== undefined)
    aside.push({
      data: [data.delegate],
      label: I18n.label_Baker,
      tooltip: I18n.tooltip_Baker,
      display: printDelegateWIcon,
      ...copyOnClick,
    })
  if (data)
    aside.push({
      data: [data.hash],
      display: printBlockHash,
      label: I18n.label_CurrentBlockHash,
      tooltip: I18n.tooltip_BlockHash,
      ...copyOnClick,
    })
  if (reproposal !== undefined)
    aside.push({
      color: 'rgba(209, 88, 74, 0.3)',
      data: [I18n.p_NthRound(reproposal.round || 0)],
      display: (s: string) => I18n.p_NthRound(reproposal.round || 0),
      ext: reproposalDetails
        ? <>
          <UI.IconButton icon={UI.QuestionCircle} onClick={() => reproposalDetailsOpen[1](true)} />
          {reproposalDetails}
        </>
        : undefined,
      label: I18n.label_ReproposalOf,
      tooltip: I18n.tooltip_ReproposalOf,
    })
  if (successors[round] !== undefined && successors[round].length !== 0)
    aside.push({
      data: successors[round],
      display: printBlockHash,
      label: I18n.label_Successors,
      tooltip: I18n.tooltip_Successors,
      ...copyOnClick,
    })
  const inlineLabelled = (label: string, value: string | JSX.Element) =>
    <UI.Stack direction='row' spacing={1}>
      <span style={{ color: theme.palette.text.secondary }}>{label}</span>
      {typeof value === "string" ? <span>{value}</span> : value}
    </UI.Stack>
  return (
    <UI.Stack direction='column' spacing={2}>
      {
        aside.map(x => {
          const display = x.display || ((x: string) => x)
          return (
            <UI.Stack key={x.label} direction='row' spacing={1} style={{ alignItems: 'center' }}>
              <UI.Tooltip title={x.tooltip} style={{ marginLeft: '0' }} />
              <span>{x.label}</span>
              {x.data.map((s) => {
                const style = x.color ? { ...buttonStyle, backgroundColor: x.color } : buttonStyle;
                return (
                  <UI.Button key={x.label + '_' + s} style={style} onClick={() => x.onClick !== undefined ? x.onClick(s) : {}}>
                    {display(s)}
                  </UI.Button>)
              })}
              {x.ext ? x.ext : <></>}
            </UI.Stack>
          )
        })
      }
      {
        data && data.predecessor !== undefined
          ?
          <UI.Stack direction='row' spacing={1} style={{ alignItems: 'flex-start' }}>
            <UI.Tooltip title={I18n.tooltip_PredecessorInfo} style={{ marginLeft: '0', marginTop: '3px' }} />
            <UI.Stack direction='column'>
              <span>{I18n.label_Predecessor}</span>
              <UI.Stack
                direction='column'
                style={
                  {
                    backgroundColor: theme.palette.neutral.solidBg,
                    padding: '8px 8.5px',
                    borderRadius: '4px',
                    marginTop: '4px',
                  }
                }
              >
                {
                  data0.prevCycle
                    ? inlineLabelled(I18n.label_Cycle, `${data0.prevCycle.cycle} (${data0.prevCycle.cycle_position + 1}/${data0.prevCycle.cycle_size})`)
                    : <></>
                }
                {inlineLabelled(I18n.label_BlockHash, shortenHash(data.predecessor.hash))}
                {inlineLabelled(I18n.label_Level, data.predecessor.level.toString())}
                {inlineLabelled(I18n.label_Round, `${data.predecessor.round || 0} / ${data.predecessor.maxRound}`)}
                {data.predecessor.delegate ? inlineLabelled(I18n.label_Baker, printDelegate(data.predecessor.delegate)) : <></>}
              </UI.Stack>
            </UI.Stack>
          </UI.Stack>
          : <></>
      }
    </UI.Stack>
  )
}

export const Level = () => {
  const { I18n } = useContext(I18nContext);
  const { ServerURL, HEAD, TzKTURL } = useContext(ServerContext)

  const [Loading, setLoading] = useState(false)
  const [FetchingData, setFetchingData] = useState(false)
  const [FetchedData, setFetchedData] = useState(undefined as [number, DataByLevel] | undefined)
  const {
    ParamURL: Source0,
    setParamURL: setSource,
  } = useHashParamStringOption('source', '' as string)
  const [SourceList, setSourceList] = useState([] as Array<string>)
  const [Delegates, setDelegates] = useState([] as Array<string>)

  const {
    ParamInput: DelegateInput,
    setParamInput: setDelegateInput,
  } = useHashParamString('delegate', '' as string)

  const {
    DebouncedParamInput: dBlock,
    ParamInput: BlockInput,
    setParamInput: setBlockInput,
  } = useHashParamString('block', 'HEAD-1' as string)
  const [Block, setBlock] = useState(undefined as number | undefined);
  useEffect(() => {
    const base = Arithmetic.parse(dBlock)
    const updated = base !== undefined && HEAD !== undefined ? Arithmetic.updateWithHEAD(base, HEAD) : base
    setBlock(updated !== undefined ? Arithmetic.toNumber(updated) : undefined)
  }, [dBlock, HEAD])

  const [BlockData, setBlockData] = useState(undefined as BlockData | undefined)

  const [Successors, setSuccessors] = useState({} as { [key: number]: Array<TeztaleServerData.BlockHash> })

  const {
    ParamInput: CategoryDisplay,
    setParamInput: setCategoryDisplay,
  } = useHashParamString('category', 'slots')

  const zoomBatch = Batch.useBatch()
  const {
    DebouncedParamInput: DebouncedLowerBound,
    ParamInput: LowerBoundInput,
    setParamInput: setLowerBoundInput,
  } = useHashParamStringOption('intervalLowerBound', '' as string, zoomBatch)
  const {
    DebouncedParamInput: DebouncedUpperBound,
    ParamInput: UpperBoundInput,
    setParamInput: setUpperBoundInput,
  } = useHashParamStringOption('intervalUpperBound', '' as string, zoomBatch)
  const {
    DebouncedParamInput: DebouncedStep,
    ParamInput: StepInput,
    setParamInput: setStepInput,
  } = useHashParamString('step', '100' as string, zoomBatch)

  const {
    ParamInput: RoundInput,
    setParamInput: setRoundInput,
  } = useHashParamString('round', '' as string)

  const [AvailableRounds, setAvailableRounds] = useState([] as Array<number>)
  const [MaxRound, setMaxRound] = useState(undefined as number | undefined) // FIXME: should be removed and rely only on available rounds?
  const [MinRound, setMinRound] = useState(undefined as number | undefined) // FIXME: should be removed and rely only on available rounds?

  const [SummaryData, setSummaryData] = useState(undefined as undefined | SummaryData)
  const [ChartData, setChartData] = useState(undefined as undefined | ChartData)

  const Round0 = parseIntUndefined(RoundInput)
  const Round: number = Round0 === undefined ? MaxRound || MinRound || 0 : (MinRound !== undefined && Round0 < MinRound) ? MinRound : MaxRound !== undefined && Round0 > MaxRound ? MaxRound : Round0
  if (Round0 !== Round) setRoundInput(Round.toString())

  const Source: string | undefined = (Source0 === undefined || SourceList.indexOf(Source0) === -1) ? SourceList.length !== 0 ? SourceList[0] : undefined : Source0;

  useEffect(() => {
    setFetchingData(true);
    setFetchedData(undefined);
    try {
      if (Block != undefined) {
        populateV1(Block - 1, Block + 1).then((data: DataByLevel) => {
          setSourceList(extractSources(data[Block]));
          setFetchedData([Block, data]);
          setFetchingData(false);
        })
      } else {
        setFetchingData(false);
      }
    } catch (e) {
      console.error(e);
      setFetchingData(false);
    }
  }, [Block])

  useEffect(() => {
    if (FetchedData !== undefined) {
      setLoading(true);
      setBlockData(undefined);
      setSuccessors({});
      setDelegates([])
      setRoundInput('');
      setMaxRound(undefined);
      setMinRound(undefined);
      setSummaryData(undefined);
      setChartData(undefined);
      const [block, data00] = FetchedData
      const data0 = Source === undefined ? data00 : NumberMap_map(data00, x => filterSource(x, Source))
      const data = data0[block];
      const cycle = data.cycle_info;
      const missingBlocks = data.missing_blocks || [];
      missingBlocks.sort((a, b) => a.baking_right.round - b.baking_right.round);
      const availableRounds = data === undefined ? [] : (data.blocks || []).map(b => b.round || 0)
      availableRounds.sort((a, b) => a - b);
      if (availableRounds.length !== 0) {
        const distribution = delaysDistributionOfOperationsAtLevelWDelegate(data, true);
        const endorsingPower = getEndorsingPowerAtLevel(data)
        const delegates = extractDelegates(endorsingPower);
        const maxRound = availableRounds[availableRounds.length - 1];
        const minRound = availableRounds[0];
        const operations = classifyOperations(data);
        const { applicationBlock, validationBlock, timestamps } = getInfoBlock(data, true);
        const nextBlock0: TeztaleServerData.T | undefined = data0[block + 1];
        const Successors: { [key: number]: Array<TeztaleServerData.BlockHash> } = {}
        if (nextBlock0 !== undefined) {
          availableRounds.forEach(i => {
            const h = (data.blocks || []).find(b => (b.round || 0) === i)?.hash
            Successors[i] = (nextBlock0.blocks || []).flatMap(x => h !== undefined && x.predecessor === h ? [x.hash] : [])
          })
        }
        const nextBlockInfo = nextBlock0 === undefined ? undefined : getInfoBlock(nextBlock0, true);
        const nextBlock = nextBlockInfo === undefined ? undefined : nextBlockInfo.applicationBlock[0] + nextBlockInfo.timestamps[0] - timestamps[0];
        const reproposal0 = reproposal(data)
        const prevBlock0: TeztaleServerData.T | undefined = data0[block - 1];
        const prevCycle = prevBlock0.cycle_info;
        const prevMaxRound = prevBlock0 === undefined ? 0 : Math.max(...(prevBlock0.blocks || []).map(b => b.round || 0))
        const blocks = (data.blocks || []).map(b => {
          if (!b.predecessor === undefined) return { ...b, predecessor: undefined, }
          const p1 = (prevBlock0.blocks || []).find(x => x.hash === b.predecessor)
          if (p1 !== undefined) {
            return { ...b, predecessor: { ...p1, maxRound: prevMaxRound, level: block - 1 } }
          } else {
            const p2 = (data.blocks || []).find(x => x.hash === b.predecessor);
            return (p2 !== undefined) ? { ...b, predecessor: { ...p2, maxRound, level: block } } : { ...b, predecessor: undefined, }
          }
        });
        setBlockData({
          blocks, cycle, endorsingPower, prevCycle, reproposal: reproposal0, missingBlocks
        });
        setSuccessors(Successors);
        setDelegates(delegates);
        setAvailableRounds(availableRounds);
        setMaxRound(maxRound);
        setMinRound(minRound);
        setRoundInput(maxRound.toString());
        setSummaryData({ endorsingPower, operations, timestamps });
        setChartData({ applicationBlock, endorsingPower, distribution, nextBlock, validationBlock, timestamps });
      }
      setLoading(false);
    }
  }, [FetchedData, Source])

  const page_LOADING = Loading
  const sticky_LOADING = FetchingData

  const navigate = Raviger.useNavigate()

  const MissingBakersPopupOpen = useState(false);

  const MissingBakersPopup =
    BlockData?.missingBlocks.length === 0
      ? undefined
      : <UI.Popup open={MissingBakersPopupOpen}>
        <UI.Typography.H1>{I18n.tooltip_MissedBakerRound}</UI.Typography.H1>
        {
          BlockData?.missingBlocks.map(m =>
            <UI.PopupSection
              subtitle={`${I18n.label_Round} ${m.baking_right.round}`}
              data={[m.baking_right.delegate]} print={pkh => <UI.DelegatePkh short={false} pkh={pkh} />} />
          )
        }
      </UI.Popup>
    ;

  const sticky =
    <UI.PageParameters>
      <UI.LabelledInputCtrlArithmetic
        HEAD={HEAD}
        label={I18n.label_Level}
        required={true}
        setState={setBlockInput}
        size={10}
        tooltip={I18n.tooltip_Level}
        value={BlockInput}
      />
      <UI.LabelledInputCtrlNumber
        datalist={AvailableRounds.map(String)}
        disabled={AvailableRounds.length < 2}
        endDecorator={<>/ {MaxRound}</>}
        label={I18n.label_Round}
        max={MaxRound}
        min={MinRound}
        readOnly={true} // Prevent the user to use arbitrary data
        renderItem={(item: string) =>
          <UI.Stack direction='row' style={{ flex: '1', alignItems: 'center' }}>
            {item}
            {Successors[parseInt(item)] === undefined || Successors[parseInt(item)].length === 0 ? <></> :
              <UI.HasSuccessor size={20} style={{ marginLeft: 'auto' }} />}
          </UI.Stack>
        }
        setState={setRoundInput}
        size={1}
        styleInput={{ maxWidth: '2ex', textAlign: 'right', }}
        tooltip={I18n.tooltip_Round}
        value={RoundInput}
      />
      <UI.Labelled label={I18n.label_Source} tooltip={I18n.tooltip_Source}>
        <UI.Select value={Source} onChange={setSource}>
          {SourceList.map(s => <UI.Option key={'option_source_' + s} value={s}>{s}</UI.Option>)}
        </UI.Select>
      </UI.Labelled>
      {sticky_LOADING ? <UI.Spinner style={{ marginBottom: 8 }} /> : <></>}
      {
        (BlockData?.missingBlocks === undefined || BlockData.missingBlocks.length == 0)
          ? <></>
          : <>
            <UI.IconButton style={{ marginLeft: 'auto' }} icon={UI.Warning} onClick={() => MissingBakersPopupOpen[1](true)} />
            {MissingBakersPopup}
          </>
      }
      <UI.IconButton
        style={{ marginLeft: (BlockData?.missingBlocks === undefined || BlockData.missingBlocks.length == 0) ? 'auto' : 0 }}
        icon={UI.Diff}
        onClick={() => navigate(Routes.path_LevelDiff + window.location.hash)} />
    </UI.PageParameters>

  const error = Block == undefined ? I18n.error_BlockUndefined : undefined

  return (
    <UI.PageWithStickySection
      sticky={sticky}
      loading={page_LOADING}
      error={error}
      data={
        Block == undefined
          || (SummaryData == undefined && ChartData == undefined && BlockData === undefined)
          ? undefined
          : [SummaryData, ChartData, BlockData, Block] as [SummaryData | undefined, ChartData | undefined, BlockData | undefined, number]
      }
    >
      {
        ([SummaryData, ChartData, BlockData, Block]) => {
          const chart =
            ChartData === undefined ? undefined : (
              <>
                <UI.PageParameters style={{ position: 'relative', marginTop: 0, }}>
                  <UI.Labelled label={I18n.label_Display} tooltip={I18n.tooltip_ChartDisplayDelegateOrSlots}>
                    <UI.Select value={CategoryDisplay} onChange={setCategoryDisplay}>
                      <UI.Option value='delegates'>{I18n.option_NbDelegates}</UI.Option>
                      <UI.Option value='slots'>{I18n.option_NbSlots}</UI.Option>
                    </UI.Select>
                  </UI.Labelled>
                  <UI.LabelledInput tooltip={I18n.tooltip_ChartStep} size={3} label={I18n.label_Step} onChange={setStepInput} value={StepInput} />
                  <UI.RangeInputs
                    size={4}
                    tooltip={I18n.tooltip_ChartRange}
                    labels={[I18n.label_LowerBound, I18n.label_UpperBound]}
                    lowerBound={[LowerBoundInput, setLowerBoundInput]}
                    upperBound={[UpperBoundInput, setUpperBoundInput]}
                  />
                  <UI.DelegateSelector
                    onChange={setDelegateInput}
                    value={DelegateInput}
                    datalist={Delegates}
                    sort={(a, b) => ChartData.endorsingPower[b] - ChartData.endorsingPower[a]}
                    style={{ position: 'absolute', right: 0, bottom: 'calc(100% + ' + ((30 + 16 * 2) - 40) / 2 + 'px)' }}
                  />
                </UI.PageParameters>
                <Charts
                  data={ChartData}
                  delegate={DelegateInput}
                  display={CategoryDisplay}
                  level={Block}
                  round={Round}
                  LowerBoundState={[DebouncedLowerBound, setLowerBoundInput]}
                  StepState={[DebouncedStep, setStepInput]}
                  UpperBoundState={[DebouncedUpperBound, setUpperBoundInput]}
                />
              </>
            )
          const asideStyle = { marginTop: 142 }
          const aside =
            BlockData === undefined
              ? undefined
              : <BlockDataSummary
                level={Block}
                data={BlockData}
                successors={Successors}
                round={Round}
              />
          return (
            <UI.Stack direction={'column'} spacing={2}>
              <UI.Chart aside={aside} asideStyle={asideStyle} chart={chart} title={I18n.title_EndorsementReceptionDelays} />
              {SummaryData == undefined ? <></> : <Summary availableRounds={AvailableRounds} data={SummaryData} level={Block} round={Round} />}
            </UI.Stack>
          )
        }
      }
    </UI.PageWithStickySection >
  )
}
