import * as UI from '../ui/UI';

export const About = (props: {}) => {
  return (
    <UI.Page data={null}>
      {
        (_: null) => (
          <UI.Container style={{ margin: 'auto' }}>
            <UI.Typography.H1 textAlign='center'>Teztale Dataviz</UI.Typography.H1>
            <UI.Typography.P textAlign='center'>
              <UI.Typography.CODE>v0.0.0</UI.Typography.CODE>
            </UI.Typography.P>
            <UI.Typography.P textAlign='center'>
              Teztale Dataviz is a frontend application for{' '}
              <a href='https://gitlab.com/nomadic-labs/teztale'>Teztale</a>.
            </UI.Typography.P>
            <UI.Typography.P textAlign='center'>
              See {' '}<a href='https://gitlab.com/nomadic-labs/teztale-dataviz/'>teztale-dataviz</a>{' '}
              for details.
            </UI.Typography.P>
            <UI.Typography.P textAlign='center'>
              Baker names are provided by the {' '}<a href='https://tzkt.io/'>tzkt.io</a>{' '} API.
            </UI.Typography.P>
          </UI.Container>
        )
      }
    </UI.Page>
  )
}
