import { useContext, useEffect, useState } from "react";
import * as UI from '../ui/UI';
import { I18nContext } from "../lib/I18nContext";
import * as TeztaleServerAPI from '../lib/TeztaleServerAPI';
import * as TeztaleServerData from '../lib/TeztaleServerData';
import * as Raviger from 'raviger';
import { NumberMap, unique } from "../lib/Utils";
import * as Routes from '../lib/Routes';
import * as Theme from '../ui/UI_Theme';
import { chartTheme } from '../ui/UI_Charts';

const Item = ({ data, onClick }: { data: FetchedData[0], onClick: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void, }) => {
  const { I18n } = useContext(I18nContext);
  const [scheme, _] = Theme.useColorScheme()
  const colors = chartTheme[scheme]
  const [level, { available, missing }] = data;
  const complete = [];
  if (available.length !== 0 && available.length - 1 === available[available.length - 1][0]) complete.push(level);
  const rounds = [];
  const maxAvailable = available.length !== 0 ? available[available.length - 1][0] : 0;
  const maxMissing = missing[missing.length - 1];
  for (let r = Math.max(maxAvailable, maxMissing === undefined ? 0 : maxMissing[0]); r > -1; r--) {
    const a = available.find(rr => rr[0] === r)
    const [color, legend] =
      undefined === a
        ? -1 === missing.findIndex(([rr, _b, _p]) => rr === r)
          ? ["#CCC", I18n.tooltip_AvaibilityUnknown]
          : ["rgb(232, 82, 82)", I18n.tooltip_AvaibilityMissing]
        : a[1]
          ? [colors.endorsement, I18n.tooltip_AvaibilityCanonical]
          : [colors.quorum, I18n.tooltip_AvaibilityNonCanonical];
    rounds.push(
      <UI.Square title={`${level}/${r}: ${legend}`} size={16} color={color} />
    )
  };
  return (
    <div onClick={onClick}>
      <UI.Stack spacing={0} style={{ marginTop: 20, }}>{rounds}</UI.Stack>
    </div>
  )
}

type missing = [TeztaleServerData.MissingData[1], TeztaleServerData.MissingData[2], TeztaleServerData.MissingData[3]]

type FetchedData = Array<[string, { available: Array<[number, boolean]>, missing: Array<missing> }]>

export const Available = () => {

  const { I18n } = useContext(I18nContext);

  const [Fetching, setFetching] = useState(false as boolean)
  const [FetchedDataAvailable, setFetchedDataAvailable] = useState(undefined as undefined | NumberMap<Array<[number, boolean]>>)
  const [FetchedDataMissing, setFetchedDataMissing] = useState(undefined as undefined | NumberMap<Array<missing>>)
  const [FetchedData, setFetchedData] = useState(undefined as undefined | FetchedData)
  const popupOpen = useState(false);
  const [PopupContent, setPopupContent] = useState(<></>)
  const Popup = <UI.Popup open={popupOpen}>{PopupContent}</UI.Popup>

  const { ServerURL } = useContext(TeztaleServerAPI.ServerContext);
  const navigate = Raviger.useNavigate();

  useEffect(() => {
    const data: NumberMap<FetchedData[0][1]> = {};
    (FetchedDataAvailable === undefined ? [] : Object.entries(FetchedDataAvailable) || []).forEach(([level, rounds]) => {
      data[+level] = { available: rounds, missing: [] };
    });
    (FetchedDataMissing === undefined ? [] : Object.entries(FetchedDataMissing) || []).forEach(([level, m]) => {
      m.forEach(([r, b, p]) => {
        if (undefined === data[+level]) {
          data[+level] = { available: [], missing: [[r, b, p]] };
        } else {
          // Need to make it unique untile teztale server 2.2.1_octez-v20.1
          if (-1 === data[+level].missing.findIndex(([r0, _b, _p]) => r === r0)) {
            data[+level].missing.push([r, b, p]);
          }
        }
      });
    });
    const entries = Object.entries(data);
    entries.forEach(([_, data]) => data.available.sort(([r1, _b1], [r2, _b2]) => r1 - r2));
    entries.forEach(([_, data]) => data.missing.sort(([r1, _b1, _p1], [r2, _b2, _p2]) => r1 - r2));
    entries.sort(([l1, _r1], [l2, _r2]) => (+l2) - (+l1));
    if (entries.length > 0 && entries.length !== ((+entries[0][0]) - (+entries[entries.length - 1][0]))) {
      const filled: Array<[string, {
        available: Array<[number, boolean]>;
        missing: Array<missing>;
      }]> = [];
      const start = +entries[0][0];
      const end = +entries[entries.length - 1][0];
      for (let i = start; i >= end; i--) {
        const current = entries.find(([level, _]) => +level === i);
        filled.push(current === undefined ? [`${i}`, { available: [], missing: [], }] : current)
      }
      setFetchedData(filled);
    } else {
      setFetchedData(entries);
    }
  }, [FetchedDataAvailable, FetchedDataMissing]);

  const btnDetailsOnClick = (level: string, available: Array<[number, boolean]>, missing: Array<missing>) => {
    setPopupContent(
      <UI.SectionWithTitle title={`${I18n.label_Level} ${level}`}>
        <UI.Stack direction={"row"} spacing={2}>
          <UI.Button onClick={e => UI.copyToClipboard(level, I18n.toast_CopiedToClipboard)}>
            <UI.Copy style={{ marginRight: 8 }} /> {I18n.tooltip_CopyToClipboard}
          </UI.Button>
          <UI.Button onClick={e => navigate(`${Routes.path_Level}#server=${ServerURL}&block=${level}`)}>
            <UI.Block style={{ marginRight: 8 }} /> {I18n.label_GoToLevelPage}
          </UI.Button>
        </UI.Stack>
        {
          missing.map(([r, b, p]) =>
            <UI.PopupSection subtitle={`${I18n.label_Round} ${r}`} data={[b]} print={pkh => <UI.DelegatePkh short={false} pkh={pkh} />} />
          )
        }
      </UI.SectionWithTitle>
    );
    popupOpen[1](true)
  }

  const onSubmit = (fst: number | undefined, lst: number | undefined) => {
    setFetchedDataAvailable(undefined);
    setFetchedDataMissing(undefined);
    if (fst !== undefined && lst !== undefined) {
      setFetching(true);
      Promise.all([
        TeztaleServerAPI.getAvailableDataJson(fst, lst)
          .then((fetched) => {
            const data: NumberMap<Array<[number, boolean]>> = {};
            fetched.forEach(([level, round, canonical]) => {
              if (undefined === data[level]) data[level] = [];
              data[level].push([round, canonical]);
            });
            (Object.values(data) || []).forEach(array => array.sort());
            setFetchedDataAvailable(data);
          })
        , TeztaleServerAPI.getMissingDataJson(fst, lst)
          .then((fetched) => {
            const data: NumberMap<Array<missing>> = {};
            fetched.map(([level, round, baker, partially_missing]) => {
              if (undefined === data[level]) data[level] = [];
              data[level].push([round, baker, partially_missing] as missing);
            });
            (Object.values(data) || []).forEach(array => array.sort(([r1, _b1, _p1], [r2, _b2, _p2]) => r2 - r1));
            setFetchedDataMissing(data);
          })
      ]).then(() => setFetching(false));
    }
  }

  return (
    <UI.BlocksRangePage0 data={FetchedData} loading={Fetching} onSubmit={onSubmit}>
      {
        (data) =>
          <UI.Pagination data={data} size={1000}>
            {
              (data) =>
                <UI.Stack direction="row" style={{ alignItems: "flex-end", flexWrap: "wrap", }}>
                  {
                    data.map(([level, { available, missing }]) =>
                      <Item key={level} data={[level, { available, missing }]} onClick={_e => btnDetailsOnClick(level, available, missing)} />)
                  }
                  {Popup}
                </UI.Stack>
            }
          </UI.Pagination>
      }
    </UI.BlocksRangePage0>
  )
}