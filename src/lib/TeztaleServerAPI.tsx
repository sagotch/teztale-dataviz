import * as TeztaleServerData from './TeztaleServerData'
import { createContext, useState, ReactNode, useEffect } from 'react';
import { useHashParamString } from './Utils';

export interface ServerContextType {
  ServerURL: string,
  setServerURL: (_: string) => void,
  HEAD: number | undefined,
  setHEAD: (_: number | undefined) => void,
  TzKTURL: string | undefined,
  setTzKTURL: (_: string | undefined) => void,
}

let inner_serverURL = '/'
let inner_currentHead = undefined as undefined | number

export const ServerContext = createContext<ServerContextType>({
  ServerURL: inner_serverURL,
  setServerURL: (url: string) => { console.log('ServerContext default setServerURL ', url) },
  HEAD: undefined,
  setHEAD: (head: number | undefined) => { console.log('ServerContext default setHEAD ', head) },
  TzKTURL: undefined,
  setTzKTURL: (url: string | undefined) => { console.log('ServerContext default setTzKTURL ', url) },
});

export const ServerContextProvider = ({ children }: React.PropsWithChildren<{}>) => {
  const {
    ParamURL: ServerURL,
    setParamURL: setServerURL,
  } = useHashParamString('server', inner_serverURL)
  useEffect(() => { inner_serverURL = ServerURL.endsWith('/') ? ServerURL : ServerURL + '/' }, [ServerURL]);
  const [HEAD, setHEAD0] = useState(undefined as number | undefined)
  const setHEAD = (head: number | undefined) => {
    inner_currentHead = head
    setHEAD0(inner_currentHead)
  }
  const [TzKTURL, setTzKTURL] = useState(undefined as string | undefined)
  return (
    <ServerContext.Provider value={{ ServerURL, setServerURL, HEAD, setHEAD, TzKTURL, setTzKTURL }}>
      {children}
    </ServerContext.Provider>
  )
}

const cacheSeed = Math.random()

/** [level, round] */
export const getAvailableDataJson = async (fst: number, lst: number): Promise<Array<[number, number, boolean]>> => {
  const [url, cache]: [string, RequestCache] = [inner_serverURL + `${fst}-${lst}/available.json`, 'no-cache']
  const fetched = await fetch(url, { cache })
  const json: Array<[number, number, boolean]> = await fetched.json()
  return json
}

/** [ level, round, baker, partially missing ] */
export const getMissingDataJson = async (fst: number, lst: number): Promise<Array<TeztaleServerData.MissingData>> => {
  const [url, cache]: [string, RequestCache] = [inner_serverURL + `${fst}-${lst}/missing.json`, 'no-cache']
  const fetched = await fetch(url, { cache })
  const json: Array<TeztaleServerData.MissingData> = await fetched.json()
  return json
}

export const getBlockDataJson = async (level: number): Promise<TeztaleServerData.T> => {
  const frozen = inner_currentHead && level < inner_currentHead - 5
  const [url, cache]: [string, RequestCache] =
    frozen
      ? [inner_serverURL + level + ".json?v=" + cacheSeed, 'force-cache']
      : [inner_serverURL + level + '.json', 'no-cache']
  const fetched = await fetch(url, { cache })
  const json: TeztaleServerData.T = await fetched.json()
  return json
}

export const getBlockRangeDataJson = async (first: number, last: number): Promise<TeztaleServerData.Batch> => {
  const frozen = inner_currentHead && last < inner_currentHead - 5
  const [url, cache]: [string, RequestCache] =
    frozen
      ? [inner_serverURL + first + '-' + last + ".json?v=" + cacheSeed, 'force-cache']
      : [inner_serverURL + first + '-' + last + '.json', 'no-cache']
  const fetched = await fetch(url, { cache })
  const json: TeztaleServerData.Batch = await fetched.json()
  return json
}

let inner_getHead_controller = new AbortController()

export const getHead = async (): Promise<number> => {
  inner_getHead_controller.abort();
  inner_getHead_controller = new AbortController();
  const inner_serverURL0 = inner_serverURL;
  const url = inner_serverURL0 + "head.json";
  return (
    fetch(url, { cache: 'no-cache', signal: inner_getHead_controller.signal })
      .then((result) => result.json())
      .then((json) => inner_serverURL === inner_serverURL0 ? json['level'] : getHead())
      .catch(_ => undefined)
  )
}
