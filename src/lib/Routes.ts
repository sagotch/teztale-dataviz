export const BASE_URL = import.meta.env.BASE_URL

export const path_About = 'About'
export const path_Available = 'Available'
export const path_DelayToConsensus = 'DelayToConsensus'
export const path_DelegateStats = 'DelegateStats'
export const path_CommitteeSizePerDelay = 'CommitteeSizePerDelay'
export const path_CommitteeSizePerLevel = 'CommitteeSizePerLevel'
export const path_Level = 'Level'
export const path_LevelDiff = 'LevelDiff'
export const path_Report = 'Report'