import I18n_en from './I18n_en'
import { I18n } from './I18n_interface';
import { createContext, useState, ReactNode } from 'react';

export type language = 'debug' | 'en'

export interface I18nContextType {
  Language: language,
  setLanguage: (_: language) => void,
  I18n: I18n,
}

const I18n_debug: I18n = new Proxy(I18n_en, {
  get(target: I18n, prop: keyof I18n) {
    if (typeof target[prop] === 'function') {
      return (...args: any[]) => `[${prop}]`
    } else {
      return `[${prop}]`
    }
  },
});

const I18nOfLanguage = (lang: language) => {
  return lang === 'en' ? I18n_en : I18n_debug
}

export const I18nContext = createContext<I18nContextType>({
  Language: 'en',
  setLanguage: (lang: language) => { console.log('I18nContext default setLanguage ', lang) },
  I18n: I18nOfLanguage('en'),
});

export const I18nContextProvider = ({ children }: React.PropsWithChildren<{}>) => {
  const [Language, setLanguage] = useState('en' as language)
  return (
    <I18nContext.Provider value={{ Language, setLanguage, I18n: I18nOfLanguage(Language) }}>
      {children}
    </I18nContext.Provider>
  )
}
