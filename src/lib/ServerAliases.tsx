import { createContext, useState } from 'react';
import { StringMap } from './Utils';
import DefaultAliases from '../assets/servers-aliases.json';

export interface ServersContextType {
  TeztaleServers: StringMap<string> | undefined,
  setServers: (_: [StringMap<string>, StringMap<string>] | undefined) => void,
  TzKTServers: StringMap<string> | undefined,
}

export const ServersContext = createContext<ServersContextType>({
  TeztaleServers: undefined,
  setServers: (Servers: [StringMap<string>, StringMap<string>] | undefined) => { console.log('ServersContext default setServers ', Servers) },
  TzKTServers: undefined,
});

export const ServersContextProvider = ({ children }: React.PropsWithChildren<{}>) => {
  const [TeztaleServers, setTeztaleServers] = useState(undefined as StringMap<string> | undefined)
  const [TzKTServers, setTzKTServers] = useState(undefined as StringMap<string> | undefined)
  const setServers = (servers: [StringMap<string>, StringMap<string>] | undefined) => {
    if (servers === undefined) {
      setTeztaleServers(undefined);
      setTzKTServers(undefined);
    } else {
      setTeztaleServers(servers[0]);
      setTzKTServers(servers[1]);
    }
  }
  return (
    <ServersContext.Provider value={{ TeztaleServers, setServers, TzKTServers }}>
      {children}
    </ServersContext.Provider>
  )
}

const getServers0 = async (): Promise<Array<{ alias: string, address: string, TzKT?: string }>> => {
  try {
    const localConf = window.location.host + '/servers-aliases.json'
    const fetched = await fetch(localConf)
    const json = await fetched.json()
    if (Array.isArray(json)) return json
    else return DefaultAliases;
  } catch (error) {
    return DefaultAliases;
  }
}

export const getServers = async (): Promise<[StringMap<string>, StringMap<string>]> => {
  const aliases: StringMap<string> = {};
  const tzkt: StringMap<string> = {};
  const json = await getServers0();
  json.forEach((o) => {
    if (o.alias) aliases[o.address] = o.alias;
    if (o.TzKT) tzkt[o.address] = o.TzKT;
  });
  return [aliases, tzkt];
}
