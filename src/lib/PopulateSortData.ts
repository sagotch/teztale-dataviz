import * as TeztaleServerData from './TeztaleServerData'
import {
  type PublicKeyHash,
} from './TeztaleServerData'
import * as TeztaleServerAPI from './TeztaleServerAPI'
import {
  type NumberMap,
  keysInSortedOrderOfValues,
  minTime,
  pushUnique,
  range,
  sumValues,
} from './Utils'

export type ByLevel<T> = { [id: number]: T }
export type ByPublicKeyHash<T> = { [id: PublicKeyHash]: T }
export type DataByLevel = ByLevel<TeztaleServerData.T> // FIXME: use undefined to indicate server errors or unusable data
export type Partitioned<T> = { pre_endorsements: T, endorsements: T }

const populateV1_fetch = async (level: number, progress: (k: number, v: TeztaleServerData.T | undefined) => void) => {
  try {
    const json = await TeztaleServerAPI.getBlockDataJson(level)
    const blocks = json.blocks
    // We prefer to miss data rather than have data that would lead to wrong info displayed or crash
    // Ensure that we have at least either blocks or endorsements data.
    // If blocks are defined, also check that we can handle them (no double baking) 
    if (blocks !== undefined) {
      if (blocks.every(({ round }, i) => blocks.findIndex(b => (b.round || 0) === (round || 0)) === i)) {
        // Ensured that it is not a situation of double baking (dataviz can't handle this right now)
        progress(level, json)
      }
    } else if (json.endorsements !== undefined) {
      progress(level, json)
    } else {
      progress(level, undefined)
    }
  } catch (e) {
    console.log(`Fetching data for level ${level} failed.`);
    progress(level, undefined)
  }
}

export const populateMany = async (first: number, last: number, progress: (first: number, last: number, v: { [key: number]: TeztaleServerData.T }) => void) => {
  try {
    const json = await TeztaleServerAPI.getBlockRangeDataJson(first, last);
    const res: { [key: number]: TeztaleServerData.T } = {};
    json.forEach(({ level, data }) => {
      const blocks = data.blocks;
      // We prefer to miss data rather than have data that would lead to wrong info displayed or crash
      // Ensure that we have at least either blocks or endorsements data.
      // If blocks are defined, also check that we can handle them (no double baking)
      if (blocks !== undefined) {
        if (blocks.every(({ round }, i) => blocks.findIndex(b => (b.round || 0) === (round || 0)) === i)) {
          // Ensured that it is not a situation of double baking (dataviz can't handle this right now)
          res[level] = data;
        } else {
          delete json[level]
        }
      } else if (data.endorsements !== undefined) {
        res[level] = data
      }
    });
    progress(first, last, res);
  } catch (e) {
    console.log(`Fetching data for range ${first}-${last} failed.`);
    progress(first, last, {})
  }
}

export const populate = async (blocks: Array<number>, progress: (k: number, v: TeztaleServerData.T | undefined) => void): Promise<void> => {
  await Promise.all(blocks.map(level => populateV1_fetch(level, progress)));
}

export const populateV1 = async (beg: number, end: number): Promise<DataByLevel> => {
  const data: DataByLevel = {};
  const blocks = range(beg, end)
  const progress = (k: number, v: TeztaleServerData.T | undefined) => v ? data[k] = v : {}
  await Promise.all(blocks.map(level => populateV1_fetch(level, progress)));
  return data;
}

const getOperationReceptionTime = (operation: TeztaleServerData.DelegateOperation_operation): number | null => {
  return (
    operation.received_in_mempools !== undefined && operation.received_in_mempools.length > 0
      ? minTime(operation.received_in_mempools, v => v.reception_time)
      : null
  );
}

/**
 * A type mapping rounds numbers to a list of delays between:
 * - corresponding block's timestamp
 * - reception time of delegates operations
 */
export type delaysDistribution = Array<Array<number>>

/**
 * @see {@link delaysDistributionOfOperationsAtLevel} or {@link delaysDistributionOfOperationsAtLevelWDelegate}
 */
const delaysDistributionOfOperationsAtLevelAux = (data: TeztaleServerData.T, msec: boolean = false, callbackInit: (round: number) => void, callBackDelay: (kind: TeztaleServerData.OperationKind | undefined, round: number, delegate: PublicKeyHash, delay: number) => void): void => {

  // For each block, `t_baker` maps its round number to its timestamp
  let t_baker: NumberMap<number> = {};

  // Register each every round timestamps
  // and prepare corresponding empty arrays for (pre)endorsement operations delays
  (data.blocks || []).forEach((block) => {
    let round = block.round || 0;
    if (block.timestamp) { t_baker[round] = new Date(block.timestamp).getTime() }
    callbackInit(round);
  });

  // For each delegate, compute diff of reception time of each of its operations with round timestamp
  // and add them to the corresponding (pre)endorsement delay array
  (data.endorsements || []).forEach((baker_ops: TeztaleServerData.DelegateOperation) => {
    (baker_ops.operations || []).forEach((operation) => {
      const round_cib = operation.round || 0;
      const reception_time = getOperationReceptionTime(operation);
      if (t_baker[round_cib] && reception_time != null) {
        const delay = reception_time - t_baker[round_cib];
        callBackDelay(operation.kind, round_cib, baker_ops.delegate, msec == false ? Math.ceil(delay / 1000) : delay);
      }
    })
  });
}

/**
 * @see {@link delaysDistributionOfOperations}
 */
export const delaysDistributionOfOperationsAtLevel = (data: TeztaleServerData.T, msec: boolean): Partitioned<delaysDistribution> => {
  const op_valid: delaysDistribution = [];
  const op_pre_valid: delaysDistribution = [];

  const callbackInit = (round: number) => {
    op_valid[round] = [];
    op_pre_valid[round] = [];
  }

  const callBackDelay = (kind: TeztaleServerData.OperationKind | undefined, round: number, _: PublicKeyHash, delay: number) => {
    if (kind) {
      op_pre_valid[round].push(delay);
    } else {
      op_valid[round].push(delay);
    }
  }

  delaysDistributionOfOperationsAtLevelAux(data, msec, callbackInit, callBackDelay);

  return { pre_endorsements: op_pre_valid, endorsements: op_valid }
}

/**
 * @param data A collection associating a block level to its data.
 * @param {boolean} msec If `true`, delays will be in milliseconds. Otherwise, seconds will be used.
 * @returns A pair of collection associating each block level with its {@link delaysDistribution} `{ 1: pre-endorsements, 2: endorsements }`.
 * @see {@link populateV1} for more details about expected data input.
 */
export const delaysDistributionOfOperations = (data: DataByLevel, msec = false): Partitioned<ByLevel<delaysDistribution>> => {

  const op_valid: ByLevel<delaysDistribution> = {};
  const op_pre_valid: ByLevel<delaysDistribution> = {};

  Object.entries(data).forEach(([levelKey, dataAtLevel]) => {
    const level = parseInt(levelKey)
    const { pre_endorsements: op_pre_valid_i, endorsements: op_valid_i } = delaysDistributionOfOperationsAtLevel(dataAtLevel, msec)
    op_pre_valid[level] = op_pre_valid_i;
    op_valid[level] = op_valid_i;
  })

  return { pre_endorsements: op_pre_valid, endorsements: op_valid }
}

/**
 * A type mapping rounds numbers to structure mapping each delegate to the delay between:
 * - corresponding block's timestamp
 * - reception time of the delegate operation (if multiple source available, minimum time is used)
 */
export type delaysDistributionWDelegate = Array<ByPublicKeyHash<number>>

export const delaysDistributionOfOperationsAtLevelWDelegate = (data: TeztaleServerData.T, msec: boolean): Partitioned<delaysDistributionWDelegate> => {
  const op_valid: delaysDistributionWDelegate = [];
  const op_pre_valid: delaysDistributionWDelegate = [];

  const callbackInit = (round: number) => {
    op_valid[round] = {};
    op_pre_valid[round] = {};
  }

  const callbackDelay = (kind: TeztaleServerData.OperationKind | undefined, round: number, delegate: PublicKeyHash, delay: number) => {
    if (kind) {
      op_pre_valid[round][delegate] = delay;
    } else {
      op_valid[round][delegate] = delay;
    }
  }

  delaysDistributionOfOperationsAtLevelAux(data, msec, callbackInit, callbackDelay);

  return { pre_endorsements: op_pre_valid, endorsements: op_valid }
}

/**
 * @param data A collection associating a block level to its data.
 * @param {boolean} msec If `true`, delays will be in milliseconds. Otherwise, seconds will be used.
 * @returns A pair of collection associating each block level with its {@link delaysDistributionWDelegate} `{ 1: pre-endorsements, 2: endorsements }`.
 * @see {@link populateV1} for more details about expected data input.
 */
export const delaysDistributionOfOperationsWDelegate = (data: DataByLevel, msec = false): Partitioned<ByLevel<delaysDistributionWDelegate>> => {

  let op_valid: ByLevel<delaysDistributionWDelegate> = {};
  let op_pre_valid: ByLevel<delaysDistributionWDelegate> = {};

  Object.entries(data).forEach(([levelKey, dataAtLevel]) => {
    let level = parseInt(levelKey)
    let { pre_endorsements: op_pre_valid_i, endorsements: op_valid_i } = delaysDistributionOfOperationsAtLevelWDelegate(dataAtLevel, msec)
    op_pre_valid[level] = op_pre_valid_i;
    op_valid[level] = op_valid_i;
  })

  return { pre_endorsements: op_pre_valid, endorsements: op_valid }
}

/**
 * Map delegate's public key hash to its endorsing power.
 */
export type endorsingPower = { [id: PublicKeyHash]: number }

export const getEndorsingPowerAtLevel = (data: TeztaleServerData.T): endorsingPower => {
  let endorsing_power: endorsingPower = {};
  Object.entries(data.endorsements || []).forEach(([_, baker_ops]) => {
    if (baker_ops.endorsing_power) {
      endorsing_power[baker_ops.delegate] = baker_ops.endorsing_power
    }
  })
  return endorsing_power
}

/**
 * For each level, get endorsing power for each delegate.
 */
export const getEndorsingPower = (data: DataByLevel): ByLevel<endorsingPower> => {
  let endorsing_power: ByLevel<endorsingPower> = {};
  Object.entries(data).forEach(([levelKey, dataAtLevel]) => {
    let level = parseInt(levelKey)
    endorsing_power[level] = getEndorsingPowerAtLevel(dataAtLevel)
  });
  return endorsing_power
}

export type ClassifyOperations<T> = {
  valid: Array<T>,
  lost: Array<T>,
  sequestered: Array<T>,
  erroneous: Array<T>
}

/**
 * For a given level and for each round within this level,
 * classify delegates into valid/missed/lost/sequestred/invalid categories.
 * Each PublicKeyHash appears in 1 or 2 categories.
 */
const classifyOperationsAux = <T>(
  fmt: (o: TeztaleServerData.DelegateOperation, t: number | null) => (T | undefined),
  missed: (_: TeztaleServerData.DelegateOperation) => void,
  data: TeztaleServerData.T
): NumberMap<Partitioned<ClassifyOperations<T>>> => {
  const operations_logs: NumberMap<Partitioned<ClassifyOperations<T>>> = {};
  const empty = () => {
    return {
      endorsements: { valid: [], lost: [], sequestered: [], erroneous: [] },
      pre_endorsements: { valid: [], lost: [], sequestered: [], erroneous: [] },
    }
  }
  const maybePush = (
    operation: TeztaleServerData.DelegateOperation_operation,
    collection: Partitioned<ClassifyOperations<T>>,
    kind: keyof ClassifyOperations<T>,
    value: T | undefined
  ) => {
    if (value !== undefined) collection[((operation.kind || 'Endorsement') === 'Endorsement' ? 'endorsements' : 'pre_endorsements')][kind].push(value)
  }
  (data.blocks || []).forEach((block) => {
    let round = block.round || 0;
    if (!operations_logs[round]) operations_logs[round] = empty()
  });
  (data.endorsements || []).forEach((endorsement) => {
    if (endorsement.operations) {
      endorsement.operations.forEach((operation) => {
        let round = operation.round || 0
        let operation_log = operations_logs[round];
        if (!operation_log) {
          operations_logs[round] = empty()
          operation_log = operations_logs[round]
        }
        const reception_time = getOperationReceptionTime(operation);
        // If the errors field is filled
        const error = operation.received_in_mempools && operation.received_in_mempools.findIndex(o => Boolean(o.errors)) !== -1
        if (error) maybePush(operation, operation_log, 'erroneous', fmt(endorsement, reception_time));
        if (reception_time === null) {
          // If the Operation is valid,
          // the reception time does not exist or is null,
          // but the operation is still included in the chain
          // => preendo/endo ESCROW
          if (operation.included_in_blocks) maybePush(operation, operation_log, 'sequestered', fmt(endorsement, null));
        } else if (!operation.included_in_blocks) {
          // If the OP is valid,
          // it is an 'endorsement' operation,
          // reception time has a value,
          // but the operation is not included in the block
          // => endo forgotten
          if (!operation.kind) maybePush(operation, operation_log, 'lost', fmt(endorsement, reception_time));
          // If the OP is 'pre-attestation', not being included in a block doesn't make it 'lost'
          // As long as the operation is received, it is 'valid'
          else maybePush(operation, operation_log, 'valid', fmt(endorsement, reception_time));
        } else {
          // Valid: received by the node (and included in the block if it is an attestation),
          // which does not mean errorless.
          maybePush(operation, operation_log, 'valid', fmt(endorsement, reception_time));
        }
      });
    } else {
      missed(endorsement)
    }
  });
  return operations_logs;
}

export type classifyOperationsOutput = NumberMap<Partitioned<ClassifyOperations<PublicKeyHash>>>
export const classifyOperations = (data: TeztaleServerData.T): classifyOperationsOutput => {
  return classifyOperationsAux(({ delegate }, _: number | null) => delegate, (_) => { }, data)
}

export type classifyOperationsWithReceptionTimeOutput = NumberMap<Partitioned<ClassifyOperations<{ delegate: PublicKeyHash, reception_time: number | null }>>>
export const classifyOperationsWithReceptionTime = (data: TeztaleServerData.T): classifyOperationsWithReceptionTimeOutput => {
  return classifyOperationsAux(({ delegate }, reception_time: number | null) => ({ delegate, reception_time }), (_) => { }, data)
}

type classifyOperationsForDelegateOutput0 = ClassifyOperations<Number> & { missed: Array<Number> }
export type classifyOperationsForDelegateOutput = Partitioned<classifyOperationsForDelegateOutput0>
export const classifyOperationsForDelegate = (data: DataByLevel, delegate0: PublicKeyHash): classifyOperationsForDelegateOutput => {
  const result: classifyOperationsForDelegateOutput = {
    endorsements: { valid: [], lost: [], sequestered: [], erroneous: [], missed: [] },
    pre_endorsements: { valid: [], lost: [], sequestered: [], erroneous: [], missed: [] },
  }
  const missedBlocks: Array<number> = []
  Object.entries(data).map(([levelKey, dataAtLevel]) => {
    const level = parseInt(levelKey)
    const missed = ({ delegate, operations }: TeztaleServerData.DelegateOperation) => {
      if (delegate === delegate0 && (operations || []).length === 0 && !missedBlocks.includes(level)) missedBlocks.push(level)
    }
    const fmt = ({ delegate }: TeztaleServerData.DelegateOperation, _: number | null) => delegate === delegate0 ? level : undefined
    const allRounds = classifyOperationsAux(fmt, missed, dataAtLevel)
    Object.values(allRounds).forEach(({ endorsements, pre_endorsements }) => {
      const aux0 = (acc: Array<Number>, value: Array<Number>) => value.forEach((x) => { if (acc.indexOf(x) === -1) acc.push(x) })
      const aux = (acc: classifyOperationsForDelegateOutput0, { valid, lost, sequestered, erroneous }: ClassifyOperations<Number>) => {
        aux0(acc.valid, valid)
        aux0(acc.lost, lost)
        aux0(acc.sequestered, sequestered)
        aux0(acc.erroneous, erroneous)
      }
      aux(result.endorsements, endorsements)
      aux(result.pre_endorsements, pre_endorsements)
    })
  });
  result.pre_endorsements.missed = missedBlocks
  result.endorsements.missed = missedBlocks
  return result;
}

/** Map a round `R` with all the blocks `B` where `R` includes some pre-attestation from `B`.
 * The associated list are delegates whose pre-attestation at round `R` has not been included in `B` */
export type reproposalOutput = { [key: number]: { [key: TeztaleServerData.BlockHash]: { proposer?: PublicKeyHash, delegates: Array<PublicKeyHash> } } }

export const reproposal = (data: DataByLevel[0]): reproposalOutput => {

  const result: reproposalOutput = {}

  // map rounds to pre-attestations operations
  const operations: { [key: number]: Array<TeztaleServerData.DelegateOperation_operation & { delegate: PublicKeyHash }> } = {};
  (data.endorsements || []).forEach((endorsement) => {
    (endorsement.operations || []).forEach((operation) => {
      if (operation.kind === 'Preendorsement') {
        const round = operation.round || 0;
        if (operations[round] === undefined) {
          operations[round] = [{ ...operation, delegate: endorsement.delegate }]
        } else {
          operations[round].push({ ...operation, delegate: endorsement.delegate })
        }
      }
    })
  })

  // map rounds to baker's pkh
  const bakers: { [key: number]: PublicKeyHash } = {};
  (data.blocks || []).forEach((block) => {
    if (block.delegate) {
      bakers[block.round || 0] = block.delegate
    }
  })

  Object.entries(operations).forEach(([round0, operations]) => {
    const round = parseInt(round0);
    (data.blocks || []).forEach(({ hash }) => {
      // If any of pre-attestation of `R` is included in a block `B`, `B` is a re-proposal of `R`
      if (operations.some(o => (o.included_in_blocks || []).includes(hash))) {
        if (result[round] === undefined) result[round] = {};
        result[round][hash] = {
          proposer: bakers[round],
          delegates: operations.flatMap(o => !((o.included_in_blocks || []).includes(hash)) ? [o.delegate] : [])
        };
      }
    });
  })

  return result
}

export type infoBlock = {
  applicationBlock: Array<number>, // associate each round to the minimum delay found in reception_times.
  validationBlock: Array<number>, // associate each round to the minimum delay found in reception_times.
  timestamps: Array<number>, // associate each round to the declared timestamp of the block.
  lastRound: number,             // round numbers for the block start at `0` and goes up to `lastRound` included. 
}

const getInfoBlockAux = (
  acc: { [k: number]: number },
  msec: boolean,
  reception_times: Array<any> | undefined,
  round: number,
  timestamp: number,
  getter: (x: {
    application?: string | undefined;
    validation?: string | undefined;
  }) => string | undefined
) => {
  const times = (reception_times || []).filter(r => getter(r) !== undefined)
  if (times.length > 0) {
    // Cast is okay because we filtered out undefined values
    const block_delay = minTime(times, getter as (_: any) => string) - timestamp;
    acc[round] = msec ? block_delay : block_delay / 1000;
  }
}

/**
 * Get {@link infoBlock} from level's data.
 */
export const getInfoBlock = (data: TeztaleServerData.T, msec = false): infoBlock => {
  const validationBlock: Array<number> = [];
  const applicationBlock: Array<number> = [];
  const timestamps: Array<number> = [];
  let lastRound = 0;
  (data.blocks || []).forEach((block) => {
    const round = block.round || 0;
    if (block.timestamp) {
      const timestamp = new Date(block.timestamp).getTime();
      timestamps[round] = timestamp;
      getInfoBlockAux(applicationBlock, msec, block.reception_times, round, timestamp, x => x.application);
      getInfoBlockAux(validationBlock, msec, block.reception_times, round, timestamp, x => x.validation);
    }
    if (round > lastRound) lastRound = round;
  });
  return { validationBlock, applicationBlock, timestamps, lastRound }
}

/**
 * Only keep timestamps coming from a given sources
 */
export const filterSource = (data: TeztaleServerData.T, source: string) => {
  const blocks = (data.blocks || []).map((b) => {
    const reception_times = (b.reception_times || []).filter(r => r.source === source)
    return {
      ...b,
      reception_times: reception_times.length === 0 ? undefined : reception_times
    }
  })
  const endorsements = (data.endorsements || []).map(e => {
    const operations = (e.operations || []).map(o => {
      const received_in_mempools = (o.received_in_mempools || []).filter(r => r.source === source)
      return { ...o, received_in_mempools: received_in_mempools.length === 0 ? undefined : received_in_mempools }
    }
    )
    return { ...e, operations: operations.length === 0 ? undefined : operations }
  })
  const missing_blocks = (data.missing_blocks || []).flatMap(m => -1 !== m.sources.findIndex(s => s === source) ? [m] : []);
  const result: TeztaleServerData.T = {
    blocks: blocks.length === 0 ? undefined : blocks,
    cycle_info: data.cycle_info,
    endorsements: endorsements.length === 0 ? undefined : endorsements,
    missing_blocks: missing_blocks.length === 0 ? undefined : missing_blocks,
  }
  return result;
}

/**
 * Returns the list of available sources in the given data
 */
export const extractSources = (data: TeztaleServerData.T) => {
  const sources: Array<string> = [];
  (data.blocks || []).forEach(b => (b.reception_times || []).forEach(r => pushUnique(sources, r.source)));
  (data.endorsements || []).forEach(e => (e.operations || []).forEach(o => (o.received_in_mempools || []).forEach(o => pushUnique(sources, o.source))));
  return sources;
}

/**
 * Returns the list of available delegates from endorsingPower
 */
export const extractDelegates = (endorsingPower: endorsingPower) => {
  const delegates: Array<PublicKeyHash> = [];
  Object.keys(endorsingPower).forEach(delegate => pushUnique(delegates, delegate));
  delegates.sort();
  return delegates;
}

/**
 * From a sorted (by reception time) delegates array, returns the associated [first slot, attested_so_far].
 * first_slot starts at 0
 */
export const getSortedDelegatesWSlotsRange = (sortedDelegates: Array<PublicKeyHash>, endorsingPower: ByPublicKeyHash<number>) => {
  let count = 0;
  const result =
    sortedDelegates.flatMap((delegate) => {
      if (endorsingPower[delegate]) {
        const end_pow = endorsingPower[delegate];
        const new_count = count + end_pow;
        const item = [delegate, [count, new_count]] as [string, [number, number]];
        count = new_count;
        return [item];
      } else {
        return []
      }
    });
  return result;
}

const getDelegateThresholdValidation = (round: ByPublicKeyHash<number>, endorsingPower: ByPublicKeyHash<number>, normalizedThreshold: number) => {

  // Sort delegate in order of arrival
  const sortedDelegates = keysInSortedOrderOfValues(round);

  // Associate each delegate with associated slots (depends on order of arrival and endorsing power of the delegate)
  const sortedDelegatesWSlotsRange = getSortedDelegatesWSlotsRange(sortedDelegates, endorsingPower);

  // Find the delegate who triggered threshold reach if exists
  const delegate = sortedDelegatesWSlotsRange.find(([_, [v0, v1]]) => normalizedThreshold >= v0 && normalizedThreshold <= v1)

  // If the delegate who triggered threshold reach exists, return its address
  return delegate ? delegate[0] : undefined;
}

/**
 * Threshold in percent (from 0.00 to 1.00)
 */
export const timeForPercIntegrationWEndorsingPower0 = (threshold: number, round: ByPublicKeyHash<number>, endorsingPower: ByPublicKeyHash<number>) => {
  const numberOfSlots = sumValues(endorsingPower); // number of slots for this block, before including endorsing power
  const delegate = getDelegateThresholdValidation(round, endorsingPower, Math.ceil(numberOfSlots * threshold));
  if (undefined != delegate) {
    return round[delegate]; // Delay on which we have the minimum number of pre endo for the round to be valid
  } else {
    return undefined;
  }
}

export const timeForPercIntegrationWEndorsingPower = (threshold: number, data: ByLevel<delaysDistributionWDelegate[0]>, endorsingPower: ByLevel<ByPublicKeyHash<number>>) => {
  const threshold_percent = threshold / 100;
  var result: Array<{ block: number, t: number }> = [];
  var didNotReachThreshold: Array<number> = [];
  Object.entries(data).forEach(([levelKey, round]) => {
    let level = parseInt(levelKey)
    try {
      const seuil_validation = timeForPercIntegrationWEndorsingPower0(threshold_percent, round, endorsingPower[level]);
      if (undefined !== seuil_validation) {
        result.push({ block: (+level), t: (seuil_validation || 0) });
      } else {
        didNotReachThreshold.push(+level);
      }
    } catch (e) {
      console.error(level, e)
    }
  });
  return [result, didNotReachThreshold] as [Array<{ block: number, t: number }>, Array<number>]
}