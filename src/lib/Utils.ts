import { useEffect, useState, useRef } from 'react'

export type ReactState<T> = [T, React.Dispatch<React.SetStateAction<T>>]

// Shorthand types
export type StringMap<value> = { [id: string]: value }
export type NumberMap<value> = { [id: number]: value }
export type Tuple2<A, B> = { A: A, B: B }
export type UseState<T> = [T, React.Dispatch<React.SetStateAction<T>>]

export var debugMode = true
var sections: Array<string> = []

export const NumberMap_map = <T1, T2>(init: NumberMap<T1>, fn: (x: T1) => T2): NumberMap<T2> => {
  const result: NumberMap<T2> = {}
  Object.entries(init).forEach(([k, v]) => {
    result[parseInt(k)] = fn(v)
  })
  return result;
}

export const beginSection = (s: string) => { if (debugMode) { sections.push(s) } }
export const endSection = (s: string) => {
  if (debugMode && sections.pop() != s) {
    console.warn("Ending section not opened (sections will be reset): " + s, sections)
    sections = []
  }
}
export const log = (...data: any[]): void => { if (debugMode) { console.debug('[' + sections.join('|') + ']', ...data) } }

export const parseRange = (s: string): [string | undefined, string | undefined] => {
  const matches = s.match(/\(([^,]+)?\.\.([^\]]+)?\)/)
  if (matches) {
    const a = matches[1]
    const b = matches[2]
    return [a !== undefined ? a : undefined, b !== undefined ? b : undefined]
  } else {
    return [undefined, undefined]
  }
}

/**
 * From a list of [pkh, timestamp] and endorsing power info,
 * compute when the consensus threshold is reached.
 * The `entries` array will be sorted by timestamp (ascending).
 */
export const quorum = (entries: Array<[PublicKeyHash, number]>, endorsingPower: endorsingPower): number | undefined => {
  entries.sort(([, a], [, b]) => a - b)
  let quorum: number | undefined = undefined
  // consensus_threshold = 4667 / 7000
  entries.reduce((acc, [delegate, delay]) => {
    const acc0 = acc + endorsingPower[delegate]
    if (acc < 4667 && acc0 >= 4667) quorum = delay
    return acc0
  }, 0)
  return quorum
}

export const rangeToString = ([a, b]: [Arithmetic.T | undefined, Arithmetic.T | undefined]) => {
  return '(' + (a?.input || '') + '..' + (b?.input || '') + ')'
}

/**
 * Generate an array of integers.
 * @param start first integer of the range (included)
 * @param end last integer of the range (included)
 * @returns An array of (`end` - `start` + 1) integers going from `end` to `start`.
 */
export const range = (start: number, end: number): Array<number> => {
  var res = [];
  for (let i = start; i <= end; i++) { res.push(+i); }
  return res;
}

/**
 * Convert a `React.Dispatch<React.SetStateAction<string>>`
 * to a `React.Dispatch<React.SetStateAction<string | undefined>>`
 * `undefined` is bound to `''`
 */
export const reactDisptachOption = (set: React.Dispatch<React.SetStateAction<string>>): React.Dispatch<React.SetStateAction<string | undefined>> => {
  return (x) => typeof x === 'function' ? set(y => x(y) || '') : set(x || '')
}

export const average = (arr: Array<number>): number => arr.reduce((p, c) => p + c, 0) / arr.length;

/**
 * Encode content and use it as data-uri with a invisible temporary <a>, using file as download file name, and trigger a click on this element.
 */
export const download = (file: string, content: string) => {
  const element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8, ' + encodeURIComponent(content));
  element.setAttribute('download', file);
  element.style.position = 'fixed';
  element.style.opacity = '0';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

export const sumValues = (obj: StringMap<number> | ArrayLike<number>) => Object.values(obj).reduce((a, b) => a + b, 0);

export const keysInSortedOrderOfValues = (dict: StringMap<number>): Array<string> => {
  return Object.keys(dict).sort((a, b) => dict[a] - dict[b])
}

export const minTime = <T>(array: Array<T>, getter: (x: T) => string): number => {
  return Math.min(...array.map(v => new Date(getter(v)).getTime()));
}

export const groupBy = <V>(xs: Array<V>, key: (_: V) => string): StringMap<Array<V>> => {
  let acc: StringMap<Array<V>> = {};
  return xs.reduce((acc, x) => {
    let k: string = key(x);
    let el: Array<V> = acc[k];
    el ? el.push(x) : acc[k] = [x];
    return acc;
  }, acc);
}

/**
 * From tz1UMMZHpwmrQBHjAdwcL8uMe3fjZSecEz3F to tz1UMMZ…cEz3F
 */
export const shortenHash = (x: string) => x.slice(0, 5) + '…' + x.slice(-5)

/**
 * Same as `parseInt`, but returns `undefined` instead of `NaN`
 */
export const parseIntUndefined = (s: string) => {
  const n = parseInt(s)
  return isNaN(n) ? undefined : n
}

// See https://usehooks-ts.com/react-hook/use-debounce
/**
 * If your debounced value follows `Value` DO NOT call `setValue` from the `map` function.
 */
export const useDebounceMap = <TInput, TOutput>(value: TInput, map: (_: TInput) => TOutput, delay?: number): TOutput => {
  const [debouncedValue, setDebouncedValue] = useState<TOutput>(map(value))
  useEffect(() => {
    const timer = setTimeout(() => setDebouncedValue(map(value)), delay || 500)
    return () => clearTimeout(timer)
  }, [value, delay])
  return debouncedValue
}
export const useDebounce = <T>(value: T, delay?: number): T => useDebounceMap(value, x => x, delay)

// See https://usehooks.com/usethrottle
export const useThrottle = <T>(value: T, interval = 500) => {
  const [throttledValue, setThrottledValue] = useState(value);
  const lastUpdated = useRef(Number.MIN_VALUE);
  useEffect(() => {
    const now = Date.now();
    if (now >= lastUpdated.current + interval) {
      lastUpdated.current = now;
      setThrottledValue(value);
    } else {
      const id = window.setTimeout(() => {
        lastUpdated.current = now;
        setThrottledValue(value);
      }, interval);
      return () => window.clearTimeout(id);
    }
  }, [value, interval]);

  return throttledValue;
}

export const unique = <T,>(array: Array<T>): Array<T> => {
  return array.filter((item, index) => array.indexOf(item) === index)
}

/**
 * Only push the element in array if not already present
 */
export const pushUnique = <T,>(array: Array<T>, item: T): void => {
  if (array.indexOf(item) === -1) array.push(item)
}

export namespace Arithmetic {

  /**
   * Represents either:
   * - an absolute value (e.g. '10', '10+2')
   * - a relative value (e.g. '+10', '-2')
   */
  export type T = {
    input: string,
    modifier?: ((_: number) => number),
    value?: number,
  }

  const parseModifier = (modifier: string): ((_: number) => number) | undefined => {
    const n = parseInt(modifier.substring(1));
    return modifier[0] == '+' ? (x) => x + n : (x) => x - n
  }

  export const parse = (input: string): T | undefined => {
    const cleaned = input.replace(/\s+/g, ''); // remove spaces
    const matches = cleaned.match(/(\d+)?([+-]\d+)?/)
    if (matches) {
      const v = matches[1]
      const m = matches[2]
      const modifier = m ? parseModifier(m) : undefined
      const value = v ? parseInt(v) : undefined
      return { input, modifier, value }
    } else {
      return undefined
    }
  }

  /**
   * Best effort to compute a [x, y] interval from two arithmetic expressions
   * e.g. [10, +2] -> [10, 12], [-1, 9] -> [8, 9]
   */
  export const computeRange = ([arithmeticsX, arithmeticsY]: [T | undefined, T | undefined]): [number | undefined, number | undefined] => {
    const computeNumber = (input: T) => {
      return input.value ? input.modifier ? input.modifier(input.value) : input.value : undefined
    }
    const initNumberX = arithmeticsX !== undefined ? computeNumber(arithmeticsX) : undefined
    const initNumberY = arithmeticsY !== undefined ? computeNumber(arithmeticsY) : undefined
    const compute = (init: number | undefined, modifier: ((_: number) => number) | undefined, otherInit: number | undefined) => {
      if (init !== undefined) {
        return init;
      } else if (modifier !== undefined && otherInit !== undefined) {
        return modifier(otherInit)
      } else {
        return undefined;
      }
    }
    const X = compute(initNumberX, arithmeticsX?.modifier, initNumberY)
    const Y = compute(initNumberY, arithmeticsY?.modifier, initNumberX)
    return [X, Y]
  }

  /**
   * If a given Arithmetic.T's input field includes 'HEAD",
   * update the `value` field according to the value of HEAD passed as argument.
   */
  export const updateWithHEAD = (arithmetic0: T, HEAD: number) => {
    if (arithmetic0.input.includes('HEAD')) {
      const s0 = arithmetic0.input
      const s = s0.replace('HEAD', HEAD !== undefined ? HEAD.toString() : 'HEAD')
      const arithmetic = Arithmetic.parse(s)
      return arithmetic !== undefined ? { ...arithmetic, input: s0 } : undefined
    } else {
      return arithmetic0
    }
  }

  export const toNumber = (a: T) => a.value !== undefined ? a.modifier !== undefined ? a.modifier(a.value) : a.value : undefined

  export const addNumber = (arithmetic0: string, n: number) => {
    const matches = arithmetic0.match(/([^+-]+)?([+-]\d+)?/)
    if (matches && matches[2]) {
      const v = matches[1]
      const m0 = parseInt(matches[2])
      const m = m0 + n
      return (v + (m == 0 ? '' : m > 0 ? '+' + m : m))
    } else {
      const i = parseInt(arithmetic0)
      return isNaN(i) ? arithmetic0 + (n > 0 ? '+' + n : n) : `${(i + n)}`
    }
  }
}

import { useCallback } from 'react'
import { PublicKeyHash } from './TeztaleServerData'
import { endorsingPower } from './PopulateSortData'

const getHashSearchParams = () => {
  return new URLSearchParams(window.location.hash.slice(1))
};

const replaceHashSearchParam = (params: URLSearchParams, k: string, v: string | null) => {
  if (v === null) {
    params.delete(k);
  } else {
    params.set(k, v);
  }
}

const replaceParams = (queue: Array<[string, string | null]>) => {
  const params = getHashSearchParams();
  queue.forEach(([k, v]) => replaceHashSearchParam(params, k, v))
  window.location.hash = params.toString();
}

export namespace Batch {

  export type T = {
    delay: number,
    queue: Array<[string, string | null]>,
    timer: number | undefined,
  }

  export const add = (q: T, key: string, value: string | null) => {
    clearTimeout(q.timer);
    q.queue.push([key, value]);
    q.timer = setTimeout(() => exec(q), q.delay);
  }

  export const exec = (q: T) => {
    const queue = [...q.queue];
    q.queue = [];
    replaceParams(queue);
  }

  export const useBatch = (delay?: number): T => {
    const q = { queue: [], timer: undefined, delay: delay || 100 }
    useEffect(() => () => clearTimeout(q.timer), [])
    return q
  }

}

// Adapted from https://github.com/hejmsdz/use-hash-param
export const useHashParam = <T,>(
  key: string,
  defaultValue: T,
  fromString0: (_: string) => T | undefined,
  toString0: (_: T) => string,
  batch?: Batch.T,
): [T, (value: T | undefined | ((_: T) => T)) => void] => {
  const fromString = (s: string) => fromString0(decodeURIComponent(s))
  const toString = (x: T) => encodeURIComponent(toString0(x))

  // Helper
  const getHashParam = () => {
    const v0 = getHashSearchParams().get(key)
    if (v0 === null) {
      return defaultValue
    } else {
      const v1 = fromString(v0)
      return v1 === undefined ? defaultValue : v1;
    }
  };
  // Helper
  const setHashParam = (value0: T | undefined) => {
    const value = value0 === undefined ? null : toString(value0)
    batch === undefined ? replaceParams([[key, value]]) : Batch.add(batch, key, value)
  }

  // Real code begins here

  const init = getHashParam();
  const [innerValue, setInnerValue] = useState(init);

  useEffect(() => {
    const handleHashChange = () => setInnerValue(getHashParam());
    handleHashChange();
    window.addEventListener('hashchange', handleHashChange);
    return () => window.removeEventListener('hashchange', handleHashChange);
  }, [key]);

  const setValue = useCallback((value: T | undefined | ((_: T) => T)) => {
    if (undefined === value) {
      setHashParam(defaultValue);
    } else if (typeof value === 'function') {
      setHashParam((value as (_: T) => T)(getHashParam()));
    } else {
      setHashParam(value);
    }
  }, [key]);

  return [innerValue || defaultValue, setValue];
}

const useHashParam0 = <T>(
  key: string,
  defaultValue: T,
  ofString: (_: string) => T | undefined,
  toString: (_: T) => string,
  neq: (a: T, b: T) => boolean,
  batch?: Batch.T,
) => {
  const [ParamURL, setParamURL] = useHashParam(key, defaultValue, ofString, toString, batch);
  const [ParamInput, setParamInput] = useState(ParamURL);
  const DebouncedParamInput = useDebounce(ParamInput, 500);
  useEffect(() => {
    if (neq(DebouncedParamInput, ParamURL)) setParamURL(DebouncedParamInput)
  }, [DebouncedParamInput]);
  useEffect(() => {
    if (neq(DebouncedParamInput, ParamURL)) setParamInput(ParamURL)
  }, [ParamURL]);
  return {
    DebouncedParamInput,
    ParamInput,
    ParamURL,
    setParamURL,
    setParamInput,
  }
}

const useHashParam0NoAutoURLUpdate = <T>(
  key: string,
  defaultValue: T,
  ofString: (_: string) => T | undefined,
  toString: (_: T) => string,
  neq: (a: T, b: T) => boolean,
  batch?: Batch.T,
) => {
  const [ParamURL, setParamURL] = useHashParam(key, defaultValue, ofString, toString, batch);
  const [ParamInput, setParamInput] = useState(ParamURL);
  const updateURL = () => { if (neq(ParamInput, ParamURL)) setParamURL(ParamInput) }
  useEffect(() => {
    if (neq(ParamInput, ParamURL)) setParamInput(ParamURL)
  }, [ParamURL]);
  return {
    updateURL,
    ParamInput,
    ParamURL,
    setParamURL,
    setParamInput,
  }
}

export const useHashParamString = <T extends string>(
  key: string,
  defaultValue: T,
  batch?: Batch.T,
) => {
  return useHashParam0(
    key,
    defaultValue,
    (x: string) => (x as T),
    (x: T) => (x as string),
    (a, b) => a !== b,
    batch,
  )
}

export const useHashParamStringNoAutoURLUpdate = <T extends string>(
  key: string,
  defaultValue: T,
  batch?: Batch.T,
) => {
  return useHashParam0NoAutoURLUpdate(
    key,
    defaultValue,
    (x: string) => (x as T),
    (x: T) => (x as string),
    (a, b) => a !== b,
    batch,
  )
}

export const useHashParamStringOption = <T extends string>(
  key: string,
  defaultValue: T,
  batch?: Batch.T,
) => {
  return useHashParam0(
    key,
    defaultValue,
    (x: string) => x === '' ? undefined : (x as T),
    (x: T) => (x === undefined ? '' : x as string),
    (a, b) => a !== b,
    batch,
  )
}

export const useHashParamInt = (
  key: string,
  defaultValue: number,
  batch?: Batch.T,
) => {
  const fromString = (s: string): number => {
    const v0 = parseInt(s)
    return isNaN(v0) ? defaultValue : v0;
  }
  const toString = (n: number): string => n.toString()
  return useHashParam0(
    key,
    defaultValue,
    fromString,
    toString,
    (a, b) => a !== b,
    batch,
  )
}

export const useHashParamIntOption = (
  key: string,
  defaultValue: number | undefined,
  batch?: Batch.T,
) => {
  const fromString = (s: string): number | undefined => {
    const v0 = parseInt(s)
    return isNaN(v0) ? defaultValue : v0;
  }
  const toString = (n: number | undefined): string => n === undefined ? '' : n.toString()
  return useHashParam0(
    key,
    defaultValue,
    fromString,
    toString,
    (a, b) => a !== b,
    batch,
  )
}

export const useHashParamArithmetic = (
  key: string,
  defaultValue: Arithmetic.T | undefined,
  batch?: Batch.T,
) => {
  const fromString = (s: string) => Arithmetic.parse(s)
  const toString = (a: Arithmetic.T | undefined) => a !== undefined ? a.input : ''
  return useHashParam0(
    key,
    defaultValue,
    fromString,
    toString,
    (a, b) => a?.input !== b?.input,
    batch,
  )
}

export const useHashParamRange = (
  key: string,
  defaultValue: [Arithmetic.T | undefined, Arithmetic.T | undefined],
  batch?: Batch.T,
) => {
  const fromString = (s: string): [Arithmetic.T | undefined, Arithmetic.T | undefined] => {
    const [a, b] = parseRange(s);
    return [a === undefined ? undefined : Arithmetic.parse(a), b !== undefined ? Arithmetic.parse(b) : undefined]
  }
  const toString = rangeToString
  return useHashParam(key, defaultValue, fromString, toString, batch)
}