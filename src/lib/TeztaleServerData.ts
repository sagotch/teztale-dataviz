export type PublicKeyHash = string
export type BlockHash = string // uses Base 58 encoding
export type TimeStamp = string // e.g. 2023-04-12T13:14:43.667-00:00 // FIXME? Check what exact type it can be

export interface Cycleinfo {
  cycle: number,
  cycle_position: number,
  cycle_size: number,
}

export interface DelegateOperations_reception {
  source: string, // identifier of the teztale-archiver who send the operation
  reception_time: TimeStamp,
  errors?: any,
}

export type OperationKind = "Endorsement" | "Preendorsement"

export interface DelegateOperation_operation {
  kind?: OperationKind, // If not present, it is 'Endorsement'
  round?: number,
  received_in_mempools?: Array<DelegateOperations_reception>,
  included_in_blocks?: Array<BlockHash>,
}

export interface DelegateOperation {
  delegate: PublicKeyHash,
  endorsing_power?: number,
  operations?: Array<DelegateOperation_operation>,
}

export interface Block {
  hash: BlockHash,
  delegate?: PublicKeyHash,
  predecessor?: BlockHash,
  successor?: BlockHash,
  round?: number,
  reception_times?: Array<{
    source: string, // identifier of the teztale-archiver who send the operation
    application?: TimeStamp,
    validation?: TimeStamp,
  }>,
  timestamp?: TimeStamp,
  nonce?: void,
}

export interface BakingRight {
  delegate: PublicKeyHash,
  round: number,
}

export interface MissingBlock {
  baking_right: BakingRight,
  sources: Array<string>,
}

export interface T {
  blocks?: Array<Block>,
  cycle_info?: Cycleinfo,
  endorsements?: Array<DelegateOperation>,
  missing_blocks?: Array<MissingBlock>
}

export interface BatchItem {
  data: T,
  level: number,
}

export type Batch = Array<BatchItem>

/** [ level, round, baker, partially missing ] */
export type MissingData = [ number, number, string, boolean ]