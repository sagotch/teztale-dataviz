import { createContext, useState } from 'react';
import { StringMap } from './Utils';

export interface AliasesContextType {
  Aliases: StringMap<string> | undefined,
  setAliases: (_: StringMap<string> | undefined) => void,
}

export const AliasesContext = createContext<AliasesContextType>({
  Aliases: undefined,
  setAliases: (Aliases: StringMap<string> | undefined) => { console.log('AliasesContext default setAliases ', Aliases) },
});

export const AliasesContextProvider = ({ children }: React.PropsWithChildren<{}>) => {
  const [Aliases, setAliases0] = useState(undefined as StringMap<string> | undefined)
  const setAliases = (Aliases: StringMap<string> | undefined) => {
    setAliases0(Aliases)
  }
  return (
    <AliasesContext.Provider value={{ Aliases, setAliases }}>
      {children}
    </AliasesContext.Provider>
  )
}

export const genGetAliases = async (url: string | undefined): Promise<StringMap<string>> => {
  if (url !== undefined) {
    try {
      const fetched = await fetch(url);
      const json = await fetched.json();
      const result: StringMap<string> = {};
      if (Array.isArray(json)) {
        json.forEach((o) => { if (o.alias) result[o.address] = o.alias })
      }
      return result
    } catch (error) {
      console.error(error)
      return {}
    }
  } else { return {} }
}

export const getTzKTAliases = async (url: string | undefined): Promise<StringMap<string>> => {
  if (url !== undefined) {
    return genGetAliases(`${url}v1/delegates?select=address,alias&limit=10000`)
  } else { return {} }
}

export const getAliases = async (url: string | undefined): Promise<StringMap<string>> => {
  if (url !== undefined) {
    return genGetAliases(`${url}delegates-aliases.json`)
  } else { return {} }
}
