import { createContext, useState } from 'react';

export type Protocol = {
  code: number,
  hash: "string",
  firstLevel: number,
  firstCycle: number,
  firstCycleLevel: number,
  lastLevel?: number,
  constants: unknown,
  extras: unknown,
  metadata: unknown,
}

export interface ProtocolsContextType {
  Protocols: Array<Protocol> | undefined,
  setProtocols: (_: Array<Protocol> | undefined) => void,
}

export const ProtocolsContext = createContext<ProtocolsContextType>({
  Protocols: undefined,
  setProtocols: (Protocol: Array<Protocol> | undefined) => { console.log('ProtocolContext default setProtocol ', Protocol) },
});

export const ProtocolsContextProvider = ({ children }: React.PropsWithChildren<{}>) => {
  const [Protocols, setProtocols0] = useState(undefined as Array<Protocol> | undefined)
  const setProtocols = (Protocols: Array<Protocol> | undefined) => {
    setProtocols0(Protocols)
  }
  return (
    <ProtocolsContext.Provider value={{ Protocols, setProtocols }}>
      {children}
    </ProtocolsContext.Provider>
  )
}

export const getProtocols = async (tzkt: string | undefined): Promise<Array<Protocol> | undefined> => {
  if (tzkt !== undefined) {
    try {
      const tzkt_getProtocolURL = `${tzkt}v1/protocols`;
      const fetched = await fetch(tzkt_getProtocolURL);
      const json: Array<Protocol> = await fetched.json();
      return json
    } catch (error) {
      console.error(error)
      return undefined
    }
  } else { return undefined }
}