stages:
  - docker
  - deploy

.build:
  image: node:lts
  cache:
    key:
      files:
        - pnpm-lock.yaml
    paths:
      - .pnpm-store
  before_script:
    - corepack enable
    - corepack prepare pnpm@latest-8 --activate
    - pnpm config set store-dir .pnpm-store
  script:
    - pnpm install
    - pnpm build
  artifacts:
    expire_in: 1 week
    paths:
      - dist/

.deploy-template:
  stage: deploy
  extends: .build
  variables:
    HOMEPAGE: "/"
  script:
    - "sed -i \"s|base: '/'|base: '$HOMEPAGE'|\" vite.config.ts"
    - "sed -i \"s|inner_serverURL = '/'|inner_serverURL = 'https://teztale-mainnet.obs.nomadic-labs.cloud/'|\" src/lib/TeztaleServerAPI.tsx"
    - echo '[' > src/assets/servers-aliases.json
    - echo '{"alias":"Mainnet","address":"https://teztale-mainnet.obs.nomadic-labs.cloud/","TzKT":"https://api.mainnet.tzkt.io/"}' >> src/assets/servers-aliases.json
    - echo ',' >> src/assets/servers-aliases.json
    - echo '{"alias":"Ghostnet","address":"https://teztale-ghostnet.obs.nomadic-labs.cloud/","TzKT":"https://api.ghostnet.tzkt.io/"}' >> src/assets/servers-aliases.json
    - echo ',' >> src/assets/servers-aliases.json
    - echo '{"alias":"Quebecnet","address":"https://teztale-quebecnet.obs.nomadic-labs.cloud/","TzKT":"https://api.quebecnet.tzkt.io/"}' >> src/assets/servers-aliases.json
    - echo ']' >> src/assets/servers-aliases.json
    - !reference [.build, script]
    - cp ./dist/index.html ./dist/404.html
    - rm -rf ./public
    - mkdir ./public
    - mv ./dist/* ./public
  artifacts:
    expire_in: 1 week
    paths:
      - public/

review:
  extends: .deploy-template
  variables:
    HOMEPAGE: "/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/"
  except:
    - main
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: "https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/index.html"
    auto_stop_in: 1 week

pages:
  extends: .deploy-template
  variables:
    HOMEPAGE: "/$CI_PROJECT_NAME/"
  only:
    - main
  environment:
    name: pages
    url: $CI_PAGES_URL

.dist:
  stage: deploy
  extends: .build
  only:
    - main
  script:
    - ([ -z "$PACKAGE" ] || [ -z "$ARCHIVE" ]) && (echo "PACKAGE and ARCHIVE variables must be defined"; exit 1) || true
    - !reference [.build, script]
    - ln -s ./index.html ./dist/Level
    - ln -s ./index.html ./dist/CommitteeSizePerDelay
    - ln -s ./index.html ./dist/CommitteeSizePerLevel
    - ln -s ./index.html ./dist/DelayToConsensus
    - ln -s ./index.html ./dist/DelegateStats
    - 'mv ./dist ./${PACKAGE}'
    - 'apt-get update'
    - 'apt-get -y install zip'
    - 'zip -r ${ARCHIVE} ./${PACKAGE}'
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${ARCHIVE} "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE}/latest/${ARCHIVE}"'
  artifacts:
    expire_in: 1 week
    paths:
      - "${ARCHIVE}"


dist:
  extends: .dist
  variables:
    PACKAGE: "teztale-dataviz"
    ARCHIVE: "teztale-dataviz.latest.zip"

# The lite variant is the same as regular distrib without custom fonts
dist-lite:
  extends: .dist
  variables:
    PACKAGE: "teztale-dataviz-lite"
    ARCHIVE: "teztale-dataviz-lite.latest.zip"
  before_script:
    - sed -i '/@fontsource/d' ./package.json
    - sed -i '/@fontsource/d' ./src/ui/UI_Theme.tsx
    # .dist extends .build, which already uses before_script
    - !reference [.build, before_script]

manual-stop-review:
  stage: deploy
  script:
    - echo "Remove review app"
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  when: manual

docker-hadolint:
  image: hadolint/hadolint:latest-debian
  stage: docker
  script:
    - mkdir -p reports
    - hadolint -f gitlab_codeclimate Dockerfile > reports/hadolint-$(md5sum Dockerfile | cut -d" " -f1).json
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    reports:
      codequality:
        - "reports/*"
    paths:
      - "reports/*"
