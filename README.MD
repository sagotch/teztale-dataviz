# Teztale Dataviz

A data visualization tool for [teztale](https://gitlab.com/nomadic-labs/teztale).

## Installation

To install the necessary dependencies, run the following command:

```
pnpm install
```

## Running the App

To start the app in development mode, run:

```
pnpm run dev
```

This will start the app and print the address to use to access to the app (e.g. http://localhost:5173/).

## Building the App

To build the app, run:

```
pnpm run build
```

This will create a production-ready build of the app in the `dist` directory.

## Previewing the Build

To preview the production build, run:

```
pnpm run preview
```

This will start a server to serve the production build of the app and print the address to use to access to the app (e.g. http://localhost:4173/).

## Acknowledgments

This app is built with:
- [pnpm](https://pnpm.io/)
- [typescript](https://www.typescriptlang.org/)

It (directly) uses the following libraries:
- [Joy UI ](https://www.npmjs.com/package/@mui/joy)
- [raviger](https://www.npmjs.com/package/raviger)
- [React](https://www.npmjs.com/package/react)
- [Recharts](https://www.npmjs.com/package/recharts)

## License

This project is licensed under the [MIT License](LICENSE).
