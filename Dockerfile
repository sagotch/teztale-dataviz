# 20.6 broke compilation. See https://github.com/nodejs/node/issues/49497
FROM node:20.5.1-slim AS base
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable
COPY . /app
WORKDIR /app

FROM base AS prod-deps
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --prod --frozen-lockfile
# hadolint ignore=DL3059
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --prod express

FROM base AS build
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --frozen-lockfile
RUN pnpm run build

FROM base
COPY --from=prod-deps /app/node_modules /app/node_modules
COPY --from=build /app/dist /app/dist
COPY --from=build /app/server.cjs /app/server.cjs
EXPOSE 3000
CMD [ "node", "/app/server.cjs" ]
